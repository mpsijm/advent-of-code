#!/usr/bin/env bash

trap 'trap - SIGTERM && kill 0' SIGINT SIGTERM EXIT

[[ -z "$1" ]] && day="$(date +%-d)" || day=$1
[[ -z "$2" ]] && year="$(date +%Y)" || year=$2

./dl "$day" "$year"
./copy_sample "$day" "$year" &
./run "$day" "$year" &
wait
