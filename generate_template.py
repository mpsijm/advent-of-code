from sys import argv, stderr


def is_bin(x):
    return all(c in "01" for c in x)


def is_int(x):
    try:
        int(x)
        return True
    except:
        return False


with open(argv[1]) as f:
    single_line = False
    raw_data = f.read().strip()
    length_line = "n = len(data)"
    if "\n\n" in raw_data:
        reader = """[[x for x in block.replace("\\n", " ").split()] for block in f.read().strip().split("\\n\\n")]"""
    else:
        # lines = f.readlines()
        lines = raw_data.split("\n")
        separators = {sep: all(sep in line for line in lines) for sep in (" ", ",")}
        if list(separators.values()).count(True) == 1:
            sep = None if separators[" "] else ","
            sep_template = "" if sep is None else f'"{sep}"'
            print(f"All lines are {'space' if sep is None else sep_template} separated", file=stderr)
            lines = [line.strip().split(sep) for line in lines]
            if len(set(len(line) for line in lines)) > 1:
                print(f"""Lines have differing lengths between {min(len(line) for line in lines)
                } and {max(len(line) for line in lines)}""", file=stderr)
                if all(is_int(y) for x in lines for y in x) and len(lines[0]) > 3:
                    print("All lines consist of integers", file=stderr)
                    reader = f"""[[int(x) for x in line.strip().split({sep_template})] for line in f.readlines()]"""
                else:
                    reader = f"""[line.strip().split({sep_template}) for line in f.readlines()]"""
            else:
                print(f"All lines have length {len(lines[0])}", file=stderr)
                intable = [all(is_int(lines[i][j]) for i in range(len(lines)))
                           and not all(is_bin(lines[i][j]) for i in range(len(lines)))
                           for j in range(len(lines[0]))]
                if any(intable):
                    if all(is_int(y) for x in lines for y in x) and len(lines[0]) > 3:
                        print("All lines consist of integers", file=stderr)
                        reader = f"""[[int(x) for x in line.strip().split({sep_template})] for line in f.readlines()]"""
                    else:
                        if len(lines[0]) <= 26:
                            print("All lines have the same structure", file=stderr)
                            a = ord("a")
                            reader = f"""[[{
                            ", ".join(f"int({chr(i + a)})" if intable[i] else chr(i + a) for i in range(len(intable)))
                            }] for {
                            ", ".join(chr(i + a) for i in range(len(intable)))
                            } in (line.strip().split({sep_template}) for line in f.readlines())]"""
                        else:
                            print("Lines are too long, good luck parsing for yourself", file=stderr)
                            reader = f"""[line.strip() for line in f.readlines()]"""
                else:
                    reader = f"""[line.strip().split({sep_template}) for line in f.readlines()]"""
        else:
            if all(is_int(x) for x in lines) and not all(is_bin(x) for x in lines):
                if all(len(x) < 42 for x in lines):
                    reader = """[int(line) for line in f.readlines()]"""
                else:
                    reader = """[[int(c) for c in line.strip()] for line in f.readlines()]"""
            else:
                reader = """[line.strip() for line in f.readlines()]"""
            if len(set(len(line) for line in lines)) == 1:
                print(f"All lines have length {len(lines[0])}", file=stderr)
                length_line = "h, w = len(data), len(data[0])"
        if len(lines) == 1:
            single_line = True
            print("Only a single line", file=stderr)
            reader += "[0]"

    template = f"""data = {reader.replace("f.read", 'open("datb.in").read')}\n{length_line}\n\n"""

    exec(f"""
from sys import stderr
from json import dumps

{template.replace("datb.in", argv[1])}

single_line = {single_line}
print("", file=stderr)
if single_line:
    dump = dumps(data)
    if len(data) > 200:
        if type(data) == str:
            dump = dump[:50] + "   ...   " + dump[-50:]
            print(f"len(data) = {{len(data)}}", file=stderr)
        else:
            dump = ",".join(dump.split(",")[:50]) + ",   ...,   " + ",".join(dump.split(",")[-50:])
    print(f"data = {{dump}}", file=stderr)
else:
    print("data = [", file=stderr)
    for x in data[:5]:
        print(f"  {{dumps(x)}},", file=stderr)
    if len(data) > 5:
        print("  ...", file=stderr)
    if len(data) > 10:
        for x in data[-5:]:
            print(f"  {{dumps(x)}},", file=stderr)
    print("]", file=stderr)
print("", file=stderr)
""")

    print(template)
