with open("data.in") as f:
    data = [[a, int(b)] for a, b in (x.strip().split(" ") for x in f.readlines())]


def execute_instructions(data, always_print=False):
    pc = 0
    seen = set()
    acc = 0
    while pc < len(data) and pc not in seen:
        seen.add(pc)
        instr, val = data[pc]
        if instr == "nop":
            pc += 1
        if instr == "acc":
            acc += val
            pc += 1
        if instr == "jmp":
            pc += val
    if pc >= len(data) or always_print:
        print(acc)


execute_instructions(data, True)

for i in range(len(data)):
    if data[i][0] == "acc":
        continue
    data[i][0] = "nop" if data[i][0] == "jmp" else "jmp"
    execute_instructions(data)
    data[i][0] = "nop" if data[i][0] == "jmp" else "jmp"
