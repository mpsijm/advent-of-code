with open("data.in") as f:
    data = [x.strip() for x in f.readlines()]

dirs = [(-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1, 0), (1, 1)]


def count_around_a(prev, i, j):
    return sum(1 for y, x in dirs if (0 <= j + x < len(prev[i]) and 0 <= i + y < len(prev) and prev[i + y][j + x] == "#"))


def count_around_b(prev, i, j):
    num = 0
    for dir in dirs:
        y, x = dir
        mult = 1
        while 0 <= j + x < len(prev[i]) and 0 <= i + y < len(prev):
            if prev[i + y][j + x] == ".":
                mult += 1
                y, x = dir[0] * mult, dir[1] * mult
            elif prev[i + y][j + x] == "#":
                num += 1
                break
            elif prev[i + y][j + x] == "L":
                break
    return num


def simulate(counter, around):
    seats = data.copy()
    prev = []
    count = 0
    while seats != prev:
        prev = seats.copy()
        for i in range(len(seats)):
            line = list(prev[i])
            for j in range(len(line)):
                if line[j] == "L" and counter(prev, i, j) == 0:
                    line[j] = "#"
                if line[j] == "#" and counter(prev, i, j) >= around:
                    line[j] = "L"
            seats[i] = "".join(line)
        count += 1
        # print(seats)
    print(count)
    print(sum(line.count("#") for line in seats))


simulate(count_around_a, 4)
simulate(count_around_b, 5)
