with open("data.in") as f:
    data = [[x for x in block.split("\n")] for block in f.read().strip().split("\n\n")]

rules, words = data
rules = {rule: [sub.split(" ") for sub in rhs.split(" | ")] for rule, rhs in (r.split(": ") for r in rules)}


def match(rule, word, positions):
    if not positions:
        return positions
    start_positions = positions.copy()
    ans = []
    for option in rules[rule]:
        positions = start_positions
        for sub in option:
            if sub in {'"a"', '"b"'}:
                positions = [i + 1 for i in positions if i < len(word) and word[i] == sub[1]]
            else:
                positions = match(sub, word, positions)
        ans.extend(positions)
    return ans


print(sum(any(j == len(word) for j in match("0", word, [0])) for word in words))
rules["8"] = [["42"], ["42", "8"]]
rules["11"] = [["42", "31"], ["42", "11", "31"]]
print(sum(any(j == len(word) for j in match("0", word, [0])) for word in words))
