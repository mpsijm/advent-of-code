with open("data.in") as f:
    data = [x.strip() for x in f.readlines()]

seats = {int(seat.translate(str.maketrans("BFRL", "1010")), 2) for seat in data}
low = min(seats)
high = max(seats)
print(high)
print([seat for seat in range(low, high) if seat not in seats])
