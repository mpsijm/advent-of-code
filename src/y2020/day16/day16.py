with open("data.in") as f:
    data = [[x for x in block.split("\n")] for block in f.read().strip().split("\n\n")]

rules, yours, nearby = data

all_rules = dict()
for rule in rules:
    name, rule = rule.split(": ")
    ranges = rule.split(" or ")
    all_rules[name] = [[int(x) for x in r.split("-")] for r in ranges]

print(all_rules)
all_ranges = [r for sublist in all_rules.values() for r in sublist]

yours = [int(x) for x in yours[1].split(",")]
print(yours)

invalids = []
valid_tickets = []
for ticket in nearby[1:]:
    vals = [int(x) for x in ticket.split(",")]

    valid = True
    for val in vals:
        if not any((a <= val <= b) for a, b in all_ranges):
            invalids.append(val)
            valid = False

    # This block is preparation for part 2
    if valid:
        valid_tickets.append(vals)

print(sum(invalids))

rules_order = [list(name for name in all_rules.keys()
                    if all(any(a <= ticket[i] <= b for a, b in all_rules[name]) for ticket in valid_tickets))
               for i in range(len(yours))]
while not all(len(o) <= 1 for o in rules_order):
    got = set(r for sublist in rules_order if len(sublist) == 1 for r in sublist)
    rules_order = [curr_rules
                   if len(curr_rules) == 1 else
                   [r for r in curr_rules if r not in got] for curr_rules in rules_order]

rules_order = [a[0] for a in rules_order]
print(rules_order)

ans = 1
for i, rule in enumerate(rules_order):
    if rule.startswith("departure"):
        ans *= yours[i]
print(ans)
