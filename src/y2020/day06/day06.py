from functools import reduce

with open("data.in") as f:
    data = " ".join(line.strip() for line in f.readlines()).split("  ")

print(sum(len({a for a in group if a != ' '}) for group in data))
print(sum(len(reduce(lambda a, b: a & b, (set(p) for p in group.split()))) for group in data))
