with open("data.in") as f:
    data = [line.strip().split() for line in f.readlines()]

mem = dict()
mask = ["X"] * 36
for instr, _, value in data:
    if instr == "mask":
        mask = list(value)
    if instr.startswith("mem"):
        val = int(value)
        loc = int(instr.split("[")[1].split("]")[0])
        strval = list("{0:036b}".format(val))
        strval = "".join(a if b == "X" else b for a, b in zip(strval, mask))
        mem[loc] = int(strval, 2)
print(sum(mem.values()))


def set_bit(loc, j, param):
    locstr = "{0:036b}".format(loc)
    locstr = "".join(b if i != j else str(param) for i, b in enumerate(locstr))
    return int(locstr, 2)


mem = dict()
mask = ["X"] * 36
for instr, _, value in data:
    if instr == "mask":
        mask = list(value)
    if instr.startswith("mem"):
        val = int(value)
        loc = int(instr.split("[")[1].split("]")[0])
        strloc = list("{0:036b}".format(loc))
        strloc = "".join(a if b == "X" else "1" if b == "1" else a for a, b in zip(strloc, mask))
        locs = [int(strloc, 2)]
        for j, b in enumerate(mask):
            if b == "X":
                locs = [*[set_bit(loc, j, 0) for loc in locs], *[set_bit(loc, j, 1) for loc in locs]]
        for loc in locs:
            mem[loc] = val
print(sum(mem.values()))
