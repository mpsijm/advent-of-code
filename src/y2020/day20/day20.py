import math

with open("data.in") as f:
    data = [[x for x in block.replace("\n", " ").split()] for block in f.read().strip().split("\n\n")]

print(len(data))


def get_borders(tile):
    """Order (0-7): top-ltr, top-rtl, bottom-ltr, bottom-rtl, left-ttb, left-btt, right-ttb, right-btt"""
    return tile[0], tile[0][::-1], tile[-1], tile[-1][::-1], \
           "".join(line[0] for line in tile), \
           "".join(line[0] for line in tile[::-1]), \
           "".join(line[-1] for line in tile), \
           "".join(line[-1] for line in tile[::-1])


tiles = [get_borders(tile[2:]) for tile in data]
matches = {i: [None] * 8 for i in range(len(tiles))}
for i, t in enumerate(tiles):
    for j, u in enumerate(tiles):
        if i == j:
            continue
        for bor in (border for border in t if border in u):
            b = list(t).index(bor)
            c = list(u).index(bor)
            matches[i][b] = (j, c)
            matches[j][c] = (i, b)

ans = 1
for i, v in matches.items():
    if v.count(None) == 4:
        ans *= int(data[i][1][:-1])
print(ans)


def flip_v(tile):
    return tile[::-1]


def flip_h(tile):
    return [line[::-1] for line in tile]


def flip_d(tile):
    return ["".join(line[i] for line in tile) for i in range(len(tile))]


tile_side = int(math.sqrt(len(data)))
img_size = tile_side * (len(data[0][2]) - 2)
combined_image = [['.'] * img_size for _ in range(img_size)]
tile_order = [[None] * tile_side for _ in range(tile_side)]
new_borders = [[None] * tile_side for _ in range(tile_side)]
found = set()

for y in range(tile_side):
    for x in range(tile_side):
        if x == y == 0:
            # Just grab any corner as the first corner in (0, 0)
            tile_id = next(i for i, v in matches.items() if v.count(None) == 4)
            tile = data[tile_id][2:]
            if matches[tile_id][2] is None:  # bottom is None
                tile = flip_v(tile)
            if matches[tile_id][6] is None:  # right is None
                tile = flip_h(tile)
        if x > 0:
            i, t = tile_order[y][x - 1], new_borders[y][x - 1]
            bor = t[6]  # The tile to the right should match the right border of the tile to the left
            touching = [(j, u) for j, u in enumerate(tiles) if j not in found and bor in u]
            if len(touching) != 1:
                print("Panic!")
                print(x, y, touching, bor)
            for tile_id, u in touching:
                c = list(u).index(bor)
                tile = data[tile_id][2:]
                if c == 0:
                    tile = flip_d(tile)
                if c == 1:
                    tile = flip_d(flip_h(tile))
                if c == 2:
                    tile = flip_h(flip_d(tile))
                if c == 3:
                    tile = flip_h(flip_d(flip_h(tile)))
                if c == 5:
                    tile = flip_v(tile)
                if c == 6:
                    tile = flip_h(tile)
                if c == 7:
                    tile = flip_v(flip_h(tile))

        if x == 0 and y > 0:
            i, t = tile_order[y - 1][x], new_borders[y - 1][x]
            bor = t[2]  # The tile below should match the bottom of the tile above
            touching = [(j, u) for j, u in enumerate(tiles) if j not in found and bor in u]
            if len(touching) != 1:
                print("Panic!")
                print(x, y, touching)
            for tile_id, u in touching:
                c = list(u).index(bor)
                tile = data[tile_id][2:]
                if c == 1:
                    tile = flip_h(tile)
                if c == 2:
                    tile = flip_v(tile)
                if c == 3:
                    tile = flip_h(flip_v(tile))
                if c == 4:
                    tile = flip_d(tile)
                if c == 5:
                    tile = flip_h(flip_d(tile))
                if c == 6:
                    tile = flip_d(flip_h(tile))
                if c == 7:
                    tile = flip_h(flip_d(flip_h(tile)))

        found.add(tile_id)
        tile_order[y][x] = tile_id
        new_borders[y][x] = get_borders(tile)
        for j, line in enumerate(tile[1:-1]):
            for i, c in enumerate(line[1:-1]):
                combined_image[j + 8 * y][i + 8 * x] = c

combined_image = ["".join(line) for line in combined_image]
# combined_image = flip_d(combined_image)
if any(any(t is None for t in line) for line in tile_order):
    print(*combined_image, sep="\n")
    exit(1)

monster = [
    "                  # ",
    "#    ##    ##    ###",
    " #  #  #  #  #  #   ",
]
monster_size = sum(line.count("#") for line in monster)

monsters = 0
imgs = [combined_image]
while not monsters:
    for img in imgs:
        for y in range(len(img) - 2):
            for x in range(len(img[0]) - len(monster[0]) + 1):
                count = 0
                for j, ml in enumerate(monster):
                    for i, mc in enumerate(ml):
                        if mc == " ":
                            continue
                        if img[y + j][x + i] == "#":
                            count += 1
                if count == monster_size:
                    monsters += 1
    imgs = [
        *(flip_d(img) for img in imgs),
        *(flip_h(img) for img in imgs),
        *(flip_v(img) for img in imgs),
    ]

print(monsters)
print(sum(line.count("#") for line in combined_image) - monsters * monster_size)
