with open("data.in") as f:
    data = [int(x) for x in f.readlines()]

data = [*sorted(data), max(data) + 3]
ones = 0
threes = 0
prev = 0
for x in data:
    if x - prev == 1:
        ones += 1
    if x - prev == 3:
        threes += 1
    prev = x
print(ones * threes)

data = [0, *data]
to_check = {i: 0 for i in range(len(data))}
to_check[0] = 1
for i in range(len(data)):
    c = to_check[i]
    for j in range(1, 4):
        if i + j < len(data) and data[i + j] - data[i] <= 3:
            to_check[i + j] += c
print(c)

