from collections import defaultdict

from math import ceil, floor

with open("data.in") as f:
    data = [x.strip() for x in f.readlines()]

directions = {"e": (1, 0), "ne": (0.5, -1), "nw": (-0.5, -1), "w": (-1, 0), "sw": (-0.5, 1), "se": (0.5, 1)}
tiles = defaultdict(lambda: False)

for tile in data:
    i = 0
    x, y = 0, 0
    while i < len(tile):
        d = tile[i] if tile[i] in "we" else tile[i:i + 2]
        dx, dy = directions[d]
        x += dx
        y += dy
        i += len(d)
    tiles[(x, y)] = not tiles[(x, y)]

print(sum(tiles.values()))

for day in range(1, 101):
    prev_tiles = tiles.copy()
    min_x = floor(min(x for x, y in tiles.keys())) - 1
    max_x = ceil(max(x for x, y in tiles.keys())) + 1
    min_y = min(y for x, y in tiles.keys()) - 1
    max_y = max(y for x, y in tiles.keys()) + 1
    for y in range(min_y, max_y + 1):
        half = (y % 2) * 0.5
        for x in range(min_x, max_x + 1):
            x += half
            nes = sum(prev_tiles[(x + dx, y + dy)] for dx, dy in directions.values())
            if (nes == 0 or nes > 2) if prev_tiles[(x, y)] else nes == 2:
                tiles[(x, y)] = not tiles[(x, y)]
    # if day % 10 == 0:
    #     print(day, sum(tiles.values()))

print(sum(tiles.values()))
