with open("data.in") as f:
    data = [tuple(int(x) for x in block.split("\n")[1:]) for block in f.read().strip().split("\n\n")]

cards = sum(len(player) for player in data)


def play_game(a, b, recursive, first=False):
    history = set()
    round = 1
    while a and b:
        # print("Round " + str(round))
        # print(a)
        # print(b)
        if (a, b) in history:
            return a, b
        history.add((a, b))
        c = a[0]
        d = b[0]

        if recursive and c < len(a) and d < len(b):
            # print()
            # print("New subgame")
            e, f = play_game(a[1:c + 1], b[1:d + 1], recursive)
            player1_wins = bool(e)  # If player 1 has any number of cards, they won
            # print()
        else:
            player1_wins = c > d

        if player1_wins:
            a = (*a[1:], c, d)
            b = b[1:]
        else:
            a = a[1:]
            b = (*b[1:], d, c)

        round += 1

    if first:
        print(sum(c * (cards - i) for i, c in enumerate(a or b)))
    return a, b


a, b = data
play_game(a, b, recursive=False, first=True)
play_game(a, b, recursive=True, first=True)
