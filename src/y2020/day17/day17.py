with open("data.in") as f:
    data = [x.strip() for x in f.readlines()]


def get(levels, w, z, y, x):
    if w in levels:
        if z in levels[w]:
            if y in levels[w][z]:
                if x in levels[w][z][y]:
                    return levels[w][z][y][x]
    return "."


def put(levels, w, z, y, x, s):
    if w not in levels:
        levels[w] = dict()
    if z not in levels[w]:
        levels[w][z] = dict()
    if y not in levels[w][z]:
        levels[w][z][y] = dict()
    if x not in levels[w][z][y]:
        levels[w][z][y][x] = dict()
    levels[w][z][y][x] = s


def count_active(levels, w, z, y, x):
    count = 0
    for h in range(-1, 2):
        for i in range(-1, 2):
            for j in range(-1, 2):
                for k in range(-1, 2):
                    if h == 0 and i == 0 and j == 0 and k == 0:
                        continue
                    if get(levels, w + h, z + i, y + j, x + k) == "#":
                        count += 1
    return count


def run(part_b):
    for cycle in range(6):
        prev = {i: {j: {k: n.copy() for k, n in l.items()} for j, l in m.items()} for i, m in levels.items()}
        # print(*("".join(get(prev, 0, 0, y, x) for x in range(-1, 5)) for y in range(-1, 5)), sep="\n")
        ks = list(prev.keys())
        for w in [min(ks) - 1, *ks, max(ks) + 1] if part_b else [0]:
            ks = {x for y in prev.values() for x in y.keys()}
            for z in [min(ks) - 1, *ks, max(ks) + 1]:
                ks = {k for y in prev.values() for x in y.values() for k in x.keys()}
                for y in [min(ks) - 1, *ks, max(ks) + 1]:
                    ks = {k for y in prev.values() for x in y.values() for z in x.values() for k in z.keys()}
                    for x in [min(ks) - 1, *ks, max(ks) + 1]:
                        active = count_active(prev, w, z, y, x)
                        if get(prev, w, z, y, x) == "#" and not (2 <= active <= 3):
                            put(levels, w, z, y, x, ".")
                        if get(prev, w, z, y, x) == "." and active == 3:
                            put(levels, w, z, y, x, "#")

    print(sum((c == "#") for z in levels.values() for y in z.values() for x in y.values() for c in x.values()))


levels = {0: {0: {i: {j: x for j, x in enumerate(l)} for i, l in enumerate(data)}}}
run(False)
levels = {0: {0: {i: {j: x for j, x in enumerate(l)} for i, l in enumerate(data)}}}
run(True)
