with open("data.in") as f:
    data = [x.strip() for x in f.readlines()]

n = int(data[0])
buses = [int(x) if x != "x" else "x" for x in data[1].split(",") if x != "x"]
for i in range(n, 2 * n):
    modulos = [i % b == 0 for b in buses]
    if any(modulos):
        print((i - n) * buses[modulos.index(True)])
        break

buses = [int(x) if x != "x" else "x" for x in data[1].split(",")]

period = 1
ans = 0
for i, b in enumerate(buses):
    if b == "x":
        continue
    while (ans + i) % b > 0:
        ans += period
    period *= b
print(ans)
