with open("data.in") as f:
    data = [int(x) for x in f.readlines()]

for x in data:
    for y in data:
        if x + y == 2020:
            print(x * y)

for x in data:
    for y in data:
        for z in data:
            if x + y + z == 2020:
                print(x * y * z)
