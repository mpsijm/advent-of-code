package y2020.day02

import scala.io.Source

object Day02 {
  def main(args: Array[String]) {
    val data = Source.fromFile("src/" + Day02.getClass.getPackageName.replace('.', '/') + "/data.in").getLines().toList
      .map { line =>
        val splitted = line.split(" ")
        val lowHigh = splitted(0).split("-")
        (lowHigh(0).toInt, lowHigh(1).toInt, splitted(1).charAt(0), splitted(2))
      }
    println(data.count { case (low, high, l, password) =>
      val count = password.count(_ == l)
      low <= count && count <= high
    })
    println(data.count { case (low, high, l, password) =>
      (password.charAt(low - 1) == l) != (password.charAt(high - 1) == l)
    })
  }
}
