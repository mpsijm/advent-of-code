with open("data.in") as f:
    data = [x.split() for x in f.readlines()]

ans1 = 0
ans2 = 0
for counts, letter, password in data:
    low, high = [int(x) for x in counts.split("-")]
    l = letter[0]
    count = sum(1 for c in password if c == l)
    if low <= count <= high:
        ans1 += 1
    if (password[low - 1] == l) + (password[high - 1] == l) == 1:
        ans2 += 1
print(ans1)
print(ans2)
