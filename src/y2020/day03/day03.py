from functools import reduce
from operator import mul

with open("data.in") as f:
    data = [line.strip() for line in f.readlines()]

ans = [sum(1 for i, line in enumerate(data) if (i * h) % 1 == 0 and line[int(i * h) % len(line)] == '#')
       for h in [1, 3, 5, 7, 0.5]]
print(ans[1])
print(reduce(mul, ans, 1))

