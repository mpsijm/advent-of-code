import re

with open("data.in") as f:
    data = [passport.replace("\n", " ").strip() for passport in f.read().split("\n\n")]

req_fields = {"byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"}
valid_ecl = {"amb", "blu", "brn", "gry", "grn", "hzl", "oth"}


def is_valid_a(passport):
    fields = {field.split(":")[0] for field in passport.split(" ")}
    return sum(1 for field in fields if field in req_fields) == 7


def is_valid_b(passport):
    fields = [field.split(":") for field in passport.split(" ")]
    res = 0
    for field, value in fields:
        if field == "byr":
            res += bool(re.match("^[0-9]{4}$", value)) and 1920 <= int(value) <= 2002
        if field == "iyr":
            res += bool(re.match("^[0-9]{4}$", value)) and 2010 <= int(value) <= 2020
        if field == "eyr":
            res += bool(re.match("^[0-9]{4}$", value)) and 2020 <= int(value) <= 2030
        if field == "hgt":
            res += bool(re.match("^[0-9]{3}cm$", value)) and 150 <= int(value[:3]) <= 193 or \
                   bool(re.match("^[0-9]{2}in$", value)) and 59 <= int(value[:2]) <= 76
        if field == "hcl":
            res += bool(re.match("^#[0-9a-f]{6}$", value))
        if field == "ecl":
            res += value in valid_ecl
        if field == "pid":
            res += bool(re.match("^[0-9]{9}$", value))
    return res == 7


print(sum(1 for passport in data if is_valid_a(passport)))
print(sum(1 for passport in data if is_valid_a(passport) and is_valid_b(passport)))

