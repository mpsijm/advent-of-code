with open("data.in") as f:
    data = [(line[0], int(line[1:])) for line in (x.strip() for x in f.readlines())]

dirs = [(1, 0), (0, 1), (-1, 0), (0, -1)]
d = 0
x, y = 0, 0
for c, v in data:
    if c == "N":
        y -= v
    if c == "S":
        y += v
    if c == "E":
        x += v
    if c == "W":
        x -= v
    if c == "L":
        d = (d - v // 90) % 4
    if c == "R":
        d = (d + v // 90) % 4
    if c == "F":
        i, j = dirs[d]
        x += v * i
        y += v * j
print(x, y, dirs[d], abs(x) + abs(y))

x, y = 0, 0
xw, yw = 10, -1
for c, v in data:
    if c == "N":
        yw -= v
    if c == "S":
        yw += v
    if c == "E":
        xw += v
    if c == "W":
        xw -= v
    if c == "L":
        for _ in range(v // 90):
            yw, xw = -xw, yw
    if c == "R":
        for _ in range(v // 90):
            yw, xw = xw, -yw
    if c == "F":
        x += xw * v
        y += yw * v
print(x, y, xw, yw, abs(x) + abs(y))
