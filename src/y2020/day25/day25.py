with open("data.in") as f:
    data = [int(x) for x in f.readlines()]

mod = 20201227


def get_loops(subject, target):
    value = 1
    loops = 0
    while value != target:
        loops += 1
        value = (value * subject) % mod
    return loops


def transform(subject, loops):
    value = 1
    for _ in range(loops):
        value = (value * subject) % mod
    return value


one = get_loops(7, data[0])
two = get_loops(7, data[1])

print(one, two)
print(transform(data[1], one))
print(transform(data[0], two))
