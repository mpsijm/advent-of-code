with open("data.in") as f:
    data = [line.strip() for line in f.readlines()]


def find_between_parens(line, i):
    fristi = i + 1
    brackets = 1
    while brackets > 0:
        i += 1
        c = line[i]
        if c == "(":
            brackets += 1
        if c == ")":
            brackets -= 1
    return line[fristi:i], i


def parse_a(line):
    def update(v):
        nonlocal val
        if op == "":
            val = v
        if op == "+":
            val += v
        if op == "*":
            val *= v

    val = 0
    op = ""
    i = 0
    while i < len(line):
        c = line[i]
        if "0" <= c <= "9":
            update(int(c))
        if c == "+" or c == "*":
            op = c
        if c == "(":
            between_parens, i = find_between_parens(line, i)
            update(parse_a(between_parens))
        i += 1
    return val


def parse_b(line):
    def update(v):
        if op == "":
            values.append(v)
        if op == "+":
            values[-1] += v
        if op == "*":
            values.append(v)

    values = []
    op = ""
    i = 0
    while i < len(line):
        c = line[i]
        if "0" <= c <= "9":
            update(int(c))
        if c == "+" or c == "*":
            op = c
        if c == "(":
            between_parens, i = find_between_parens(line, i)
            update(parse_b(between_parens))
        i += 1

    ans = 1
    for value in values:
        ans *= value
    return ans


print(sum(parse_a(line) for line in data))
print(sum(parse_b(line) for line in data))
