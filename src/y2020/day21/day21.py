from functools import reduce

with open("data.in") as f:
    data = [line.strip().replace(")", "").split(" (contains ") for line in f.readlines()]

data = [(ings.split(" "), algs.split(", ")) for ings, algs in data]
all_algs = set(alg for _, algs in data for alg in algs)

possibles = set()
alg_ings = dict()
for alg in all_algs:
    possible = reduce(lambda a, b: a & b, (set(ings) for ings, algs in data if alg in algs))
    possibles |= possible
    alg_ings[alg] = possible

print(sum(ing not in possibles for ings, _ in data for ing in ings))

while any(len(ings) > 1 for ings in alg_ings.values()):
    got_alg = {alg for alg, ings in alg_ings.items() if len(ings) == 1}
    got_ing = {next(iter(ings)) for alg, ings in alg_ings.items() if len(ings) == 1}
    alg_ings = {alg: ings if alg in got_alg else {ing for ing in ings if ing not in got_ing}
                for alg, ings in alg_ings.items()}

print(",".join(next(iter(ings)) for _, ings in sorted(alg_ings.items())))
