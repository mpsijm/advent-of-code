from typing import List

with open("data.in") as f:
    data = [int(x) for x in f.read().strip()]

print(data)


class Link:
    val: int
    next: "Link"

    def __init__(self, val: int, prev=None):
        self.val = val
        if prev is not None:
            prev.set_next(self)

    def set_next(self, new_next):
        self.next = new_next

    def __str__(self):
        link = self
        acc = []
        for i in range(9):
            if i == 0:
                acc.append("(" + str(link.val) + ")")
            else:
                acc.append(str(link.val))
            link = link.next
        return "..., " + ", ".join(acc) + ", ..."


def play_game(n, moves):
    cups = [*data, *range(len(data) + 1, n + 1)]
    first_link = Link(cups[0])
    link_map: List[Link] = [None] * (n + 1)
    link_map[first_link.val] = first_link

    link = first_link
    for c in cups[1:]:
        new_link = Link(c, link)
        link_map[new_link.val] = new_link
        link = new_link
    link.set_next(first_link)
    # print(first_link)
    print()

    curr = first_link
    for i in range(moves):
        if i > 0 and i % 100000 == 0:
            print(f"\r{i}", end="")
        pickup = [curr.next, curr.next.next, curr.next.next.next]
        pickup_values = [p.val for p in pickup]
        dest = curr.val - 1
        if dest < 1:
            dest += n
        while dest in pickup_values:
            dest -= 1
            if dest < 1:
                dest += n
        dest_link = link_map[dest]
        # print(i, "\n", curr, "\n", pickup_values, dest, "\n", dest_link)
        curr.set_next(pickup[2].next)
        pickup[2].set_next(dest_link.next)
        dest_link.set_next(pickup[0])
        # print(" " + str(dest_link))

        curr = curr.next
    print("\r", end="")

    one_cup = link_map[1]
    print(one_cup)
    if n == len(data):
        ans = []
        link = link_map[1]
        while link.next.val != 1:
            link = link.next
            ans.append(str(link.val))
        print("".join(ans))
    else:
        print(one_cup.next.val * one_cup.next.next.val)


play_game(len(data), 100)
play_game(1_000_000, 10_000_000)
