with open("data.in") as f:
    data = [int(c) for c in f.read().split(",")]

last_turn = {c: i for i, c in enumerate(data, start=1)}
delta = {c: 0 for c in data}
last = data[-1]
for i in range(len(data) + 1, 30_000_001):
    if last not in delta:
        val = 0
    else:
        val = delta[last]
    if val in last_turn:
        delta[val] = i - last_turn[val]
    else:
        delta[val] = 0
    last_turn[val] = i
    last = val
    if i == 2020:
        print(val)
print(last)
