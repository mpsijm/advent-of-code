with open("data.in") as f:
    data = [x[:-2].replace("bags", "bag").split(" contain ") for x in f.readlines()]

bags = {holder: {} if holdings == "no other bag" else {holding[2:]: int(holding[0]) for holding in holdings.split(", ")}
        for holder, holdings in data}

todo = ["shiny gold bag"]
done = set(todo)
count = 0
while todo:
    current = todo.pop()
    holders = {holder: holdings.keys() for holder, holdings in bags.items()
               if any(current in holder for holder in holdings)}
    new_holders = [holder for holder, holdings in holders.items() if holder not in done]
    done = done.union(set(new_holders))
    count += len(new_holders)
    todo.extend(new_holders)
print(count)


def count_bags(current):
    return 1 + sum(count * count_bags(bag) for bag, count in bags[current].items())


print(count_bags("shiny gold bag") - 1)



# ### BRO-KEN ###
bag_count = {"shiny gold bag": 1}
todo = list(bag_count.keys())
while todo:
    current = todo.pop()
    current_count = bag_count[current]
    for holding, c in bags[current].items():
        if holding in bag_count:
            bag_count[holding] += c * current_count
        else:
            bag_count[holding] = c * current_count
        todo.append(holding)
print(sum(bag_count.values()) - 1)

exit(0)
todo = ["shiny gold bag"]
non_empty = set()
empty = set()
reverse_bags = {"shiny gold bag": []}
while todo:
    current = todo.pop()
    if bags[current]:
        non_empty.add(current)
    else:
        empty.add(current)
    for holding in bags[current].keys():
        todo.append(holding)
        if holding in reverse_bags:
            reverse_bags[holding].append(current)
        else:
            reverse_bags[holding] = [current]
print(empty)
print(non_empty)
print(reverse_bags)

bag_count = {bag: dict() for bag in empty}
todo = list(bag_count.keys())
while todo:
    current = todo.pop()
    print("----")
    print(current)
    current_counts = bag_count[current]
    for parent in reverse_bags[current]:
        c = bags[parent][current]
        if parent in bag_count:
            if current in bag_count[parent]:
                print("AAAA")
            else:
                new_dict = dict(bag_count[parent])
                for b, d in bags[current].items():
                    if b in new_dict:
                        new_dict[b] += c * d
                    else:
                        new_dict[b] = c * d
                bag_count[parent] = new_dict
        else:
            bag_count[parent] = {current: c, **{b: c * d for b, d in bags[current].items()}}
            print(bag_count[parent])
        todo = [parent, *todo]
    # print(bag_count)
print(sum(bag_count["shiny gold bag"].values()))
