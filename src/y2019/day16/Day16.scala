package y2019.day16

import scala.io.Source

object Day16 {
  val example1: String =
    """12345678""".stripMargin

  val example2: String =
    """80871224585914546619083218645595""".stripMargin

  def print2D[T](arr: Array[Array[T]]) = arr foreach { line => println(line mkString ",") }

  def main(args: Array[String]) {
    // val in = Source.fromString(example1)
    val in = Source.fromFile("src/" + Day16.getClass.getSimpleName.toLowerCase().dropRight(1) + "/data.in")
    val str = in.getLines().next()
    var list = str.split("").map(_.toInt).toList
    val pattern = List(0, 1, 0, -1)
    for (phase <- 1 to 100) {
      list = list.indices.map { newPos =>
        math.abs(list.zipWithIndex.map { case (digit, pos) =>
          digit * pattern((pos + 1) / (newPos + 1) % pattern.size)
        }.sum) % 10
      }.toList
    }
    println(list.slice(0, 8) mkString "")
  }
}
