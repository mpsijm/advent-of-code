package y2019.day16

import scala.io.Source

object Day16b {
  val example1: String =
    """03036732577212944063491565474664""".stripMargin

  def print2D[T](arr: Array[Array[T]]) = arr foreach { line => println(line mkString ",") }

  def main(args: Array[String]) {
    // val in = Source.fromString(example1)
    val in = Source.fromFile("src/" + Day16.getClass.getSimpleName.toLowerCase().dropRight(1) + "/data.in")
    val str = in.getLines().next()
    var list = str.split("").map(_.toInt)
    list = Range(0, 10000).flatMap(_ => list).toArray
    val offset = str.substring(0, 7).toInt
    for (phase <- 1 to 100) {
      for (i <- list.length - 2 to offset by -1) {
        list(i) += list(i + 1)
        list(i) %= 10
      }
    }
    println(list.slice(offset, offset + 8) mkString "")
  }


}
