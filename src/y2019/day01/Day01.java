package y2019.day01;

import java.io.*;
import java.util.*;

public class Day01 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new BufferedReader(new FileReader("src/y2019/day01/data.in")));
        solve(sc);
        sc.close();
    }

    private static void solve(Scanner sc) {
        long ans1 = 0;
        long ans2 = 0;
        while (sc.hasNext()) {
            int newFuel = sc.nextInt() / 3 - 2;
            ans1 += newFuel;
            ans2 += newFuel;
            long diff = newFuel;
            while (diff > 0) {
                diff /= 3;
                diff -= 2;
                if (diff < 0)
                    break;
                ans2 += diff;
            }
        }
        System.err.println(ans1);
        System.out.println(ans2);
    }
}
