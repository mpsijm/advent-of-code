package y2019.day06

import scala.collection.mutable
import scala.io.Source

object Day06 {
  val example1: String =
    """COM)B
      |B)C
      |C)D
      |D)E
      |E)F
      |B)G
      |G)H
      |D)I
      |E)J
      |J)K
      |K)L""".stripMargin
  val example2: String = example1 + "\nK)YOU\nI)SAN"

  def main(args: Array[String]) {
    // val in = Source.fromString(example2)
    val in = Source.fromFile("src/" + Day06.getClass.getSimpleName.toLowerCase().dropRight(1) + "/data.in")
    val orbits = new mutable.HashMap[String, List[String]]()
    val orbitsBoth = new mutable.HashMap[String, List[String]]()
    in.getLines().map { line => line.split("\\)").toList }.foreach { case List(a, b) =>
      orbits.put(a, b :: orbits.getOrElse(a, Nil))
      orbitsBoth.put(a, b :: orbitsBoth.getOrElse(a, Nil))
      orbitsBoth.put(b, a :: orbitsBoth.getOrElse(b, Nil))
    }
    println(dfs(orbits.toMap, "COM").values.sum) // part 1
    println(bfs(orbitsBoth.toMap, "YOU", "SAN")) // part 2
  }

  def bfs(orbits: Map[String, List[String]], start: String, end: String): Int = {
    val dist = mutable.HashMap[String, Int](start -> 0)
    val queue = mutable.Queue[String](start)
    while (queue.nonEmpty) {
      val curr = queue.dequeue()
      orbits.getOrElse(curr, Nil).foreach(o => {
        if (!dist.contains(o)) {
          queue.enqueue(o)
          dist.put(o, dist(curr) + 1)
        }
      })
    }
    dist(end) - 2
  }

  def dfs(orbits: Map[String, List[String]], start: String): Map[String, Int] = {
    val res = mutable.HashMap[String, Int](start -> 0)
    val stack = mutable.Stack[String](start)
    while (stack.nonEmpty) {
      val curr = stack.pop()
      orbits.getOrElse(curr, Nil).foreach(o => {
        stack.push(o)
        res.put(o, res(curr) + 1)
      })
    }
    res.toMap
  }
}

