# X = None
# Y = None
# Z = None
# edge = lambda x,y: False
# bi_edge = lambda x,y: False
# transitive = lambda x,y: False
# len_ = lambda x: False

import re

from pyDatalog import pyDatalog

example1 = """COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L
"""
example2 = example1 + "K)YOU\nI)SAN\n"
data = example2
# data = open('data.in', mode='r').read()

pyDatalog.create_terms('D,X,Y,Z, edge, bi_edge, dist, transitive, count')
exec(re.sub(r"^(.*)\)(.*)$", "+ edge('\\1','\\2')", data, flags=re.MULTILINE))

transitive(X, Y) <= edge(X, Y)
transitive(X, Z) <= edge(X, Y) & transitive(Y, Z)
print(len_(transitive(X, Y)))  # Part 1

# bi_edge(X, Y) <= edge(X, Y)
# bi_edge(X, Y) <= edge(Y, X)
# dist(X, Y, 1) <= bi_edge(X, Y)
# dist(X, Z, 2) <= dist(X, Y, 1) & bi_edge(Y, Z) & (X != Z)
# dist(X, Z, 3) <= dist(X, Y, 2) & bi_edge(Y, Z) & (X != Z)
# dist(X, Z, 4) <= dist(X, Y, 3) & bi_edge(Y, Z) & (X != Z)
# dist(X, Z, 5) <= dist(X, Y, 4) & bi_edge(Y, Z) & (X != Z)
# dist(X, Z, 6) <= dist(X, Y, 5) & bi_edge(Y, Z) & (X != Z)
# dist(X, Z, 7) <= dist(X, Y, 6) & bi_edge(Y, Z) & (X != Z)
# dist(X, Z, 8) <= dist(X, Y, 7) & bi_edge(Y, Z) & (X != Z)
# print(dist(X, Y, Z))
# print(dist("YOU", "SAN", X))  # Part 2
