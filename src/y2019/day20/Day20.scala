package y2019.day20

import y2019.day18.Day18.move

import scala.collection.mutable
import scala.io.Source

object Day20 {
  val example1: String =
    """         A
      |         A
      |  #######.#########
      |  #######.........#
      |  #######.#######.#
      |  #######.#######.#
      |  #######.#######.#
      |  #####  B    ###.#
      |BC...##  C    ###.#
      |  ##.##       ###.#
      |  ##...DE  F  ###.#
      |  #####    G  ###.#
      |  #########.#####.#
      |DE..#######...###.#
      |  #.#########.###.#
      |FG..#########.....#
      |  ###########.#####
      |             Z
      |             Z       """.stripMargin

  val example2: String =
    """                   A
      |                   A
      |  #################.#############
      |  #.#...#...................#.#.#
      |  #.#.#.###.###.###.#########.#.#
      |  #.#.#.......#...#.....#.#.#...#
      |  #.#########.###.#####.#.#.###.#
      |  #.............#.#.....#.......#
      |  ###.###########.###.#####.#.#.#
      |  #.....#        A   C    #.#.#.#
      |  #######        S   P    #####.#
      |  #.#...#                 #......VT
      |  #.#.#.#                 #.#####
      |  #...#.#               YN....#.#
      |  #.###.#                 #####.#
      |DI....#.#                 #.....#
      |  #####.#                 #.###.#
      |ZZ......#               QG....#..AS
      |  ###.###                 #######
      |JO..#.#.#                 #.....#
      |  #.#.#.#                 ###.#.#
      |  #...#..DI             BU....#..LF
      |  #####.#                 #.#####
      |YN......#               VT..#....QG
      |  #.###.#                 #.###.#
      |  #.#...#                 #.....#
      |  ###.###    J L     J    #.#.###
      |  #.....#    O F     P    #.#...#
      |  #.###.#####.#.#####.#####.###.#
      |  #...#.#.#...#.....#.....#.#...#
      |  #.#####.###.###.#.#.#########.#
      |  #...#.#.....#...#.#.#.#.....#.#
      |  #.###.#####.###.###.#.#.#######
      |  #.#.........#...#.............#
      |  #########.###.###.#############
      |           B   J   C
      |           U   P   P               """.stripMargin

  val example3: String =
    """             Z L X W       C
      |             Z P Q B       K
      |  ###########.#.#.#.#######.###############
      |  #...#.......#.#.......#.#.......#.#.#...#
      |  ###.#.#.#.#.#.#.#.###.#.#.#######.#.#.###
      |  #.#...#.#.#...#.#.#...#...#...#.#.......#
      |  #.###.#######.###.###.#.###.###.#.#######
      |  #...#.......#.#...#...#.............#...#
      |  #.#########.#######.#.#######.#######.###
      |  #...#.#    F       R I       Z    #.#.#.#
      |  #.###.#    D       E C       H    #.#.#.#
      |  #.#...#                           #...#.#
      |  #.###.#                           #.###.#
      |  #.#....OA                       WB..#.#..ZH
      |  #.###.#                           #.#.#.#
      |CJ......#                           #.....#
      |  #######                           #######
      |  #.#....CK                         #......IC
      |  #.###.#                           #.###.#
      |  #.....#                           #...#.#
      |  ###.###                           #.#.#.#
      |XF....#.#                         RF..#.#.#
      |  #####.#                           #######
      |  #......CJ                       NM..#...#
      |  ###.#.#                           #.###.#
      |RE....#.#                           #......RF
      |  ###.###        X   X       L      #.#.#.#
      |  #.....#        F   Q       P      #.#.#.#
      |  ###.###########.###.#######.#########.###
      |  #.....#...#.....#.......#...#.....#.#...#
      |  #####.#.###.#######.#######.###.###.#.#.#
      |  #.......#.......#.#.#.#.#...#...#...#.#.#
      |  #####.###.#####.#.#.#.#.###.###.#.###.###
      |  #.......#.....#.#...#...............#...#
      |  #############.#.#.###.###################
      |               A O F   N
      |               A A D   M                     """.stripMargin

  def print2D[T](arr: Array[Array[T]]): Unit = arr foreach { line => println(line mkString ",") }

  def getPortals(map: List[String], w: Int, h: Int) = {
    val locations = mutable.HashMap[String, List[(Int, Int)]]().withDefault(_ => Nil)
    val donutTopLeft = Range(2, w - 3).map(p => (p, p)).find(p => map(p._2)(p._1) == ' ').get
    val donutBotRight = (Range(w - 3, 2, -1) zip Range(h - 3, 2, -1)).find(p => map(p._2)(p._1) == ' ').get
    List((3, 1, w - 3), (3, h - 2, w - 3),
      (donutTopLeft._1 + 1, donutTopLeft._2, donutBotRight._1 - 1),
      (donutTopLeft._1 + 1, donutBotRight._2, donutBotRight._1 - 1)).foreach { case (x1, y, x2) =>
      (x1 until x2).foreach(x => {
        if (map(y)(x).isUpper) {
          val upsideDown = map(y + 1)(x).isUpper
          val k = "" + map(if (upsideDown) y else y - 1)(x) + map(if (upsideDown) y + 1 else y)(x)
          locations.put(k, (x, if (upsideDown) y - 1 else y + 1) :: locations(k))
        }
      })
    }
    List((3, 1, h - 3), (3, w - 2, h - 3),
      (donutTopLeft._2 + 1, donutTopLeft._1, donutBotRight._2 - 1),
      (donutTopLeft._2 + 1, donutBotRight._1, donutBotRight._2 - 1)).foreach { case (y1, x, y2) =>
      (y1 to y2).foreach(y => {
        if (map(y)(x).isUpper) {
          val upsideDown = map(y)(x + 1).isUpper
          val k = "" + map(y)(if (upsideDown) x else x - 1) + map(y)(if (upsideDown) x + 1 else x)
          locations.put(k, (if (upsideDown) x - 1 else x + 1, y) :: locations(k))
        }
      })
    }

    val start = locations("AA").head
    val end = locations("ZZ").head
    locations.remove("AA")
    locations.remove("ZZ")
    val portals = locations.map(t => (t._2.head, t._2.tail.head)) ++ locations.map(t => (t._2.tail.head, t._2.head))
    val pred = (m: ((Int, Int), (Int, Int))) => m match {
      case ((x1, y1), _) => x1 == 2 || y1 == 2 || x1 == w - 3 || y1 == h - 3
    }
    (start, end, portals.filter(pred), portals.filterNot(pred))
  }

  def main(args: Array[String]) {
//        val in = Source.fromString(example3)
    val in = Source.fromFile("src/" + Day20.getClass.getSimpleName.toLowerCase().dropRight(1) + "/data.in")
    val preMap = in.getLines().toList
    val h = preMap.size
    val w = preMap.maxBy(_.length).length // My editor automatically trims off trailing spaces
    val map = preMap.map(l => l.padTo(w, ' '))
    val (start, end, portalsUp, portalsDown) = getPortals(map, w, h)

    val queue = mutable.Queue[(Int, (Int, Int))]()
    val nextQueue = mutable.Queue[(Int, (Int, Int))]((0, start))
    var dist = 0
    var done = false
    val visited = mutable.HashMap[Int, Array[Array[Boolean]]]()
    visited(0) = Array.ofDim[Boolean](h, w)
    visited(nextQueue.head._1)(nextQueue.head._2._2)(nextQueue.head._2._1) = true
    while (nextQueue.nonEmpty && !done) {
      while (nextQueue.nonEmpty)
        queue.enqueue(nextQueue.dequeue())
      dist += 1

      while (queue.nonEmpty) {
        val (level, pos) = queue.dequeue()
        val currentPortal = if (portalsDown.contains(pos)) {
          println(s"Taking portal $pos from level $level to ${level + 1}")
          List((level + 1, portalsDown(pos)))
        } else if (level > 0 && portalsUp.contains(pos)) {
          println(s"Taking portal $pos from level $level to ${level - 1}")
          List((level - 1, portalsUp(pos)))
        } else Nil
        ((1 to 4).map(dir => (level, move(pos, dir))) ++ currentPortal).foreach { case n@(newLevel, newPos) =>
          val c = map(newPos._2)(newPos._1)
          if (!visited.contains(newLevel))
            visited.put(newLevel, Array.ofDim[Boolean](h, w))
          if (!visited(newLevel)(newPos._2)(newPos._1) && c == '.') {
            nextQueue.enqueue(n)
            visited(newLevel)(newPos._2)(newPos._1) = true
            if (newPos == end && newLevel == 0) {
              println(dist)
              done = true
            }
          }
        }
      }
    }
  }
}
