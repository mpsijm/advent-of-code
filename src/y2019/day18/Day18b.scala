package y2019.day18

import scala.collection.mutable
import scala.io.Source

object Day18b {
  val example1: String =
    """###############
      |#d.ABC.#.....a#
      |######=#>######
      |###############
      |######?#@######
      |#b.....#.....c#
      |###############""".stripMargin

  val example2: String =
    """#############
      |#DcBa.#.GhKl#
      |#.###=#>#I###
      |#e#d#####j#k#
      |###C#?#@###J#
      |#fEbA.#.FgHi#
      |#############""".stripMargin

  val example3: String =
    """#############
      |#g#f.D#..h#l#
      |#F###e#E###.#
      |#dCba=#>BcIJ#
      |#############
      |#nK.L?#@G...#
      |#M###N#H###.#
      |#o#m..#i#jk.#
      |#############""".stripMargin

  def move(pos: (Int, Int), dir: Int): (Int, Int) = {
    val (x, y) = pos
    dir match {
      case 1 => (x, y - 1)
      case 2 => (x, y + 1)
      case 3 => (x - 1, y)
      case 4 => (x + 1, y)
    }
  }

  def getLocOf(map: Seq[String], char: Char): (Int, Int) =
    map.indices.flatMap(y => map(y).indices.map((_, y)).filter { case (x, y) => map(y)(x) == char }).head

  def print2D[T](arr: Array[Array[T]]): Unit = arr foreach { line => println(line mkString ",") }

  def getDependencies(map: List[String], h: Int, w: Int, n: Int): Array[Array[Boolean]] = {
    val dependencies = Array.ofDim[Boolean](n + 4, n + 4)
    "=>?@".foreach(start => {
      val queue = mutable.Queue[((Int, Int), List[Char])]((getLocOf(map, start), Nil))
      val visited = Array.ofDim[Boolean](h, w)
      visited(queue.head._1._2)(queue.head._1._1) = true
      while (queue.nonEmpty) {
        val (pos, doors) = queue.dequeue()
        (1 to 4).foreach(dir => {
          val newPos = move(pos, dir)
          val c = map(newPos._2)(newPos._1)
          if (!visited(newPos._2)(newPos._1) && c != '#') {
            if (c.isUpper)
              queue.enqueue((newPos, c.toLower :: doors))
            else
              queue.enqueue((newPos, doors))
            visited(newPos._2)(newPos._1) = true
            if (c.isLower) {
              doors.foreach(d => dependencies(c.toUpper - '=')(d - 'a' + 4) = true)
            }
          }
        })
      }
    })
    dependencies
  }

  def getDistances(map: List[String], h: Int, w: Int, n: Int): Array[Array[Int]] = {
    val distances = Array.fill[Int](n + 4, n + 4)(-1)
    ('=' until ('A' + n).toChar).foreach(start => {
      val queue = mutable.Queue[(Int, Int)]()
      val nextQueue = mutable.Queue[(Int, Int)](getLocOf(map, start.toLower))

      var dist = 0
      val visited = Array.ofDim[Boolean](h, w)
      visited(nextQueue.head._2)(nextQueue.head._1) = true
      while (nextQueue.nonEmpty) {
        while (nextQueue.nonEmpty)
          queue.enqueue(nextQueue.dequeue())
        dist += 1

        while (queue.nonEmpty) {
          val pos = queue.dequeue()
          (1 to 4).foreach(dir => {
            val newPos = move(pos, dir)
            val c = map(newPos._2)(newPos._1)
            if (!visited(newPos._2)(newPos._1) && c != '#') {
              nextQueue.enqueue(newPos)
              visited(newPos._2)(newPos._1) = true
              if (c.isLower || ('=' <= c && c <= '@')) {
                distances(start - '=')(c.toUpper - '=') = dist
              }
            }
          })
        }
      }
    })
    distances
  }

  def main(args: Array[String]) {
    //    val in = Source.fromString(example2)
    val in = Source.fromFile("src/" + Day18.getClass.getSimpleName.toLowerCase().dropRight(1) + "/data_b.in")
    val map = in.getLines().toList
    val h = map.length
    val w = map.head.length
    val n = (map mkString "" filter (_.isLower)).length // Number of keys

    val dependencies = getDependencies(map, h, w, n)
    print2D(dependencies)
    //    val keysForGrabs = dependencies.drop(4).zipWithIndex.filter(t => t._1.forall(!_)).map(t => (t._2 + 'a').toChar).toList
    //    println(keysForGrabs)
    //    val uselessKeys = ('a' until ('a' + n).toChar).filter(c => ('a' until ('a' + n).toChar).forall(d => !dependencies(d - ']')(c - ']'))).toList
    //    println(uselessKeys)
    val distances = getDistances(map, h, w, n)
    print2D(distances)
    //    val keyLocs = ('a' until ('a' + n).toChar).map(c => getLocOf(map, c))
    //    println(keyLocs)

    val masks = (0 until n).map(1 << _)
    val fullMask = masks.reduce(_ | _)
    val memo = mutable.HashMap[(Int, (Int, Int, Int, Int)), Int]()
    val queue = mutable.PriorityQueue[(Int, (Int, Int, Int, Int), Int)]((0, (0, 1, 2, 3), 0))(Ordering.by(t => -t._3))
    while (queue.nonEmpty) {
      val (keys, currs, dist) = queue.dequeue()
      if (keys == fullMask) {
        println(dist)
      }
      if (!memo.contains((keys, currs))) {
        memo.put((keys, currs), dist)
        val reachableKeys = ('a' until ('a' + n).toChar).filter(c => (keys & masks(c - 'a')) == 0 && ('a' until ('a' + n).toChar).forall(d => ((keys & masks(d - 'a')) > 0) || !dependencies(c - ']')(d - ']')))
        reachableKeys.foreach(k => {
          val reachableFrom = (0 to 3).find(s => distances(s)(k - ']') >= 0).get
          val newCurrs: (Int, Int, Int, Int) = (reachableFrom match {
            case 0 => (k - ']', currs._2, currs._3, currs._4)
            case 1 => (currs._1, k - ']', currs._3, currs._4)
            case 2 => (currs._1, currs._2, k - ']', currs._4)
            case 3 => (currs._1, currs._2, currs._3, k - ']')
          })
          queue.enqueue((keys | masks(k - 'a'), newCurrs, dist + distances(currs.productElement(reachableFrom).asInstanceOf[Int])(k - ']')))
        })
      }
    }
    println(memo.size)
  }
}
