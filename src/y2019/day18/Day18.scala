package y2019.day18

import scala.collection.mutable
import scala.io.Source

object Day18 {
  val example1: String =
    """#########
      |#b.A.@.a#
      |#########""".stripMargin

  val example2: String =
    """########################
      |#f.D.E.e.C.b.A.@.a.B.c.#
      |######################.#
      |#d.....................#
      |########################""".stripMargin

  val example3: String =
    """########################
      |#...............b.C.D.f#
      |#.######################
      |#.....@.a.B.c.d.A.e.F.g#
      |########################""".stripMargin

  val example4: String =
    """#################
      |#i.G..c...e..H.p#
      |########.########
      |#j.A..b...f..D.o#
      |########@########
      |#k.E..a...g..B.n#
      |########.########
      |#l.F..d...h..C.m#
      |#################""".stripMargin

  def move(pos: (Int, Int), dir: Int): (Int, Int) = {
    val (x, y) = pos
    dir match {
      case 1 => (x, y - 1)
      case 2 => (x, y + 1)
      case 3 => (x - 1, y)
      case 4 => (x + 1, y)
    }
  }

  def getLocOf(map: Seq[String], char: Char): (Int, Int) =
    map.indices.flatMap(y => map(y).indices.map((_, y)).filter { case (x, y) => map(y)(x) == char }).head

  def print2D[T](arr: Array[Array[T]]): Unit = arr foreach { line => println(line mkString ",") }

  def getDependencies(map: List[String], h: Int, w: Int, n: Int): Array[Array[Boolean]] = {
    val dependencies = Array.ofDim[Boolean](n + 1, n + 1)
    val queue = mutable.Queue[((Int, Int), List[Char])]((getLocOf(map, '@'), Nil))
    val visited = Array.ofDim[Boolean](h, w)
    visited(queue.head._1._2)(queue.head._1._1) = true
    while (queue.nonEmpty) {
      val (pos, doors) = queue.dequeue()
      (1 to 4).foreach(dir => {
        val newPos = move(pos, dir)
        val c = map(newPos._2)(newPos._1)
        if (!visited(newPos._2)(newPos._1) && c != '#') {
          if (c.isUpper)
            queue.enqueue((newPos, c.toLower :: doors))
          else
            queue.enqueue((newPos, doors))
          visited(newPos._2)(newPos._1) = true
          if (c.isLower || c.isUpper) {
            doors.foreach(d => dependencies(c.toUpper - '@')(d - 'a' + 1) = true)
          }
        }
      })
    }
    dependencies
  }

  def getDistances(map: List[String], h: Int, w: Int, n: Int): Array[Array[Int]] = {
    val distances = Array.ofDim[Int](n + 1, n + 1)
    ('@' until ('A' + n).toChar).foreach(start => {
      val queue = mutable.Queue[(Int, Int)]()
      val nextQueue = mutable.Queue[(Int, Int)](getLocOf(map, start.toLower))

      var dist = 0
      val visited = Array.ofDim[Boolean](h, w)
      visited(nextQueue.head._2)(nextQueue.head._1) = true
      while (nextQueue.nonEmpty) {
        while (nextQueue.nonEmpty)
          queue.enqueue(nextQueue.dequeue())
        dist += 1

        while (queue.nonEmpty) {
          val pos = queue.dequeue()
          (1 to 4).foreach(dir => {
            val newPos = move(pos, dir)
            val c = map(newPos._2)(newPos._1)
            if (!visited(newPos._2)(newPos._1) && c != '#') {
              nextQueue.enqueue(newPos)
              visited(newPos._2)(newPos._1) = true
              if (c.isLower || c == '@') {
                distances(start - '@')(c.toUpper - '@') = dist
              }
            }
          })
        }
      }
    })
    distances
  }

  def main(args: Array[String]) {
    //    val in = Source.fromString(example4)
    val in = Source.fromFile("src/" + Day18.getClass.getSimpleName.toLowerCase().dropRight(1) + "/data.in")
    val map = in.getLines().toList
    val h = map.length
    val w = map.head.length
    val n = (map mkString "" filter (_.isLower)).length // Number of keys

    val dependencies = getDependencies(map, h, w, n)
    print2D(dependencies)
    //    val keysForGrabs = dependencies.drop(1).zipWithIndex.filter(t => t._1.forall(!_)).map(t => (t._2 + 'a').toChar).toList
    //    println(keysForGrabs)
    //    val uselessKeys = ('a' until ('a' + n).toChar).filter(c => ('a' until ('a' + n).toChar).forall(d => !dependencies(d - '`')(c - '`'))).toList
    //    println(uselessKeys)
    val distances = getDistances(map, h, w, n)
    print2D(distances)
    //    val keyLocs = ('a' until ('a' + n).toChar).map(c => getLocOf(map, c))
    //    println(keyLocs)

    val masks = (0 until n).map(1 << _)
    val fullMask = masks.reduce(_ | _)
    val memo = mutable.HashMap[Int, Int]()
    val queue = mutable.PriorityQueue[(Int, Int, Int)]((0, 0, 0))(Ordering.by(t => -t._3))
    while (queue.nonEmpty) {
      val (keys, curr, dist) = queue.dequeue()
      if (keys == fullMask) {
        println(dist)
      }
      val memoKey = keys | (curr << 26)
      if (!memo.contains(memoKey)) {
        memo.put(memoKey, dist)
        val reachableKeys = ('a' until ('a' + n).toChar).filter(c => (keys & masks(c - 'a')) == 0 && ('a' until ('a' + n).toChar).forall(d => ((keys & masks(d - 'a')) > 0) || !dependencies(c - '`')(d - '`')))
        reachableKeys.foreach(k => queue.enqueue((keys | masks(k - 'a'), k - '`', dist + distances(curr)(k - '`'))))
      }
    }
    println(memo.size)
  }
}
