package y2019.day22

import scala.io.Source

object Day22a {
  def main(args: Array[String]) {
    val in = Source.fromFile("src/" + Day22.getClass.getSimpleName.toLowerCase().dropRight(1) + "/data.in")
    val n = 10007
    val ops = in.getLines()
    var pos = 2019 // Expected: 6794
    val cut = "cut "
    val increment = "deal with increment "
    ops.foreach {
      case "deal into new stack" =>
        pos = n - pos - 1
      case s if s startsWith cut =>
        val m = -s.substring(cut.length).toInt + n
        pos = (pos + m) % n
      case s if s startsWith increment =>
        val m = s.substring(increment.length).toInt
        pos = (pos * m) % n
    }
    println(pos)
  }
}
