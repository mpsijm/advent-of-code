package y2019.day22

import scala.io.Source

object Day22b {
  val example1: String =
    """deal with increment 7
      |deal into new stack
      |deal into new stack""".stripMargin // Result with n = 11: 0,8,5,2,10,7,4,1,9,6,3

  val example4: String =
    """deal into new stack
      |cut -2
      |deal with increment 7
      |cut 8
      |cut -4
      |deal with increment 7
      |cut 3
      |deal with increment 9
      |deal with increment 3
      |cut -1""".stripMargin // Result with n = 11: 1,8,4,0,7,3,10,6,2,9,5

  def main(args: Array[String]) {
    //    val in = Source.fromString(example4)
    //    val n = 11
    //    var pos = 0L // Expected: 1
    val in = Source.fromFile("src/" + Day22.getClass.getSimpleName.toLowerCase().dropRight(1) + "/data.in")
    //    val n = BigInt(10007)
    //    var pos = BigInt(6794) // Expected: 2019
    //    val times = BigInt(5004) // Because it looped around after 5003 times
    val n = BigInt(119315717514047L)
    var pos = BigInt(2020)
    val times = BigInt(101741582076661L)
    val ops = in.getLines().toList
    val cut = "cut "
    val increment = "deal with increment "
    val initPos = pos

    // Let's build a linear function from all the actions
    var (a, b) = (BigInt(1), BigInt(0))
    ops.reverse.foreach { s =>
      s match {
        case "deal into new stack" =>
          pos = n - 1 - pos
          b += 1
          a *= -1
          b *= -1
        case s if s startsWith cut =>
          val m = s.substring(cut.length).toInt + n
          pos = (pos + m) % n
          b += m
        case s if s startsWith increment =>
          val m = s.substring(increment.length).toInt
          val inv = inversemod(m, n)
          pos = (pos * inv) % n
          a *= inv
          b *= inv
      }
      a %= n
      b %= n
      if (a < 0) a += n
      if (b < 0) b += n
    }

    // Apply that linear function {times} times to itself
    // f(x) = a*x + b
    // f^2(x) = a*(a*x + b) + b
    //        = a*a*x + a*b + b
    // f^3(x) = a*a*a*x + a*a*b + a*b + b
    //        = a*a*a*x + (a*a + a + 1)*b
    // f^4(x) = a*a*a*a*x + (a*a*a + a*a + a + 1)*b
    // f^{times)(x) = a^{times} * x + b * SUM_{i=0}^{times-1} a^i
    //              = a^{times} * x + b * (a^{times} - 1) / (a - 1)   // https://www.wolframalpha.com/input/?i=%E2%88%91_%7Bi%3D0%7D%5E%7Bt-1%7D+a%5Ei
    println((powmod(a, times, n) * initPos + b * (powmod(a, times, n) - 1) * inversemod(a - 1, n)) % n)
  }

  def powmod(base: BigInt, exp: BigInt, m: BigInt): BigInt = {
    var x = base
    var y = exp
    var r = BigInt(1)
    while (y > 0) {
      if (y % 2 == 1) r = (r * x) % m
      x = (x * x) % m // squaring the base
      y /= 2
    }
    r
  }

  // https://en.wikipedia.org/wiki/Extended_Euclidean_algorithm#Modular_integers
  def inversemod(a: BigInt, m: BigInt): BigInt = {
    var t = BigInt(0)
    var newT = BigInt(1)
    var r = m
    var newR = a
    while (newR != 0) {
      val quotient = r / newR
      val (a, b) = (newT, t - quotient * newT)
      t = a
      newT = b
      val (c, d) = (newR, r - quotient * newR)
      r = c
      newR = d
    }
    if (r > 1) throw new RuntimeException(a + " is not invertible")
    if (t < 0) t += m
    t
  }

}
