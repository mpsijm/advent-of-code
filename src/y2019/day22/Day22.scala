package y2019.day22

import scala.io.Source

object Day22 {
  val example1: String =
    """deal with increment 7
      |deal into new stack
      |deal into new stack""".stripMargin

  val example2: String =
    """cut 6
      |deal with increment 7
      |deal into new stack""".stripMargin

  val example3: String =
    """deal with increment 7
      |deal with increment 9
      |cut -2""".stripMargin

  val example4: String =
    """deal into new stack
      |cut -2
      |deal with increment 7
      |cut 8
      |cut -4
      |deal with increment 7
      |cut 3
      |deal with increment 9
      |deal with increment 3
      |cut -1""".stripMargin

  def main(args: Array[String]) {
//        val in = Source.fromString(example4)
//        val n = 11
    val in = Source.fromFile("src/" + Day22.getClass.getSimpleName.toLowerCase().dropRight(1) + "/data.in")
    val n = 10007
    val ops = in.getLines()
    var cards = Array.ofDim[Long](n)
    (0 until n).foreach(i => cards(i) = i)
    ops.foreach(s => {
      println(cards mkString " ")
      val temp = Array.ofDim[Long](n)
      s match {
        case "deal into new stack" =>
          (0 until n).foreach(i => temp(i) = cards(n - i - 1))
        case s if s startsWith "cut" =>
          val m = s.substring("cut ".length).toInt + n
          (0 until n).foreach(i => temp(i) = cards((i + m) % n))
        case s if s startsWith "deal with increment" =>
          val m = s.substring("deal with increment ".length).toInt
          (0 until n).foreach(i => temp((i * m) % n) = cards(i))
      }
      cards = temp
    })
    println(cards mkString " ")
    if (n > 2019)
      println(cards indexWhere (_ == 2019))
  }
}
