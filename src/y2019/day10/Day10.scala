package y2019.day10

import scala.io.Source

object Day10 {
  val example1: String =
    """.#..#
      |.....
      |#####
      |....#
      |...##""".stripMargin
  val example2: String =
    """......#.#.
      |#..#.#....
      |..#######.
      |.#.#.###..
      |.#..#.....
      |..#....#.#
      |#..#....#.
      |.##.#..###
      |##...#..#.
      |.#....####""".stripMargin
  val example3: String =
    """.#..#..###
      |####.###.#
      |....###.#.
      |..###.##.#
      |##.##.#.#.
      |....###..#
      |..#.#..#.#
      |#..#.#.###
      |.##...##.#
      |.....#.#..""".stripMargin
  val example4: String =
    """.#..##.###...#######
      |##.############..##.
      |.#.######.########.#
      |.###.#######.####.#.
      |#####.##.#.##.###.##
      |..#####..#.#########
      |####################
      |#.####....###.#.#.##
      |##.#################
      |#####.##.###..####..
      |..######..##.#######
      |####.##.####...##..#
      |.#####..#.######.###
      |##...#.##########...
      |#.##########.#######
      |.####.#.###.###.#.##
      |....##.##.###..#####
      |.#.#.###########.###
      |#.#.#.#####.####.###
      |###.##.####.##.#..##""".stripMargin

  class asInt(b: Boolean) {
    def toInt = if (b) 1 else 0
  }

  implicit def convertBooleanToInt(b: Boolean) = new asInt(b)

  private def processAsteroid(map: List[String], hidden: Array[Array[Boolean]], w: Int, h: Int, x: Int, y: Int, dx: Int, dy: Int) = {
    var newY = y + dy
    var newX = x + dx
    var res = false
    if (!hidden(newY)(newX))
      while (newX >= 0 && newY >= 0 && newX < w && newY < h) {
        if (res)
          hidden(newY)(newX) = true
        if (map(newY)(newX) == '#')
          res = true
        newX += dx
        newY += dy
      }
    res
  }

  def maxVisibility(map: List[String], x: Int, y: Int): Int = {
    val w = map.head.length
    val h = map.length
    val horiz = Range(0, x).exists(i => map(y)(i) == '#').toInt +
      Range(x + 1, w).exists(i => map(y)(i) == '#').toInt +
      Range(0, y).exists(i => map(i)(x) == '#').toInt +
      Range(y + 1, w).exists(i => map(i)(x) == '#').toInt
    //    val diag = Range(1, x min y).exists(i => map(y - i)(x - i) == '#').toInt +
    //      Range(1, (w - x) min y).exists(i => map(y - i)(x + i) == '#').toInt +
    //      Range(1, x min (h - y)).exists(i => map(y + i)(x - i) == '#').toInt +
    //      Range(1, (w - x) min (h - y)).exists(i => map(y + i)(x + i) == '#').toInt
    val hidden = Array.ofDim[Boolean](h, w)
    var count = 0
    for (dx <- 1 until w - x) {
      for (dy <- 1 until h - y) {
        if (processAsteroid(map, hidden, w, h, x, y, dx, dy)) count += 1
      }
    }
    for (dx <- -1 to -x by -1) {
      for (dy <- 1 until h - y) {
        if (processAsteroid(map, hidden, w, h, x, y, dx, dy)) count += 1
      }
    }
    for (dx <- 1 until w - x) {
      for (dy <- -1 to -y by -1) {
        if (processAsteroid(map, hidden, w, h, x, y, dx, dy)) count += 1
      }
    }
    for (dx <- -1 to -x by -1) {
      for (dy <- -1 to -y by -1) {
        if (processAsteroid(map, hidden, w, h, x, y, dx, dy)) count += 1
      }
    }
    Range(0, w) foreach {
      hidden(y)(_) = true
    }
    Range(0, h) foreach {
      hidden(_)(x) = true
    }

    val locations = (map.zip(hidden).zipWithIndex.flatMap { case ((mr, hr), y) =>
      mr.zip(hr).zipWithIndex.map { case ((m, h), x) =>
        if (m == '#' && !h) (x, y) else (-1, -1)
      }
    }.filter(_ != (-1, -1)) ++ List((x - 1, y), (x + 1, y), (x, y - 1), (x, y + 1))).sortBy { case (nx, ny) =>
      val res2 = math.atan2((ny - y).toDouble, (nx - x).toDouble) + math.Pi / 2.0
      val res = if (res2 < 0) res2 + 2 * math.Pi else res2
      res
    }
    println(locations)
    println(locations foreach { case l@(nx, ny) =>
      val res2 = math.atan2((ny - y).toDouble, (nx - x).toDouble) + math.Pi / 2.0
      val res = if (res2 < 0) res2 + 2 * math.Pi else res2
      println(s"$l    $nx,$ny    $res")
    })
    println(locations(199))

    //        map zip hidden foreach { case (mr, hr) => mr zip hr foreach { case (m, h) => if (m == '#' && !h) print('#') else print('.') }; println }
    val newCount = map zip hidden map { case (mr, hr) => mr zip hr count { case (m, h) => m == '#' && !h } }
    if (newCount.sum + horiz == 276) println(s"$x $y")
    newCount.sum + horiz // + diag
  }

  def main(args: Array[String]) {
    //    val in = Source.fromString(example4)
    val in = Source.fromFile("src/" + Day10.getClass.getSimpleName.toLowerCase().dropRight(1) + "/data.in")
    val map = in.getLines().toList
    //    val res = map.zipWithIndex.flatMap { case (line, y) => line.zipWithIndex.map { case (curr, x) =>
    //      if (curr == '.') 0 else maxVisibility(map, x, y)
    //    }
    //    }.max
    //    println(res)
    //    println(maxVisibility(map, 6, 3)) // example 3
    println(maxVisibility(map, 17, 22)) // full input
  }

}
