package y2019.day19

import y2019.day09.Day09

import scala.collection.mutable
import scala.io.Source

object Day19 {
  def print2D[T](arr: Array[Array[T]]): Unit = arr foreach { line => println(line mkString "") }

  def main(args: Array[String]) {
    val in = Source.fromFile("src/" + Day19.getClass.getSimpleName.toLowerCase().dropRight(1) + "/data.in")
    val prog = in.getLines().next().split(",").map(_.toLong).padTo(100000, 0L)

    val map = (0 until 50).map(x => (0 until 50).map(y => getBiem(prog, x, y)))
    map.foreach(l => println(l.map(c => if (c == 1) '#' else '.') mkString "")) // Visualize biem
    println(map.map(_.sum).sum) // Part 1

    var (x, y) = (2, 3) // For some reason, my biem has a gap between (0,0) and (1,3)
    var yay = false
    while (!yay) {
      getBiem(prog, x, y) match {
        case 0 => y += 1
        case 1 =>
          if (x > 100) {
            if (getBiem(prog, x - 99, y + 99) == 1) {
              yay = true
              println((x - 99) * 10000 + y) // Part 2
            }
          }
          x += 1
      }
    }
  }

  private def getBiem(prog: Array[Long], x: Int, y: Int) = {
    new Day09.IntcodeComputer(prog.clone(), mutable.Queue[Long](x, y)).simulate()._1.head
  }
}
