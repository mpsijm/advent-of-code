package y2019.day07

import java.io._
import java.util.Scanner

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

object Day07 {
  def main(args: Array[String]) {
    val sc = new Scanner(new BufferedReader(new FileReader("src/" + Day07.getClass.getSimpleName.toLowerCase().dropRight(1) + "/data.in")))
    //    val sc = new Scanner("3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5");
    solve(sc)
    sc.close()
  }

  def solve(sc: Scanner) {
    val prog = sc.next().split(",").map(_.toInt)

    val res1 = permutations(List(0, 1, 2, 3, 4)).map(perm =>
      perm.map(phase => phase).foldLeft(0)((prev, phase) =>
        new IntcodeComputer(prog.clone(), mutable.Queue(phase, prev)).simulate()._1(0)
      )
    ).max
    println(res1)

    val res2 = permutations(List(5, 6, 7, 8, 9)).map(perm => {
      val computers = perm.map(phase => new IntcodeComputer(prog.clone(), mutable.Queue(phase)))
      var res = 0
      var halt = false
      while (!halt) {
        computers.foreach(computer => {
          computer.simulate(mutable.Queue(res)) match {
            case (out, h) =>
              res = out(0)
              halt = h
          }
        })
      }
      res
    }).max
    println(res2)
  }

  def permutations(list: List[Int]): List[List[Int]] = list match {
    case a :: Nil => List(a :: Nil)
    case _ => list.flatMap(i => permutations(list.diff(List(i))).map(l => i :: l))
  }

  class IntcodeComputer(prog: Array[Int], input: mutable.Queue[Int]) {
    var counter = 0

    def simulate(localInput: mutable.Queue[Int] = mutable.Queue()): (List[Int], Boolean) = {
      localInput.foreach(input.enqueue)
      val output = ListBuffer[Int]()
      val OP_CODES = List(
        OpCode(4, true, Some(args => prog(args(2)) = args(0) + args(1))),
        OpCode(4, true, Some(args => prog(args(2)) = args(0) * args(1))),
        OpCode(2, true, Some(args => prog(args(0)) = if (input.isEmpty) return (output.toList, false) else input.dequeue())),
        OpCode(2, false, Some(args => output.addOne(args(0)))),
        OpCode(3, false, jump = Some(args => if (args(0) != 0) Some(args(1)) else None)),
        OpCode(3, false, jump = Some(args => if (args(0) == 0) Some(args(1)) else None)),
        OpCode(4, true, Some(args => prog(args(2)) = if (args(0) < args(1)) 1 else 0)),
        OpCode(4, true, Some(args => prog(args(2)) = if (args(0) == args(1)) 1 else 0))
      )
      while (true) {
        val op = prog(counter) % 100
        if (op == 99) return (output.toList, true)
        val mode = prog(counter) / 100
        val opCode = OP_CODES(op - 1) // 1-indexed to 0-indexed
        val newMode = if (opCode.lastArgIsPos) mode + math.pow(10, opCode.size - 2).toInt else mode

        val args = newMode.toString.toCharArray.toList.reverse.padTo(opCode.size - 1, '0').zipWithIndex.map({
          case ('0', i) => prog(prog(counter + i + 1))
          case ('1', i) => prog(counter + i + 1)
        })
        if (opCode.action.isDefined)
          opCode.action.get(args)
        (opCode.jump match {
          case Some(jump) => jump(args)
          case None => None
        }) match {
          case Some(value) => counter = value
          case None => counter += opCode.size
        }
      }
      (output.toList, true)
    }

  }

  case class OpCode(
                     size: Int,
                     lastArgIsPos: Boolean,
                     action: Option[List[Int] => Unit] = None,
                     jump: Option[List[Int] => Option[Int]] = None
                   )

}

