package y2019.day24

import scala.io.Source

object Day24b {
  val example1: String =
    """....#
      |#..#.
      |#..##
      |..#..
      |#....""".stripMargin

  val example2: String =
    """.....
      |.....
      |.....
      |#....
      |.#...""".stripMargin

  val empty: Array[Array[Char]] =
    """.....
      |.....
      |..?..
      |.....
      |.....""".stripMargin.split("\n").map(_.toCharArray)

  def print2D[T](arr: Array[Array[T]]): Unit = arr foreach { line => println(line mkString "") }

  implicit def bool2int(b: Boolean): Int = if (b) 1 else 0

  def surrounding(map: Map[Int, Array[Array[Char]]], x: Int, y: Int, z: Int): Int =
    bool2int(x > 0 && map(z)(y)(x - 1) == '#') +
      (x < 4 && map(z)(y)(x + 1) == '#') +
      (y > 0 && map(z)(y - 1)(x) == '#') +
      (y < 4 && map(z)(y + 1)(x) == '#') +
      (x == 0 && map(z - 1)(2)(1) == '#') +
      (y == 0 && map(z - 1)(1)(2) == '#') +
      (x == 4 && map(z - 1)(2)(3) == '#') +
      (y == 4 && map(z - 1)(3)(2) == '#') +
      (if (x == 1 && y == 2) (0 until 5).map(y => bool2int(map(z + 1)(y)(0) == '#')).sum else 0) +
      (if (x == 3 && y == 2) (0 until 5).map(y => bool2int(map(z + 1)(y)(4) == '#')).sum else 0) +
      (if (x == 2 && y == 1) (0 until 5).map(x => bool2int(map(z + 1)(0)(x) == '#')).sum else 0) +
      (if (x == 2 && y == 3) (0 until 5).map(x => bool2int(map(z + 1)(4)(x) == '#')).sum else 0)

  def main(args: Array[String]) {
    //    val in = Source.fromString(example1)
    val in = Source.fromFile("src/" + Day24.getClass.getSimpleName.toLowerCase().dropRight(1) + "/data.in")
    var map = Map(0 -> in.getLines().toArray.map(_.toCharArray)).withDefault(_ => empty)
    map(0)(2)(2) = '?'
    (1 to 200).foreach(Z => {
      val newMap = (-(Z + 1) / 2 to (Z + 1) / 2).map(z =>
        z -> (0 until 5).map(y =>
          (0 until 5).map(x =>
            map(z)(y)(x) match {
              case '#' => if (surrounding(map, x, y, z) == 1) '#' else '.'
              case '.' => if (List(1, 2) contains surrounding(map, x, y, z)) '#' else '.'
              case '?' => '?'
            }).toArray
        ).toArray
      ).toMap.withDefault(_ => empty)
      map = newMap
      map.values.foreach(plane => print2D(plane))
      println()
      println()
    })
    println(map.values.map(plane => plane.map(line => line.map(c => bool2int(c == '#')).sum).sum).sum)
  }
}
