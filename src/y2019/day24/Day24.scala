package y2019.day24

import scala.collection.mutable
import scala.io.Source

object Day24 {
  val example1: String =
    """....#
      |#..#.
      |#..##
      |..#..
      |#....""".stripMargin

  val example2: String =
    """.....
      |.....
      |.....
      |#....
      |.#...""".stripMargin

  def print2D[T](arr: Array[Array[T]]): Unit = arr foreach { line => println(line mkString "") }

  implicit def bool2int(b: Boolean): Int = if (b) 1 else 0

  def biodiversity(map: Array[Array[Char]]): Int =
    (0 until 25).map(i => (map(i / 5)(i % 5) == '#') << i).reduce(_ | _)

  def surrounding(map: Array[Array[Char]], x: Int, y: Int): Int =
    bool2int(x > 0 && map(y)(x - 1) == '#') +
      (x < 4 && map(y)(x + 1) == '#') +
      (y > 0 && map(y - 1)(x) == '#') +
      (y < 4 && map(y + 1)(x) == '#')

  def main(args: Array[String]) {
    //    val in = Source.fromString(example1)
    val in = Source.fromFile("src/" + Day24.getClass.getSimpleName.toLowerCase().dropRight(1) + "/data.in")
    var map = in.getLines().toArray map (_.toCharArray)
    val seen = mutable.Set[Int]()
    while (true) {
      val newMap = (0 until 5).map(y => (0 until 5).map(x => map(y)(x) match {
        case '#' => if (surrounding(map, x, y) == 1) '#' else '.'
        case '.' => if (List(1, 2) contains surrounding(map, x, y)) '#' else '.'
      }).toArray).toArray
      map = newMap
      val bio = biodiversity(map)
      print2D(map)
      println(bio)
      println()
      if (seen.contains(bio)) {
        System.exit(0) // Because breaking a loop in Scala is soooo out of fashion
      }
      seen.add(bio)
    }
  }
}
