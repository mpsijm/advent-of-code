package y2019.day23

import y2019.day09.Day09

import scala.collection.mutable
import scala.io.Source

object Day23 {
  def main(args: Array[String]) {
    val in = Source.fromFile("src/" + Day23.getClass.getSimpleName.toLowerCase().dropRight(1) + "/data.in")
    val prog = in.getLines().next().split(",").map(_.toLong).padTo(100000, 0L)
    val computers = (0 until 50).map(addr => new Day09.IntcodeComputer(prog.clone(), mutable.Queue[Long](addr))).toList
    val queues = computers.indices.map(_ => mutable.Queue[Long]()).toList
    var NAT: List[Long] = Nil
    var prevNAT: List[Long] = Nil
    var halt = false
    while (!halt) {
      if (queues.forall(_.isEmpty) && NAT.nonEmpty) {
        queues.head.enqueueAll(NAT)
        if (prevNAT == NAT) {
          println(NAT.last) // Part 2
          halt = true
        }
        prevNAT = NAT
      }
      (computers zip queues).foreach { case (computer, queue) =>
        val (out, h) = computer.simulate(if (queue.isEmpty) mutable.Queue(-1L) else queue)
        out.indices.by(3).foreach(i => {
          if (out(i) == 255) {
            if (prevNAT == Nil) {
              println(out(i + 2)) // Part 1
              prevNAT = out(i + 2) :: Nil
            }
            NAT = out.slice(i + 1, i + 3)
          } else
            queues(out(i).toInt).enqueue(out(i + 1), out(i + 2))
        })
        if (h) halt = true
      }
    }
  }

}
