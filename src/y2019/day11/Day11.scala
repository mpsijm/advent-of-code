package y2019.day11

import y2019.day09.Day09

import scala.collection.mutable
import scala.io.Source

object Day11 {
  def main(args: Array[String]) {
    val in = Source.fromFile("src/" + Day11.getClass.getSimpleName.toLowerCase().dropRight(1) + "/data.in")
    val prog = in.getLines().next().split(",").map(_.toLong).padTo(100000, 0L)
    val computer = new Day09.IntcodeComputer(prog.clone(), mutable.Queue())
    val colors = mutable.HashMap[(Int, Int), Long]()
    colors.put((0, 0), 1L) // Part 2
    var halt = false
    var x = 0
    var y = 0
    var dir = 0
    while (!halt) {
      val color = colors.getOrElse((x, y), 0L)
      val (out, h) = computer.simulate(mutable.Queue(color))
      out match {
        case List(paint, turn) =>
          colors.put((x, y), paint)
          dir += turn.toInt * 2 - 1 // converts from  0 or 1  to  -1 or 1
          if (dir < 0) dir += 4
          if (dir > 3) dir -= 4
          dir match {
            case 0 => y -= 1
            case 1 => x += 1
            case 2 => y += 1
            case 3 => x -= 1
          }
        case l => throw new RuntimeException("AAHH" + l)
      }
      halt = h
    }
    println(colors.size)

    // Part 2
    val hull = Array.ofDim[Long](6, 6*8)
    colors.foreach{ case ((x, y), c) => hull(y)(x) = c }
    hull foreach { line => println(line map { c => if (c == 0) " " else "#" } mkString "") }
  }

}
