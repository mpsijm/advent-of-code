package y2019.day05;

import java.io.*;
import java.util.ArrayList;
import java.util.*;
import java.util.function.*;

import static java.util.Arrays.*;

public class Day05Java {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new BufferedReader(new FileReader("src/" + Day05Java.class.getSimpleName().toLowerCase() + "/data.in")));
//        Scanner sc = new Scanner("1,9,10,3,2,3,11,0,99,30,40,50");
        new Day05Java().solve(sc);
        sc.close();
    }

    private void solve(Scanner sc) {
        int[] prog = Arrays.stream(sc.next().split(",")).mapToInt(Integer::parseInt).toArray();
        List<Integer> res = simulate(prog.clone(), new LinkedList<>(asList(/* 1 */ 5)));
        System.out.println(res);
    }

    private List<Integer> simulate(int[] prog, Queue<Integer> input) {
        List<Integer> output = new ArrayList<>();
        final OpCode[] OP_CODES = new OpCode[]{
                new OpCode(4, true, args -> prog[args[2]] = args[0] + args[1]),
                new OpCode(4, true, args -> prog[args[2]] = args[0] * args[1]),
                new OpCode(2, true, args -> prog[args[0]] = input.poll()),
                new OpCode(2, false, args -> output.add(args[0])),
                new OpCode(3, false, null, args -> args[0] != 0 ? args[1] : -1),
                new OpCode(3, false, null, args -> args[0] == 0 ? args[1] : -1),
                new OpCode(4, true, args -> prog[args[2]] = args[0] < args[1] ? 1 : 0),
                new OpCode(4, true, args -> prog[args[2]] = args[0] == args[1] ? 1 : 0),
        };
        int counter = 0;
        while (true) {
            int op = prog[counter] % 100;
            if (op == 99) return output;
            int mode = prog[counter] / 100;

            OpCode opCode = OP_CODES[op - 1]; // 1-indexed to 0-indexed
            if (opCode.lastArgIsPos)
                mode += Math.pow(10, opCode.size - 2);

            int[] args = getArgs(prog, counter, opCode.size - 1, mode);
            if (opCode.action != null)
                opCode.action.accept(args);
            if (opCode.jump != null) {
                int newPos = opCode.jump.apply(args);
                if (newPos != -1) {
                    counter = newPos;
                    continue;
                }
            }
            counter += opCode.size;
        }
    }

    private int[] getArgs(int[] prog, int counter, int size, int mode) {
        int[] res = new int[size];
        for (int i = 0; i < size; i++) {
            switch (mode % 10) {
                case 0:
                    res[i] = prog[prog[counter + i + 1]];
                    break;
                case 1:
                    res[i] = prog[counter + i + 1];
                    break;
                default:
                    System.err.println("AARRRGGGSSSS!");
                    throw new RuntimeException(counter + " " + size + " " + mode + " " + Arrays.toString(prog));
            }
            mode /= 10;
        }
        return res;
    }

    static class OpCode {
        final int size;
        final boolean lastArgIsPos;
        final Consumer<int[]> action;
        final Function<int[], Integer> jump;

        OpCode(int size, boolean lastArgIsPos, Consumer<int[]> action) {
            this.size = size;
            this.lastArgIsPos = lastArgIsPos;
            this.action = action;
            this.jump = null;
        }

        OpCode(int size, boolean lastArgIsPos, Consumer<int[]> action, Function<int[], Integer> jump) {
            this.size = size;
            this.lastArgIsPos = lastArgIsPos;
            this.action = action;
            this.jump = jump;
        }
    }
}
