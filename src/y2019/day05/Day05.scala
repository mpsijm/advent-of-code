package y2019.day05

import java.io._
import java.util.Scanner

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

object Day05 {
  def main(args: Array[String]) {
    val sc = new Scanner(new BufferedReader(new FileReader("src/" + Day05.getClass.getSimpleName.toLowerCase().dropRight(1) + "/data.in")))
    // val sc = new Scanner("1,9,10,3,2,3,11,0,99,30,40,50");
    new Day05().solve(sc)
    sc.close()
  }
}

class Day05 {
  def solve(sc: Scanner) {
    val prog = sc.next().split(",").map(_.toInt)
    val res = simulate(prog.clone(), mutable.Queue(/* 1 */ 5))
    println(res)
  }

  def simulate(prog: Array[Int], input: mutable.Queue[Int]): List[Int] = {
    val output = ListBuffer[Int]()
    val OP_CODES = List(
      OpCode(4, true, Some(args => prog(args(2)) = args(0) + args(1))),
      OpCode(4, true, Some(args => prog(args(2)) = args(0) * args(1))),
      OpCode(2, true, Some(args => prog(args(0)) = input.dequeue())),
      OpCode(2, false, Some(args => output.addOne(args(0)))),
      OpCode(3, false, jump = Some(args => if (args(0) != 0) Some(args(1)) else None)),
      OpCode(3, false, jump = Some(args => if (args(0) == 0) Some(args(1)) else None)),
      OpCode(4, true, Some(args => prog(args(2)) = if (args(0) < args(1)) 1 else 0)),
      OpCode(4, true, Some(args => prog(args(2)) = if (args(0) == args(1)) 1 else 0))
    )
    var counter = 0
    while (true) {
      val op = prog(counter) % 100
      if (op == 99) return output.toList
      val mode = prog(counter) / 100
      val opCode = OP_CODES(op - 1) // 1-indexed to 0-indexed
      val newMode = if (opCode.lastArgIsPos) mode + math.pow(10, opCode.size - 2).toInt else mode

      val args = newMode.toString.toCharArray.toList.reverse.padTo(opCode.size - 1, '0').zipWithIndex.map({
        case ('0', i) => prog(prog(counter + i + 1))
        case ('1', i) => prog(counter + i + 1)
      })
      if (opCode.action.isDefined)
        opCode.action.get(args)
      (opCode.jump match {
        case Some(jump) => jump(args)
        case None => None
      }) match {
        case Some(value) => counter = value
        case None => counter += opCode.size
      }
    }
    output.toList
  }

}

case class OpCode(
                   size: Int,
                   lastArgIsPos: Boolean,
                   action: Option[List[Int] => Unit] = None,
                   jump: Option[List[Int] => Option[Int]] = None
                 )
