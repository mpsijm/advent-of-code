package y2019.day25

import y2019.day09.Day09

import scala.collection.mutable
import scala.io.Source

object Day25 {
  def print2D[T](arr: Array[Array[T]]): Unit = arr foreach { line => println(line mkString "") }

  def main(args: Array[String]) {
    val in = Source.fromFile("src/" + Day25.getClass.getSimpleName.toLowerCase().dropRight(1) + "/data.in")
    val prog = in.getLines().next().split(",").map(_.toLong).padTo(100000, 0L)
    val computer = new Day09.IntcodeComputer(prog.clone(), mutable.Queue[Long]())
    val strInput = new mutable.StringBuilder()
    strInput.addAll(
      """east
        |east
        |south
        |take monolith
        |north
        |east
        |take shell
        |west
        |west
        |north
        |west
        |take bowl of rice
        |east
        |north
        |take planetoid
        |west
        |take ornament
        |south
        |south
        |take fuel cell
        |north
        |north
        |east
        |east
        |take cake
        |south
        |south
        |west
        |north
        |take astrolabe
        |west
        |""".stripMargin)
    val inventory =
      """monolith
        |bowl of rice
        |ornament
        |shell
        |astrolabe
        |planetoid
        |fuel cell
        |cake""".stripMargin.split("\n")
    (0 to 255).foreach(x => {
      inventory.indices.foreach(i => if ((1 << i & x) > 0) strInput.addAll("drop " + inventory(i) + "\n"))
      strInput.addAll("inv\nnorth\n")
      inventory.indices.foreach(i => if ((1 << i & x) > 0) strInput.addAll("take " + inventory(i) + "\n"))
    })
    println(strInput.toString())

    val input = mutable.Queue[Long]()
    input.enqueueAll(strInput.toString() map (_.toLong))
    var halt = false
    while (!halt) {
      val (out, h) = computer.simulate(input)
      println(out map (_.toChar) mkString "")
      input.enqueueAll(io.StdIn.readLine() map (_.toLong))
      input.enqueue(10)
      halt = h
    }
  }

}
