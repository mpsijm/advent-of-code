package y2019.day21

import y2019.day09.Day09

import scala.collection.mutable
import scala.io.Source

object Day21 {
  def print2D[T](arr: Array[Array[T]]): Unit = arr foreach { line => println(line mkString "") }

  def main(args: Array[String]) {
    val in = Source.fromFile("src/" + Day21.getClass.getSimpleName.toLowerCase().dropRight(1) + "/data.in")
    val prog = in.getLines().next().split(",").map(_.toLong).padTo(100000, 0L)
    val part1 = // (!A | !B | !C) & D
      """NOT A J
        |NOT B T
        |OR T J
        |NOT C T
        |OR T J
        |AND D J
        |WALK
        |""".stripMargin
    val part2 =
    // (!A | !B | !C) & [(D & H) | (D & E & F) | (D & E & I)]   // simplified
    // (!A | !B | !C) & D & {[(F | I) & E] | H}                 // converted to SpringScript
      """NOT A J
        |NOT B T
        |OR T J
        |NOT C T
        |OR T J
        |AND D J
        |NOT F T
        |NOT T T
        |OR I T
        |AND E T
        |OR H T
        |AND T J
        |RUN
        |""".stripMargin
    List(part1, part2).foreach(part => {
      val computer = new Day09.IntcodeComputer(prog.clone(), mutable.Queue[Long]())
      val input = mutable.Queue[Long]()
      part.toCharArray.foreach(c => input.enqueue(c))
      val (out, halt) = computer.simulate(input)
      assert(halt, "Computer should halt... right?")
      println(out map (_.toChar) dropRight 1 mkString "")
      println(out.last)
    })
  }

}
