package y2019.day02;

import java.io.*;
import java.util.*;

public class Day02 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new BufferedReader(new FileReader("src/" + Day02.class.getSimpleName().toLowerCase() + "/data.in")));
//        Scanner sc = new Scanner("1,9,10,3,2,3,11,0,99,30,40,50");
        new Day02().solve(sc);
        sc.close();
    }

    private void solve(Scanner sc) {
        String[] read = sc.next().split(",");
        int[] prog = Arrays.stream(read).mapToInt(Integer::parseInt).toArray();
        for (int i = 0; i < 10000; i++) {
            int res = simulate(prog.clone(), i / 100, i % 100);
            if (res == 19690720)
                System.out.println(i);
        }
    }

    private int simulate(int[] prog, int param1, int param2) {
        prog[1] = param1;
        prog[2] = param2;
        int counter = 0;
        while (true) {
            switch (prog[counter]) {
                case 1:
                    prog[prog[counter + 3]] = prog[prog[counter + 1]] + prog[prog[counter + 2]];
                    break;
                case 2:
                    prog[prog[counter + 3]] = prog[prog[counter + 1]] * prog[prog[counter + 2]];
                    break;
                default:
                    System.err.println("AAHHH BOOM");
                    throw new RuntimeException(counter + "    " + Arrays.toString(prog));
                case 99:
                    return prog[0];
            }
            counter += 4;
        }
    }
}
