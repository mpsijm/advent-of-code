package y2019.day17

import y2019.day09.Day09

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.io.Source

object Day17 {
  def print2D[T](arr: Array[Array[T]]): Unit = arr foreach { line => println(line mkString "") }

  def main(args: Array[String]) {
    val in = Source.fromFile("src/" + Day17.getClass.getSimpleName.toLowerCase().dropRight(1) + "/data.in")
    val prog = in.getLines().next().split(",").map(_.toLong).padTo(100000, 0L)
    val computer1 = new Day09.IntcodeComputer(prog.clone(), mutable.Queue())
    prog(0) = 2
    val computer2 = new Day09.IntcodeComputer(prog.clone(), mutable.Queue())

    val (out, h) = computer1.simulate()
    assert(h, "Computer 1 should have halted")
    val mapStr = out map (_.toChar) mkString ""
    val map = mapStr.split("\n")
    println(mapStr)
    println(getAlignment(map)) // Part 1

    val movements = getMovements(map)
    println(movements)
    val subseqs = getSubsequences(movements).get
    println(subseqs)
    val (main, functions) = subseqs
    val commands = (main.mkString(",") :: functions.map(_.mkString(","))) mkString "\n"
    println(commands)

    val input = mutable.Queue[Long]()
    commands.foreach(c => input.enqueue(c.toLong))
    input.enqueue('\n')
    input.enqueue('n')
    input.enqueue('\n')
    val (out2, h2) = computer2.simulate(input)
    assert(h2, "Computer 2 should have halted")
    println(out2 map (_.toChar) mkString "") // Part 2
    println(out2.last)
  }

  private def getAlignment(map: Array[String]) = {
    Range(1, map.length - 2).flatMap(y => Range(1, map(y).length - 2).map((_, y)).filter { case (x, y) =>
      !List(map(y)(x), map(y - 1)(x), map(y + 1)(x), map(y)(x - 1), map(y)(x + 1)).contains('.')
    }).map(p => p._1 * p._2).sum
  }

  private def getMovements(map: Array[String]) = {
    var (x, y) = map.indices.flatMap(y => map(y).indices.map((_, y)).filter { case (x, y) =>
      map(y)(x) != '#' && map(y)(x) != '.'
    }).head
    var dir = map(y)(x) match {
      case '^' => 0
      case '>' => 1
      case 'v' => 2
      case '<' => 3
    }
    val movements = mutable.ListBuffer[String]()
    var pathFound = false
    while (!pathFound) {
      dir match {
        case 0 | 2 =>
          val wallR = x + 1 < map(y).length && map(y)(x + 1) == '#'
          val wallL = x - 1 >= 0 && map(y)(x - 1) == '#'
          assert(!wallR || !wallL) // Cannot both be true
          if (wallR) {
            movements.addOne(if (dir == 0) "R" else "L")
            dir = 1
          }
          if (wallL) {
            movements.addOne(if (dir == 0) "L" else "R")
            dir = 3
          }
          if (!wallL && !wallR)
            pathFound = true
        case 1 | 3 =>
          val wallU = y - 1 >= 0 && map(y - 1)(x) == '#'
          val wallD = y + 1 < map.length && map(y + 1)(x) == '#'
          assert(!wallU || !wallD) // Cannot both be true
          if (wallU) {
            movements.addOne(if (dir == 3) "R" else "L")
            dir = 0
          }
          if (wallD) {
            movements.addOne(if (dir == 3) "L" else "R")
            dir = 2
          }
          if (!wallU && !wallD)
            pathFound = true
      }

      if (!pathFound) {
        var dist = 0
        while (x >= 0 && y >= 0 && y < map.length && x < map(y).length && map(y)(x) != '.') {
          dir match {
            case 0 => y -= 1
            case 1 => x += 1
            case 2 => y += 1
            case 3 => x -= 1
          }
          dist += 1
        }
        // One step back, else we fall off
        dir match {
          case 2 => y -= 1
          case 3 => x += 1
          case 0 => y += 1
          case 1 => x -= 1
        }
        dist -= 1
        movements.addOne(dist.toString)
      }
    }
    movements
  }

  private def getSubsequences(movements: ListBuffer[String]): Option[(ListBuffer[String], List[List[String]])] = {
    val letters = List("A", "B", "C")
    for {a <- 2 to movements.length by 2
         b <- 2 to movements.length by 2
         c <- 2 to movements.length by 2} {
      val movs = movements.clone()
      val subSeqs = List(a, b, c).zipWithIndex.map { case (a, i) =>
        val currentLetter = List(('A' + i).toChar.toString)
        val start = movs.indexWhere(s => !letters.contains(s))
        if (start == -1 || start + a > movs.size) currentLetter else {
          val subSeq = movs.slice(start, start + a).toList
          while (movs.containsSlice(subSeq)) {
            movs.patchInPlace(movs.indexOfSlice(subSeq), currentLetter, a)
          }
          subSeq
        }
      }
      if (movs.forall(m => letters.contains(m)) && subSeqs.forall(subSeq => letters.forall(s => !subSeq.contains(s))))
        return Some(movs, subSeqs)
    }
    None
  }

}
