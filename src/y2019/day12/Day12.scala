package y2019.day12

import scala.io.Source

object Day12 {
  val example1: String =
    """<x=-1, y=0, z=2>
      |<x=2, y=-10, z=-7>
      |<x=4, y=-8, z=8>
      |<x=3, y=5, z=-1>""".stripMargin

  val example2: String =
    """<x=-8, y=-10, z=0>
      |<x=5, y=5, z=10>
      |<x=2, y=-7, z=3>
      |<x=9, y=-8, z=-3>""".stripMargin

  def print2D[T](arr: Array[Array[T]]) = arr foreach { line => println(line mkString ",") }

  // https://rosettacode.org/wiki/Least_common_multiple#Scala
  @scala.annotation.tailrec
  def gcd(a: Int, b: Int): Int = if (b == 0) a.abs else gcd(b, a % b)

  def lcm(a: Int, b: Int): Int = (a * b).abs / gcd(a, b)

  def main(args: Array[String]) {
    //    val in = Source.fromString(example1)
    val in = Source.fromFile("src/" + Day12.getClass.getSimpleName.toLowerCase().dropRight(1) + "/data.in")
    val moonPos = in.getLines().map(line => line.split(",").map(coord => coord.replaceAll("[^-0-9]", "").toInt)).toArray
    val moonPosOrig = moonPos map {
      _.clone()
    }
    val moonVel = Array.ofDim[Int](4, 3)
    val period = Array.fill[Int](3)(-1)
    var iter = 0
    while (period.contains(-1)) {
      iter += 1
      for (k <- 0 until 3) {
        for (i <- 0 until 4) {
          for (j <- 0 until 4) {
            if (i != j) {
              moonVel(i)(k) += math.signum(moonPos(j)(k) - moonPos(i)(k))
            }
          }
        }
        for (i <- 0 until 4) {
          moonPos(i)(k) += moonVel(i)(k)
        }
        if (period(k) == -1 && moonVel.forall(v => v(k) == 0) && (moonPos zip moonPosOrig).forall { case (p, o) => p(k) == o(k) })
          period(k) = iter
      }
      if (iter == 1000) {
        // Part 1
        println((moonPos zip moonVel).map { case (p, v) => p.map(math.abs).sum * v.map(math.abs).sum }.sum)
      }
      //      println(iter)
      //      print2D(moonPos)
      //      print2D(moonVel)
      //      println(period mkString ",")
      //      println()
    }
    // Part 2
    println(period mkString ",")
    // Lowest common multiple calculated with https://www.calculatorsoup.com/calculators/math/lcm.php
  }

}
