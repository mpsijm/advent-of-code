package y2019.day14

import scala.collection.mutable
import scala.io.Source

object Day14 {
  val example1: String =
    """10 ORE => 10 A
      |1 ORE => 1 B
      |7 A, 1 B => 1 C
      |7 A, 1 C => 1 D
      |7 A, 1 D => 1 E
      |7 A, 1 E => 1 FUEL""".stripMargin

  val example2: String =
    """9 ORE => 2 A
      |8 ORE => 3 B
      |7 ORE => 5 C
      |3 A, 4 B => 1 AB
      |5 B, 7 C => 1 BC
      |4 C, 1 A => 1 CA
      |2 AB, 3 BC, 4 CA => 1 FUEL""".stripMargin

  def print2D[T](arr: Array[Array[T]]) = arr foreach { line => println(line mkString ",") }

  def parse(i: String) = {
    val two = i.split(" ")
    (two(0).toLong, two(1))
  }

  def main(args: Array[String]) {
    //        val in = Source.fromString(example2)
    val in = Source.fromFile("src/" + Day14.getClass.getSimpleName.toLowerCase().dropRight(1) + "/data.in")
    val reactions = in.getLines().map(line => line.split(" => ")).map {
      case Array(i, o) =>
        val element = parse(o)
        (element._2, (element._1, i.split(", ").map(parse)))
    }.toMap

    println(oreNeeded(reactions, 1)) // Part 1

    // Binary search over all options because calculating is cheap anyway :D
    var (low, high) = (1L, 100_000_000L)
    while (low < high) {
      val middle = low + (high - low) / 2
      val ore = oreNeeded(reactions, middle)
      if (ore > 1_000_000_000_000L)
        high = middle - 1
      else
        low = middle
    }
    println(high, oreNeeded(reactions, high)) // Part 2
  }

  private def oreNeeded(reactions: Map[String, (Long, Array[(Long, String)])], fuelWanted: Long) = {
    val toProcess = mutable.HashMap[String, Long]("FUEL" -> 0)
    reactions.foreach { case (product, (_, inputs)) =>
      inputs.foreach { case (_, input) =>
        toProcess.put(input, toProcess.getOrElseUpdate(input, 0) + 1)
      }
    }

    val amounts = mutable.HashMap[String, Long]("FUEL" -> fuelWanted)
    while (toProcess.nonEmpty) {
      val ready = toProcess.filter(_._2 == 0)
      if (ready.isEmpty) throw new RuntimeException(toProcess.toString())
      ready.foreach { case (k, _) =>
        toProcess.remove(k)
        if (k != "ORE") {
          val (gives, inputs) = reactions(k)
          val needed = amounts(k)
          val mult = math.ceil(needed.toDouble / gives.toDouble).toLong
          inputs.foreach { case (i, component) =>
            amounts.put(component, amounts.getOrElseUpdate(component, 0) + i * mult)
            toProcess.put(component, toProcess(component) - 1)
          }
        }
      }
    }

    amounts("ORE")
  }
}
