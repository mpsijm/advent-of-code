package y2019.day13

import y2019.day09.Day09

import scala.collection.mutable
import scala.io.Source

object Day13 {
  def print2D[T](arr: Array[Array[T]]) = arr foreach { line => println(line mkString ",") }

  def intToTile(o: Long) = o match {
    case 0 => ' '
    case 1 => '#'
    case 2 => 'B'
    case 3 => '-'
    case 4 => 'o'
  }

  def main(args: Array[String]) {
    val in = Source.fromFile("src/" + Day13.getClass.getSimpleName.toLowerCase().dropRight(1) + "/data.in")
    val prog = in.getLines().next().split(",").map(_.toLong).padTo(100000, 0L)
    val computer = new Day09.IntcodeComputer(prog.clone(), mutable.Queue())
    val screen = Array.ofDim[Long](23, 43)

    val input = mutable.Queue[Long]()
    var score = 0L
    var halt = false
    var padX = 0L
    var ballX = 0L
    var ballY = 0L
    var prevBallX = 0L
    while (!halt) {
      prevBallX = ballX
      val (out, h) = computer.simulate(input)
      for (i <- out.indices by 3) {
        val x = out(i).toInt
        val y = out(i + 1).toInt
        val v = out(i + 2)
        if (x == -1) score = v else screen(y)(x) = v
        v match {
          case 3 => padX = x
          case 4 => ballX = x; ballY = y
          case _ =>
        }
      }
      screen foreach { line => println(line map intToTile mkString "") }
      val dir = math.signum(ballX - padX)
      val dir2 = if (dir == 0 && ballY < 20) math.signum(ballX - prevBallX) else dir
      println(s"Pad: $padX  Ball: $ballX,$ballY  Dir: $dir2  Score: $score")
      input.enqueue(dir2)
      halt = h
    }

    println()
    println(score)
  }

}
