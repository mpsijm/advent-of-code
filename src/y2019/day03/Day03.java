package y2019.day03;

import java.io.*;
import java.util.*;

public class Day03 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new BufferedReader(new FileReader("src/" + Day03.class.getSimpleName().toLowerCase() + "/data.in")));
//        Scanner sc = new Scanner("R8,U5,L5,D3\nU7,R6,D4,L4");
//        Scanner sc = new Scanner("R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83");
//        Scanner sc = new Scanner("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\nU98,R91,D20,R16,D67,R40,U7,R15,U6,R7");
        new Day03().solve(sc);
        sc.close();
    }

    private void solve(Scanner sc) {
        String wire1 = sc.next(), wire2 = sc.next();
        Map<Pos, Pos> locs = new HashMap<>();
        Pos currentPos = new Pos(0, 0, 0);
        for (String dirDist : wire1.split(",")) {
            char dir = dirDist.charAt(0);
            int dist = Integer.parseInt(dirDist.substring(1));
            for (int i = 0; i < dist; i++) {
                currentPos = currentPos.to(dir);
                locs.put(currentPos, currentPos);
            }
        }

        Set<Integer> intersections = new HashSet<>();
        currentPos = new Pos(0, 0, 0);
        for (String dirDist : wire2.split(",")) {
            char dir = dirDist.charAt(0);
            int dist = Integer.parseInt(dirDist.substring(1));
            for (int i = 0; i < dist; i++) {
                currentPos = currentPos.to(dir);
                if (locs.containsKey(currentPos)) {
                    intersections.add(currentPos.d + locs.get(currentPos).d);
                }
            }
        }
        int min = intersections.stream().min(Comparator.comparingInt(Integer::intValue)).get();
        System.out.println(min);
    }

    class Pos {
        final int x, y, d;

        public Pos(int x, int y, int d) {
            this.x = x;
            this.y = y;
            this.d = d;
        }

        public Pos to(char dir) {
            switch (dir) {
                case 'R':
                    return new Pos(x + 1, y, d + 1);
                case 'L':
                    return new Pos(x - 1, y, d + 1);
                case 'U':
                    return new Pos(x, y + 1, d + 1);
                case 'D':
                    return new Pos(x, y - 1, d + 1);
                default:
                    throw new RuntimeException("Boom!");
            }
        }

        public int dist() {
            return Math.abs(x) + Math.abs(y);
        }

        @Override
        public int hashCode() {
            return Objects.hash(x, y);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Pos pos = (Pos) o;
            return x == pos.x && y == pos.y;
        }

        @Override
        public String toString() {
            return "(" + x + ',' + y + ')';
        }
    }
}
