package y2019.day04;

import java.util.*;

public class Day04 {
    public static void main(String[] args) {
        int low = 172851, high = 675869;
        int part1 = 0, part2 = 0;

        nextPassword: for (int i = low; i <= high; i++) {
            char[] password = Integer.toString(i).toCharArray();
            int[] frequency = new int['9' + 1];
            char prev = '0';
            for (char curr : password) {
                if (curr < prev)
                    continue nextPassword;
                frequency[curr]++;
                prev = curr;
            }

            if (Arrays.stream(frequency).anyMatch(j -> j >= 2))
                part1++;
            if (Arrays.stream(frequency).anyMatch(j -> j == 2))
                part2++;
        }

        System.out.println(part1);
        System.out.println(part2);
    }
}
