
# Day 07b
with open("data.in", "r") as f:
    line=f.readline().strip()
    pict=['2' for i in range(0,25*6)]
    for i in range(0,len(line),25*6):
        pict=[line[i+j] if pict[j]=='2' else pict[j] for j in range(0,25*6)]
    for i in range(0,6):
        print("".join(pict[i*25:(i+1)*25]))
