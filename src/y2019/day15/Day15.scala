package y2019.day15

import y2019.day09.Day09

import scala.collection.mutable
import scala.io.Source

object Day15 {
  def print2D[T](arr: Array[Array[T]]) = arr foreach { line => println(line mkString "") }

  case class Pos(x: Int, y: Int, retDir: Int, var dir: Int)

  def reverse(dir: Int): Int = dir match {
    case 1 => 2
    case 2 => 1
    case 3 => 4
    case 4 => 3
  }

  def move(x: Int, y: Int, dir: Int): (Int, Int) = dir match {
    case 1 => (x, y - 1)
    case 2 => (x, y + 1)
    case 3 => (x - 1, y)
    case 4 => (x + 1, y)
  }

  def main(args: Array[String]) {
    val in = Source.fromFile("src/" + Day15.getClass.getSimpleName.toLowerCase().dropRight(1) + "/data.in")
    val prog = in.getLines().next().split(",").map(_.toLong).padTo(100000, 0L)
    val computer = new Day09.IntcodeComputer(prog.clone(), mutable.Queue())
    val size = 21 // Experimentally determined
    val screen = Array.fill[Char](2 * size, 2 * size)(' ')

    val input = mutable.Queue[Long]()
    var (x, y) = (size, size)
    var end = (-1, -1)
    val ariadnesThread = mutable.Stack[Pos](Pos(x, y, 0, 1))
    var halt = false
    while (!halt) {
      val topPos = ariadnesThread.top
      if (topPos.dir != 5) {
        var (x2, y2) = move(x, y, topPos.dir)
        while (screen(y2)(x2) != ' ' && topPos.dir != 5) {
          topPos.dir += 1
          if (topPos.dir != 5) {
            val t = move(x, y, topPos.dir)
            x2 = t._1
            y2 = t._2
          }
        }
      }
      if (topPos.dir == 5) {
        input.enqueue(topPos.retDir)
        val (out, h) = computer.simulate(input)
        if (out.nonEmpty) {
          out.head match {
            case 1 =>
              val t = move(x, y, topPos.retDir)
              x = t._1
              y = t._2
            case _ =>
              screen(y)(x) = 'D'
              screen(size)(size) = 'O'
              print2D(screen)
              throw new RuntimeException("Oy, this wall was not here before...")
          }
          ariadnesThread.pop()
        }
        halt = h
      } else {
        val (x2, y2) = move(x, y, topPos.dir)
        input.enqueue(topPos.dir)
        val (out, h) = computer.simulate(input)
        if (out.nonEmpty) {
          out.head match {
            case 0 =>
              screen(y2)(x2) = '#'
            case 1 =>
              screen(y2)(x2) = '.'
              x = x2
              y = y2
              ariadnesThread.push(Pos(x, y, reverse(topPos.dir), 1))
            case 2 =>
              println(ariadnesThread.size) // Part 1
              screen(y2)(x2) = 'O'
              end = (x, y)
              x = x2
              y = y2
              ariadnesThread.push(Pos(x, y, reverse(topPos.dir), 1))
          }
          topPos.dir += 1
        }
        halt = h
      }
      // Animation
//      val prev = screen(y)(x)
//      screen(y)(x) = 'D'
//      print2D(screen)
//      screen(y)(x) = prev
    }
    screen(y)(x) = 'D'

    print2D(screen) // Just for fun :D

    val nextMinute = mutable.Queue[(Int, Int)](end)
    val thisMinute = mutable.Queue[(Int, Int)](end)
    screen(end._2)(end._1) = 'O'
    var minutes = 0
    while (nextMinute.nonEmpty) {
      while (nextMinute.nonEmpty)
        thisMinute.enqueue(nextMinute.dequeue())
      thisMinute.foreach { case (x, y) =>
        Range(1, 5).foreach(dir => {
          val (x2, y2) = move(x, y, dir)
          if (screen(y2)(x2) != '#' && screen(y2)(x2) != 'O') {
            screen(y2)(x2) = 'O'
            nextMinute.enqueue((x2, y2))
          }
        })
      }
      minutes += 1
      // Animation
//      print2D(screen)
    }
    println(minutes) // Part 2
    print2D(screen) // Just for fun :D
  }

}
