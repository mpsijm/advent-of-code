package y2019.day09

import java.io._
import java.util.Scanner

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

object Day09 {
  def main(args: Array[String]) {
    val sc = new Scanner(new BufferedReader(new FileReader("src/" + Day09.getClass.getSimpleName.toLowerCase().dropRight(1) + "/data.in")))
//        val sc = new Scanner("109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99")
//        val sc = new Scanner("1102,34915192,34915192,7,4,7,99,0")
    solve(sc)
    sc.close()
  }

  def solve(sc: Scanner) {
    val prog = sc.next().split(",").map(_.toLong).padTo(1000000, 0L)
    println(new IntcodeComputer(prog.clone(), mutable.Queue(2)).simulateAll())
  }

  class IntcodeComputer(prog: Array[Long], input: mutable.Queue[Long]) {
    var counter = 0
    var relativeBase = 0

    def simulateAll(): List[Long] = {
      val output = ListBuffer[Long]()
      var halt = false
      while (!halt) {
        val (out, h) = simulate()
        output.addAll(out)
        halt = h
      }
      output.toList
    }

    def simulate(localInput: mutable.Queue[Long] = mutable.Queue()): (List[Long], Boolean) = {
      while (localInput.nonEmpty)
        input.enqueue(localInput.dequeue())
      val output = ListBuffer[Long]()
      val OP_CODES = List(
        OpCode(4, true, Some(args => prog(args(2).toInt) = args(0) + args(1))),
        OpCode(4, true, Some(args => prog(args(2).toInt) = args(0) * args(1))),
        OpCode(2, true, Some(args => prog(args(0).toInt) = if (input.isEmpty) return (output.toList, false) else input.dequeue())),
        OpCode(2, false, Some(args => output.addOne(args(0)))),
        OpCode(3, false, jump = Some(args => if (args(0) != 0) Some(args(1)) else None)),
        OpCode(3, false, jump = Some(args => if (args(0) == 0) Some(args(1)) else None)),
        OpCode(4, true, Some(args => prog(args(2).toInt) = if (args(0) < args(1)) 1 else 0)),
        OpCode(4, true, Some(args => prog(args(2).toInt) = if (args(0) == args(1)) 1 else 0)),
        OpCode(2, false, Some(args => relativeBase += args(0).toInt)),
      )
      while (true) {
        val op = prog(counter) % 100
        if (op == 99) return (output.toList, true)
        val mode = prog(counter) / 100
        val opCode = OP_CODES(op.toInt - 1) // 1-indexed to 0-indexed

        val preArgs = mode.toString.toCharArray.toList.reverse.padTo(opCode.size - 1, '0').zipWithIndex.map({
          case ('0', i) => prog(counter + i + 1).toInt
          case ('1', i) => counter + i + 1
          case ('2', i) => prog(counter + i + 1).toInt + relativeBase
        })
        val args = preArgs.zipWithIndex.map(t => {
          if (t._2 == opCode.size - 2 && opCode.lastArgIsPos) t._1 else prog(t._1)
        })
        if (opCode.action.isDefined)
          opCode.action.get(args)
        (opCode.jump match {
          case Some(jump) => jump(args)
          case None => None
        }) match {
          case Some(value) => counter = value.toInt
          case None => counter += opCode.size
        }
      }
      (output.toList, true)
    }

  }

  case class OpCode(
                     size: Int,
                     lastArgIsPos: Boolean,
                     action: Option[List[Long] => Unit] = None,
                     jump: Option[List[Long] => Option[Long]] = None
                   )

}

