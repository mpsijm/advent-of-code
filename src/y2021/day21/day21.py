from collections import Counter

data = [[a, int(b), c, d, int(e)] for a, b, c, d, e in (line.strip().split() for line in open("data.in").readlines())]

space = [data[0][4], data[1][4]]
score = [0, 0]
rolls = 0
while not any(s >= 1000 for s in score):
    for player in range(2):
        add = 0
        for _ in range(3):
            add += rolls % 100 + 1
            rolls += 1
        space[player] = (space[player] + add - 1) % 10 + 1
        score[player] += space[player]
        if score[player] >= 1000:
            print(score[1 - player] * rolls)
            break

states = Counter({((data[0][4], 0), (data[1][4], 0)): 1})
done_states = Counter()
adds = Counter(d1 + d2 + d3 for d1 in range(1, 4) for d2 in range(1, 4) for d3 in range(1, 4))
while states:
    for player in range(2):
        new_states = Counter()
        for state, c in states.items():
            for add, d in adds.items():
                space, score = state[player]
                other = state[1 - player]
                space = (space + add - 1) % 10 + 1
                score += space
                new_state = ((space, score), other) if player == 0 else (other, (space, score))
                (done_states if score >= 21 else new_states)[new_state] += c * d
        states = new_states
print(max(sum((state[p][1] > state[1 - p][1]) * c for state, c in done_states.items()) for p in range(2)))
