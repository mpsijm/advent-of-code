from functools import reduce

res1, res2, left_chars, right_chars = 0, [], "([{<", ")]}>"
for line in [x.strip() for x in open("data.in").readlines()]:
    stack = []
    for c in line:
        if c in left_chars:
            stack.append(c)
        if c in right_chars:
            d = stack.pop()
            if left_chars.index(d) != right_chars.index(c):
                res1 += [3, 57, 1197, 25137][right_chars.index(c)]
                break
    else:  # Python magic: in for-else, `else` is only executed when the `for` doesn't `break`
        res2.append(reduce(lambda acc, c: acc * 5 + left_chars.index(c) + 1, reversed(stack), 0))

print(res1, sorted(res2)[len(res2) // 2], sep="\n")
