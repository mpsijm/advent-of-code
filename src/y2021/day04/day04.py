with open("data.in") as f:
    data = [[x for x in block.replace("\n", " ").split()] for block in f.read().strip().split("\n\n")]

numbers = data[0][0].split(",")
boards = data[1:]
checkeds = [[False] * 25 for _ in boards]
bingo = [False for _ in boards]
for x in numbers:
    for k, (board, checked) in enumerate(zip(boards, checkeds)):
        if bingo[k] or x not in board:
            continue
        checked[board.index(x)] = True
        if any(all(checked[i * 5:i * 5 + 5]) or all(checked[i + j * 5] for j in range(5)) for i in range(5)):
            bingo[k] = True
            if sum(bingo) in (1, len(boards)):
                print(int(x) * sum(int(y) for y, z in zip(board, checked) if not z))
