def flash(x, y):
    if data[y][x] > 9 and (x, y) not in flashed:
        flashed.add((x, y))
        for yy in range(y - 1, y + 2):
            for xx in range(x - 1, x + 2):
                if not (x == xx and y == yy) and 0 <= xx < w and 0 <= yy < h:
                    data[yy][xx] += 1
                    flash(xx, yy)


data = [[int(y) for y in str(x.strip())] for x in open("data.in").readlines()]
h, w, res, i = len(data), len(data[0]), 0, 0
while True:
    for y in range(h):
        for x in range(w):
            data[y][x] += 1
    flashed = set()
    for y in range(h):
        for x in range(w):
            flash(x, y)
    for x, y in flashed:
        data[y][x] = 0
    res += len(flashed)
    i += 1
    if i == 100:
        print(res)
    if len(flashed) == w * h:
        print(i)
        break
