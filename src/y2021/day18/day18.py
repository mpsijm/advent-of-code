def combine(next, to_add):
    next = [[n, d + 1] for n, d in [*next, *to_add]]
    doing = True
    while doing:
        curr = next
        next = []
        doing = False
        i = 0
        do_explode = any(d >= 5 for n, d in curr)
        while i < len(curr):
            n, d = curr[i]
            if d >= 5:
                doing = True
                if next:
                    next[-1][0] += n
                next.append([0, d - 1])
                if i < len(curr) - 2:
                    next.append([curr[i + 2][0] + curr[i + 1][0], curr[i + 2][1]])
                if i < len(curr) - 3:
                    next.extend(curr[i + 3:])
                break
            if n > 9 and not do_explode:
                doing = True
                next.append([n // 2, d + 1])
                next.append([n - n // 2, d + 1])
                next.extend(curr[i + 1:])
                break
            next.append([n, d])
            i += 1
    return next


def to_orig(next):
    while len(next) > 1:
        curr = next
        next = []
        i = 0
        while i < len(curr) - 1:
            if curr[i][1] == curr[i + 1][1]:
                next.append([[curr[i][0], curr[i + 1][0]], curr[i][1] - 1])
                if i < len(curr) - 2:
                    next.extend(curr[i + 2:])
                    break
            next.append(curr[i])
            i += 1
    orig = next[0][0]
    return orig


def magn(l):
    if type(l) is int:
        return l
    return 3 * magn(l[0]) + 2 * magn(l[1])


snailfish = [[[eval(line), 0]] for line in open("dath.in").readlines()]
for i in range(5):
    snailfish = [
        [c for sub, d in line for c in ([[sub[0], d + 1], [sub[1], d + 1]] if type(sub) is list else [[sub, d]])]
        for line in snailfish
    ]

curr = snailfish[0]
for to_add in snailfish[1:]:
    curr = combine(curr, to_add)
print(magn(to_orig(curr)))

max_res = 0
for i, s1 in enumerate(snailfish):
    for j, s2 in enumerate(snailfish):
        if i == j:
            continue
        max_res = max(max_res, magn(to_orig(combine(s1, s2))))
print(max_res)
