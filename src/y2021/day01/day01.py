from functools import reduce

with open("data.in") as f:
    data = [int(x) for x in f.readlines()]

inc = 0
prev = data[0]
for x in data[1:]:
    if x > prev:
        inc += 1
    prev = x
print(inc)

inc = 0
prev = sum(data[0:3])
for i in range(1, len(data) - 2):
    x = sum(data[i:i + 3])
    if x > prev:
        inc += 1
    prev = x
print(inc)

# print(reduce(lambda c, i: c + (data[i] > data[i - 1]), range(len(data))))
# print(reduce(lambda c, i: c + (sum(data[i:i + 3]) > sum(data[i - 1:i + 2])), range(len(data) - 2)))
