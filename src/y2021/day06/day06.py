from collections import Counter

with open("data.in") as f:
    data = Counter(int(x) for x in f.readline().split(","))

for i in range(1, 257):
    old0 = data[0]
    data = Counter({k - 1: v for k, v in data.items() if k > 0})
    data[8] = old0
    data[6] += old0
    if i in (80, 256):
        print(sum(data.values()))
