with open("data.in") as f:
    data = [[a, int(b)] for a, b in (line.strip().split() for line in f.readlines())]

x = 0
y = 0
for a, b in data:
    if a == "forward":
        x += b
    if a == "down":
        y += b
    if a == "up":
        y -= b
print(x * y)

aim = 0
x = 0
y = 0
for a, b in data:
    if a == "forward":
        x += b
        y += aim * b
    if a == "down":
        aim += b
    if a == "up":
        aim -= b
print(x * y)

