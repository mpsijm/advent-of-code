scanners = [[line[2], [tuple(map(int, coords.split(","))) for coords in line[4:]]]
            for line in [block.replace("\n", " ").split() for block in open("data.in").read().strip().split("\n\n")]]


def sign(n):
    return 0 if n == 0 else -1 if n < 0 else 1


def rotations(coords):
    return [
        [(sign(xx) * c[abs(xx) - 1], sign(yy) * c[abs(yy) - 1], sign(zz) * c[abs(zz) - 1]) for c in coords]
        for xx, yy, zz in (
            (1, 2, 3), (-2, 1, 3), (-1, -2, 3), (2, -1, 3),
            (1, -2, -3), (-2, -1, -3), (-1, 2, -3), (2, 1, -3),
            (2, 3, 1), (3, -2, 1), (-2, -3, 1), (-3, 2, 1),
            (2, -3, -1), (-3, -2, -1), (-2, 3, -1), (3, 2, -1),
            (3, 1, 2), (1, -3, 2), (-3, -1, 2), (-1, 3, 2),
            (3, -1, -2), (-1, -3, -2), (-3, 1, -2), (1, 3, -2),
        )
    ]


def rotate(scanner_a, scanner_b):
    distances_a = {abs(x - xx) + abs(y - yy) + abs(z - zz) for x, y, z in scanner_a for xx, yy, zz in scanner_a}
    distances_b = {abs(x - xx) + abs(y - yy) + abs(z - zz) for x, y, z in scanner_b for xx, yy, zz in scanner_b}
    if len(distances_a & distances_b) < 72:  # 72 = 12 * 12 // 2
        return None
    for dx, dy, dz in scanner_a:
        srota = {(x - dx, y - dy, z - dz) for x, y, z in scanner_a}
        for rotb in rotations(scanner_b):
            for dxx, dyy, dzz in rotb:
                srotb = {(x - dxx, y - dyy, z - dzz) for x, y, z in rotb}
                overlap = len(srota & srotb)
                if overlap >= 12:
                    return rotb, (dxx - dx, dyy - dy, dzz - dz)
    return None


todo = [0]
offsets = {0: (0, 0, 0)}
res = set(scanners[0][1])
while todo:
    curr = todo.pop()
    dx, dy, dz = offsets[curr]
    _, sa = scanners[curr]
    for j, (_, sb) in enumerate(scanners):
        if j in offsets: continue
        new_rot = rotate(sa, sb)
        if new_rot is not None:
            scanners[j][1], (dxx, dyy, dzz) = new_rot
            res |= {(x - dx - dxx, y - dy - dyy, z - dz - dzz) for x, y, z in new_rot[0]}
            todo.append(j)
            offsets[j] = (dx + dxx, dy + dyy, dz + dzz)

print(len(res))
print(max(abs(x - xx) + abs(y - yy) + abs(z - zz) for x, y, z in offsets.values() for xx, yy, zz in offsets.values()))
