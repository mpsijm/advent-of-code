with open("data.in") as f:
    data = [[int(x) for x in line.strip()] for line in f.readlines()]

w, h = len(data[0]), len(data)

print(sum(data[y][x] + 1 for y in range(h) for x in range(w)
          if all(not (0 <= xx < w and 0 <= yy < h) or data[yy][xx] > data[y][x]
                 for xx, yy in ((x - 1, y), (x + 1, y), (x, y - 1), (x, y + 1)))))


def flood_fill(x0, y0):
    q, basin = [(x0, y0)], {(x0, y0)}
    while q:
        x, y = q.pop()
        for xx, yy in ((x - 1, y), (x + 1, y), (x, y - 1), (x, y + 1)):
            if 0 <= xx < w and 0 <= yy < h and (xx, yy) not in basin and data[yy][xx] < 9:
                basin.add((xx, yy))
                q.append((xx, yy))
    return basin


done, basins = set(), []
for y in range(h):
    for x in range(w):
        if data[y][x] < 9 and (x, y) not in done:
            basin = flood_fill(x, y)
            done |= basin
            basins.append(len(basin))

basins = sorted(basins)
print(basins[-3] * basins[-2] * basins[-1])
