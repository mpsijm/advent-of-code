import gc

data = [line.strip() for line in open("data.in").readlines()]


def handle_inp(instr):
    target = ord(instr.split()[1]) - ord("w")
    return lambda mem: ((i, (*mem[:target], i, *mem[target + 1:])) for i in range(1, 10))


def handle_other(instr):
    code, a, b = instr.split()
    target = ord(a) - ord("w")
    if b in "wxyz":
        source = ord(b) - ord("w")
        val = lambda mem: mem[source]
    else:
        source = int(b)
        val = lambda mem: source

    if code == "add":
        calc = lambda mem, v: v + val(mem)
    if code == "mul":
        calc = lambda mem, v: v * val(mem)
    if code == "div":
        calc = lambda mem, v: v // val(mem)
    if code == "mod":
        calc = lambda mem, v: v % val(mem)
    if code == "eql":
        calc = lambda mem, v: 1 if v == val(mem) else 0

    return lambda mem: (*mem[:target], calc(mem, mem[target]), *mem[target + 1:])


for part in (1, 2):
    states = {tuple(0 for _ in range(4)): []}
    for pc, instr in enumerate(data):
        gc.collect()
        print(pc, len(states))
        new_states = dict()
        process = handle_inp(instr) if instr.startswith("inp") else handle_other(instr)
        for mem_vals, inps in states.items():
            if instr.startswith("inp"):
                for i, new_mem in process(mem_vals):
                    new_inps = [*inps, i]
                    if new_mem not in new_states or \
                            (new_inps > new_states[new_mem] if part == 1 else new_inps < new_states[new_mem]):
                        new_states[new_mem] = new_inps
                continue
            new_mem = process(mem_vals)
            if new_mem not in new_states or (inps > new_states[new_mem] if part == 1 else inps < new_states[new_mem]):
                new_states[new_mem] = inps
        states = new_states
        # print(states)
    print((max if part == 1 else min)(inps for mem_vals, inps in states.items() if mem_vals[3] == 0))
