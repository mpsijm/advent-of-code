print(sum(v > 1 for k, v in __import__("collections").Counter(
    pos for (x1, y1), (x2, y2) in [
        (map(int, c.split(",")) for c in p)
        for p in (line.split("->") for line in open("data.in").readlines())
    ] for pos in zip(
        [x1] * (abs(y2 - y1) + 1) if x1 == x2 else range(x1, x2 + 1) if x1 < x2 else range(x1, x2 - 1, -1),
        [y1] * (abs(x2 - x1) + 1) if y1 == y2 else range(y1, y2 + 1) if y1 < y2 else range(y1, y2 - 1, -1))
    # if x1 == x2 or y1 == y2  # Comment out this condition to print the answer for part one
).items()))
