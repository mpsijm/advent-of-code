data = list(map(int, open("data.in").readline().strip().split(",")))


def run(calc):
    low, high = min(data), max(data)
    while True:
        mid = (low + high) // 2
        vals = [sum(calc(x, p) for x in data) for p in (mid, mid + 1)]
        if low == mid:
            print(min(vals))
            break
        if vals[0] < vals[1]:
            high = mid
        else:
            low = mid + 1


[run(calc) for calc in (lambda a, b: abs(a - b), lambda a, b: abs(a - b) * (abs(a - b) + 1) // 2)]

# print(min(sum(abs(x - i) for x in data) for i in range(min(data), max(data))))
# print(min(sum(abs(x - i) * (abs(x - i) + 1) // 2 for x in data) for i in range(min(data), max(data))))
