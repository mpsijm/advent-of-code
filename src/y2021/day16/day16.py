data = [x.strip() for x in open("dati.in").readlines()][0]

data = "".join(x for c in data for x in {
    "0": "0000", "1": "0001", "2": "0010", "3": "0011",
    "4": "0100", "5": "0101", "6": "0110", "7": "0111",
    "8": "1000", "9": "1001", "A": "1010", "B": "1011",
    "C": "1100", "D": "1101", "E": "1110", "F": "1111",
}[c])
n = len(data)
versions = []


def parse(datb, i, k=None):
    p = 0
    values = []
    while (k is None and i < len(datb) - 3) or (k is not None and p < k):
        # print(p, k)
        # print(" " * i + datb[i:])
        p += 1
        V = int(datb[i:i + 3], 2)
        versions.append(V)
        i += 3
        T = int(datb[i:i + 3], 2)
        i += 3
        # print(V, T)
        if T == 4:
            s = []
            while datb[i] == '1':
                s.append(datb[i + 1:i + 5])
                i += 5
            s.append(datb[i + 1:i + 5])
            i += 5
            values.append(int("".join(s), 2))
        else:
            # print("i", i)
            i += 1
            if datb[i - 1] == '1':
                q = int(datb[i:i + 11], 2)
                # print("Q", q)
                i, vs = parse(datb, i + 11, q)
            else:
                q = int(datb[i:i + 15], 2)
                # print("q", q)
                _, vs = parse(datb[i + 15:i + 15 + q], 0)
                i += 15 + q
            if T == 0:

                values.append(sum(vs))
            if T == 1:
                acc = 1
                for v in vs: acc *= v
                values.append(acc)
            if T == 2:
                values.append(min(vs))
            if T == 3:
                values.append(max(vs))
            if T == 5:
                values.append(1 if vs[0] > vs[1] else 0)
            if T == 6:
                values.append(1 if vs[0] < vs[1] else 0)
            if T == 7:
                values.append(1 if vs[0] == vs[1] else 0)
    return i, values


_, res = parse(data, 0, 1)

print(sum(versions))
print(res[0])
