from collections import Counter

print(sum(len(c) in (2, 3, 4, 7) for line in open("data.in").readlines() for c in line.split("|")[1].split()))

res = 0
for left, right in ((l.split() for l in line.split("|")) for line in open("data.in").readlines()):
    left = sorted(left, key=lambda c: len(c))
    numbers = {1: left[0], 7: left[1], 4: left[2], 8: left[9]}

    n096 = [c for c in left if len(c) == 6]
    common = [k for k, v in Counter(c for num in [numbers[1], numbers[4], *n096] for c in num).items() if v == 2][0]
    numbers[9] = [num for num in n096 if common not in num][0]
    n06 = [num for num in n096 if num != numbers[9]]
    common = [k for k, v in Counter(c for num in [numbers[1], *n06] for c in num).items() if v == 1][0]
    numbers[6] = [num for num in n06 if common in num][0]
    numbers[0] = [num for num in n06 if num != numbers[6]][0]

    n235 = [c for c in left if len(c) == 5]
    common = [k for k, v in Counter(c for num in [numbers[9], *n235] for c in num).items() if v == 1][0]
    numbers[2] = [num for num in n235 if common in num][0]
    n35 = [num for num in n235 if num != numbers[2]]
    common = [k for k, v in Counter(c for num in [numbers[7], *n35] for c in num).items() if v == 1][0]
    numbers[5] = [num for num in n35 if common in num][0]
    numbers[3] = [num for num in n35 if num != numbers[5]][0]

    numbers = {"".join(sorted(str(v))): str(k) for k, v in numbers.items()}
    res += int("".join(numbers["".join(sorted(num))] for num in right))
print(res)

