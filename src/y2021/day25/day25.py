data = [x.strip() for x in open("data.in").readlines()]

w, h = len(data[0]), len(data)
step = 0
done = False
while not done:
    done = True
    for herd in ">v":
        new_data = [list(line) for line in data]
        for y in range(h):
            for x in range(w):
                if data[y][x] != herd:
                    continue
                new_y, new_x = (y + (herd == "v")) % h, (x + (herd == ">")) % w
                if data[new_y][new_x] == ".":
                    new_data[new_y][new_x] = herd
                    new_data[y][x] = "."
                    done = False
        data = new_data
    step += 1
print(step)
