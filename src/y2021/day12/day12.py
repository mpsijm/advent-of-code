import string
from collections import defaultdict, Counter

data, orig_graph = [x.strip() for x in open("data.in").readlines()], defaultdict(list)
for a, b in (line.split("-") for line in data):
    orig_graph[a].append(b)
    orig_graph[b].append(a)
graph = {k: Counter(v) for k, v in orig_graph.items()}
for upper in [k for k in graph.keys() if all(c in string.ascii_uppercase for c in k)]:
    for a in graph[upper]:
        for b in graph[upper]:
            graph[a][b] += 1
        del graph[a][upper]
    del graph[upper]
for a in graph:
    del graph[a]["start"]

paths, res = [("start", {"start"}, 1)], 0
while paths:
    curr, seen, multiplier = paths.pop()
    for neigh, m in graph[curr].items():
        if neigh in seen:
            continue
        if neigh == "end":
            res += multiplier * m
        else:
            paths.append((neigh, {*seen, neigh}, multiplier * m))
print(res)

paths, res = [("start", {"start"}, 1, False)], 0
while paths:
    curr, seen, multiplier, double = paths.pop()
    for neigh, m in graph[curr].items():
        if double and neigh in seen:
            continue
        if neigh == "end":
            res += multiplier * m
        else:
            paths.append((neigh, {*seen, neigh}, multiplier * m, double or neigh in seen))
print(res)
