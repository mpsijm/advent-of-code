def dijkstra(grid):
    queue, seen = [[(0, 0)], *([] for _ in range(9 * (w + h)))], [[False for _ in line] for line in grid]
    for cost in range(len(queue)):
        for x, y in queue[cost]:
            if x == w - 1 and y == h - 1:
                print(cost)
                break
            for xx, yy in [(x - 1, y), (x + 1, y), (x, y - 1), (x, y + 1)]:
                if not (0 <= xx < w and 0 <= yy < h) or seen[yy][xx]:
                    continue
                seen[yy][xx] = True
                queue[cost + grid[yy][xx]].append((xx, yy))


data = [[int(x) for x in line.strip()] for line in open("data.in").readlines()]
w, h = len(data[0]), len(data)
dijkstra(data)

data = [[(data[y % h][x % w] + x // w + y // h - 1) % 9 + 1 for x in range(w * 5)] for y in range(h * 5)]
w, h = len(data[0]), len(data)
dijkstra(data)
