with open("data.in") as f:
    data = [x.strip() for x in f.readlines()]

l = max(len(x) for x in data)

gamma, eps = "", ""
for i in range(l):
    one, zero = [sum(x[i] == b for x in data) for b in ("0", "1")]
    gamma += "1" if one > zero else "0"
    eps += "0" if one > zero else "1"
print(int(gamma, 2) * int(eps, 2))

data_copy = list(data)
o2, co2 = 0, 0
for i in range(l):
    one, zero = [sum(x[i] == b for x in data) for b in ("0", "1")]
    data = [x for x in data if x[i] == ("1" if one >= zero else "0")]
    if len(data) == 1:
        o2 = int(data[0], 2)
        break
data = data_copy
for i in range(l):
    one, zero = [sum(x[i] == b for x in data) for b in ("0", "1")]
    data = [x for x in data if x[i] == ("1" if one < zero else "0")]
    if len(data) == 1:
        co2 = int(data[0], 2)
        break
print(o2 * co2)
