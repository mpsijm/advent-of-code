from collections import Counter

template, rules = open("data.in").read().strip().split("\n\n")
rules = {a + b: [a + c, c + b] for (a, b), c in (line.split(" -> ") for line in rules.split("\n"))}
inv_rules = {a: [b for b, c in rules.items() if a in c] for a in rules.keys()}
letters = set("".join(rules.keys()))
pairs = Counter(c + d for c, d in zip(template, template[1:]))
for i in range(40):
    pairs = Counter({k: sum(pairs[a] for a in inv_rules[k]) for k in rules.keys()})
    if i + 1 in (10, 40):
        counter = Counter({c: sum(v * k.count(c) for k, v in pairs.items()) for c in letters})
        counter[template[0]] += 1
        counter[template[-1]] += 1
        # In `counter`, everything is counted double, because we count both members of each pair, but pairs overlap
        print((max(counter.values()) - min(counter.values())) // 2)
