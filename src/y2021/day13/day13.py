coords, folds = open("data.in").read().strip().split("\n\n")
grid = {tuple(map(int, line.split(","))) for line in coords.split("\n")}
for i, (a, b) in enumerate(line.split("=") for line in folds.split("\n")):
    b = int(b)
    if "y" in a:
        grid = {(x, y if y < b else 2 * b - y) for x, y in grid}
    if "x" in a:
        grid = {(x if x < b else 2 * b - x, y) for x, y in grid}
    if i == 0:
        print(len(grid))
print(*("".join("#" if (x, y) in grid else "." for x in range(max(x for x, _ in grid) + 1))
        for y in range(max(y for _, y in grid) + 1)), sep="\n")
