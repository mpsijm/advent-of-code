data = [[x for x in block.replace("\n", " ").translate({ord("#"): ord("1"), ord("."): ord("0")}).split()]
        for block in open("data.in").read().strip().split("\n\n")]

enhance = data[0][0]
grid = data[1]
inverted = enhance[0] == '1' and enhance[511] == '0'
for i in range(50):
    outside_bit = "1" if inverted and i % 2 == 1 else "0"
    w, h = len(grid[0]) + 2, len(grid) + 2
    outside_line = outside_bit * (w + 2)
    grid = [
        outside_line,
        outside_line,
        *(outside_bit * 2 + line + outside_bit * 2 for line in grid),
        outside_line,
        outside_line,
    ]
    grid = [
        "".join([
            enhance[int(grid[y][x:x + 3] + grid[y + 1][x:x + 3] + grid[y + 2][x:x + 3], 2)]
            for x in range(w)
        ])
        for y in range(h)
    ]
    if i + 1 in (2, 50):
        print(sum(line.count("1") for line in grid))
