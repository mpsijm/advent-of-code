data = [x.replace("\n", "") for x in open("data.in").readlines()]

hallway_spots = (1, 2, 4, 6, 8, 10, 11)
costs = {"A": 1, "B": 10, "C": 100, "D": 1000}
pod_pos = {"A": 0, "B": 1, "C": 2, "D": 3}
empty_hallway = "." * 11


def add(queue, done, new_cost, new_stacks, new_hallway):
    key = (new_stacks, new_hallway)
    str_key = ",".join(new_stacks) + new_hallway
    if str_key in done:
        old_cost = done[str_key]
        if new_cost < old_cost:
            queue[old_cost].remove(key)
        else:
            return
    queue[new_cost].append(key)
    done[str_key] = new_cost


def generator_from_bucket_queue(queue):
    for i in range(len(queue)):
        for x in queue[i]:
            yield i, x


for part in (1, 2):
    if part == 2:
        data = [*data[:3], "  #D#C#B#A#  ", "  #D#B#A#C#  ", *data[3:]]
    target = tuple(c * (part * 2) for c in "ABCD")
    stacks = tuple("".join(data[y][x] for y in range(3 if part == 1 else 5, 1, -1)) for x in range(3, 11, 2))
    hallway = " " + "." * 11
    queue = [[(stacks, hallway)], *([] for _ in range(100_000))]
    done = dict()
    for cost, (stacks, hallway) in generator_from_bucket_queue(queue):
        if stacks == target:
            print(cost)
            break
        for i, stack in enumerate(stacks):
            if stack:
                stack_pos = (i + 1) * 2 + 1
                pod = stack[-1]
                for spot in hallway_spots:
                    if (hallway[spot:stack_pos + 1] if spot < stack_pos else hallway[stack_pos:spot + 1]) \
                            not in empty_hallway:
                        continue
                    add(queue, done,
                        cost + (part * 2 + 1 - len(stack) + abs(stack_pos - spot)) * costs[pod],
                        (*stacks[:i], stack[:-1], *stacks[i + 1:]),
                        hallway[:spot] + pod + hallway[spot + 1:],
                    )
        for spot in hallway_spots:
            pod = hallway[spot]
            if pod == ".": continue
            i = pod_pos[pod]
            stack = stacks[i]
            if stack not in pod * 4: continue
            stack_pos = (i + 1) * 2 + 1
            if (hallway[spot + 1:stack_pos + 1] if spot < stack_pos else hallway[stack_pos:spot]) not in empty_hallway:
                continue
            add(queue, done,
                cost + (part * 2 - len(stack) + abs(stack_pos - spot)) * costs[pod],
                (*stacks[:i], stack + pod, *stacks[i + 1:]),
                hallway[:spot] + "." + hallway[spot + 1:],
            )
