data = [(onoff, tuple(tuple(map(int, coord[2:].split(".."))) for coord in coords.split(",")))
        for onoff, coords in (line.strip().split() for line in open("data.in").readlines())]

# cubes = set()
# for onoff, ((x1, x2), (y1, y2), (z1, z2)) in data:
#     if not (-50 <= x1 and x2 <= 50 and -50 <= y1 and y2 <= 50 and -50 <= z1 and z2 <= 50):
#         continue
#
#     subcube = {(x, y, z) for x in range(x1, x2 + 1) for y in range(y1, y2 + 1) for z in range(z1, z2 + 1)}
#     if onoff == "on":
#         cubes |= subcube
#     else:
#         cubes -= subcube
# print(len(cubes))


def split_ranges(xranges, x1, x2, onoff, new_sub):
    x_before, x_switch, x_after = [], [], []

    if x1 < xranges[0][0] and onoff == "on":
        x_switch.append((x1, min(x2, xranges[0][0] - 1), new_sub(True)))
        if x2 < xranges[0][0] - 1:
            x_after.append((x2 + 1, xranges[0][0] - 1, new_sub(False)))

    for xx1, xx2, yranges in xranges:
        if xx1 < x1 and x2 < xx2:
            x_before.append((xx1, x1 - 1, yranges))
            x_switch.append((x1, x2, yranges))
            x_after.append((x2 + 1, xx2, yranges))
        elif x1 > xx2:
            x_before.append((xx1, xx2, yranges))
        elif x2 < xx1:
            x_after.append((xx1, xx2, yranges))
        elif xx1 < x1 <= xx2:
            x_before.append((xx1, x1 - 1, yranges))
            x_switch.append((x1, xx2, yranges))
        elif xx1 <= x2 < xx2:
            x_switch.append((xx1, x2, yranges))
            x_after.append((x2 + 1, xx2, yranges))
        elif x1 <= xx1 and xx2 <= x2:
            x_switch.append((xx1, xx2, yranges))

    if x2 > xranges[-1][1] and onoff == "on":
        if x1 > xranges[-1][1] + 1:
            x_before.append((xranges[-1][1] + 1, x1 - 1, new_sub(False)))
        x_switch.append((max(x1, xranges[-1][1] + 1), x2, new_sub(True)))

    return x_before, x_switch, x_after


for part in (1, 2):
    xranges = [next((x1, x2, [(y1, y2, [(z1, z2, True)])]) for o, ((x1, x2), (y1, y2), (z1, z2)) in data if o == "on")]

    for onoff, ((x1, x2), (y1, y2), (z1, z2)) in data[1:]:
        if part == 1 and not (-50 <= x1 and x2 <= 50 and -50 <= y1 and y2 <= 50 and -50 <= z1 and z2 <= 50):
            continue

        new_xranges, x_switch, x_after = split_ranges(xranges, x1, x2, onoff, lambda b: [(y1, y2, [(z1, z2, b)])])
        for xx1, xx2, yranges in x_switch:
            new_yranges, y_switch, y_after = split_ranges(yranges, y1, y2, onoff, lambda b: [(z1, z2, b)])
            for yy1, yy2, zranges in y_switch:
                z_before, z_switch, z_after = split_ranges(zranges, z1, z2, onoff, lambda b: b)
                new_yranges.append(
                    (yy1, yy2, [*z_before, *((z1, z2, onoff == "on") for z1, z2, _ in z_switch), *z_after]))
            new_yranges.extend(y_after)
            new_xranges.append((xx1, xx2, new_yranges))
        new_xranges.extend(x_after)
        xranges = new_xranges

    print(sum((x2 - x1 + 1) * (y2 - y1 + 1) * (z2 - z1 + 1)
              for x1, x2, yranges in xranges for y1, y2, zranges in yranges for z1, z2, oo in zranges if oo))
