data = open("data.in").read().strip().split(":")[1].strip().split(", ")
(x1, x2), (y1, y2) = (tuple(map(int, bounds.split("=")[1].split(".."))) for bounds in data)

yss = []
for dy0 in range(y1, x2 + 1):
    ys, dy, y = [], dy0, 0
    while y >= y1:
        ys.append(y)
        y += dy
        dy -= 1
    if y1 <= ys[-1] <= y2:
        yss.append((dy0, ys))
print(max(y for dy0, ys in yss for y in ys))

res = 0
l = max(len(ys) for _, ys in yss)
for dx0 in range(0, x2 + 1):
    xs, dx, x, hits = [], dx0, 0, False
    for _ in range(l):
        if x1 <= x <= x2:
            hits = True
        if x > x2:
            break
        xs.append(x)
        x += dx
        dx -= (0 if dx == 0 else 1 if dx > 0 else -1)
    if not hits:
        continue
    for dy0, ys in yss:
        # start scanning at the lower/right boundary of the target area
        i = min(len(xs), len(ys)) - 1
        while True:
            if x1 <= xs[i] <= x2 and y1 <= ys[i] <= y2:
                # If we find a hit in the target, count and stop (in case there are multiple hits in this target)
                res += 1
                break
            if xs[i] < x1 or ys[i] < y1:
                # If we venture too far left or up, we can stop looking
                break
            i -= 1
print(res)
