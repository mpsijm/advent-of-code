from bisect import bisect_right
from itertools import accumulate

data = [[a, int(b), c] for a, b, c in (line.strip().split() for line in open("data.in").readlines())]
n = len(data)


class Trench:
    def __init__(self, x, y, xx, yy):
        if x > xx:
            x, xx = xx, x
        if y > yy:
            y, yy = yy, y
        self.x = x
        self.y = y
        self.xx = xx
        self.yy = yy
        self.length = xx - x + yy - y

    def __contains__(self, item):
        xxx, yyy = item
        if self.xx == self.x:
            return xxx == self.x and yyy in range(self.y, self.yy + 1)
        if self.yy == self.y:
            return yyy == self.y and xxx in range(self.x, self.xx + 1)
        return False


trenches, gridx, gridy = [], [], []
x, y = 0, 0
for dir, _, h in data:
    d = int(h[-2])
    l = int(h[2:-2], 16)
    dx, dy = [(1, 0), (0, 1), (-1, 0), (0, -1)][d]
    xx, yy = x + dx * l, y + dy * l
    trenches.append(Trench(x, y, xx, yy))
    gridx.append(x)
    gridy.append(y)
    x, y = xx, yy
gridx, gridy = sorted(set(gridx)), sorted(set(gridy))

# The lists grid2{x,y} compose a grid that also has cells of width 1, to accommodate for the width of a trench.
W, H = 2 * len(gridx) - 1, 2 * len(gridy) - 1
grid2x, grid2y = [1] * W, [1] * H
for i, (x, xx) in enumerate(zip(gridx, gridx[1:])):
    grid2x[2 * i + 1] = xx - x - 1
for i, (y, yy) in enumerate(zip(gridy, gridy[1:])):
    grid2y[2 * i + 1] = yy - y - 1
x2index = {x: i for i, x in enumerate(accumulate((gridx[0], *grid2x)))}
y2index = {y: i for i, y in enumerate(accumulate((gridy[0], *grid2y)))}

# Create the boundaries for the flood fill from the trenches
trench_grid = [[False] * W for y in range(H)]
for trench in trenches:
    x = x2index[trench.x]
    y = y2index[trench.y]
    if trench.x == trench.xx:
        for yy in range(y, y2index[trench.yy] + 1):
            trench_grid[yy][x] = True
    if trench.y == trench.yy:
        for xx in range(x, x2index[trench.xx] + 1):
            trench_grid[y][xx] = True


def flood_fill(x0, y0):
    q, basin = [(x0, y0)], [[False] * W for _ in range(H)]
    basin[y0][x0] = True
    count = grid2x[x0] * grid2y[y0]
    while q:
        x, y = q.pop()
        for xx, yy in ((x - 1, y), (x + 1, y), (x, y - 1), (x, y + 1)):
            if 0 <= xx < W and 0 <= yy < H and not basin[yy][xx] and not trench_grid[yy][xx]:
                q.append((xx, yy))
                basin[yy][xx] = True
                count += grid2x[xx] * grid2y[yy]
    return count


x0, y0 = bisect_right(gridx, 0), bisect_right(gridy, 0)
print(sum(trench.length for trench in trenches) + flood_fill(x0 * 2 + 1, y0 * 2 + 1))
