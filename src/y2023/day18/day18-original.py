from bisect import bisect_right

data = [[a, int(b), c] for a, b, c in (line.strip().split() for line in open("data.in").readlines())]
n = len(data)

# Sneaky trick: my ./run script automatically replaces "datb.in" with "data.in", so use that to switch
is_example = "b" in """open("datb.in")"""


class Line:
    def __init__(self, x, y, xx, yy):
        if x > xx:
            x, xx = xx, x
        if y > yy:
            y, yy = yy, y
        self.x = x
        self.y = y
        self.xx = xx
        self.yy = yy

    def __contains__(self, item):
        xxx, yyy = item
        if self.xx == self.x:
            return xxx == self.x and yyy in range(self.y, self.yy + 1)
        if self.yy == self.y:
            return yyy == self.y and xxx in range(self.x, self.xx + 1)
        return False

    def __repr__(self):
        return f"Line({self.x}, {self.y}, {self.xx}, {self.yy})"


grid = set()
x, y = 0, 0
coun = 0
for dir, _, h in data:
    d = int(h[-2])
    l = int(h[2:-2], 16)
    dx, dy = [(1, 0), (0, 1), (-1, 0), (0, -1)][d]
    xx, yy = x + dx * l, y + dy * l
    grid.add((x, y, xx, yy))
    x += dx * l
    y += dy * l
    coun += l

gridf = []
x, y = 0, 0
for dir, _, h in data:
    d = int(h[-2])
    l = int(h[2:-2], 16)
    dx, dy = [(1, 0), (0, 1), (-1, 0), (0, -1)][d]
    xx, yy = x + dx * l, y + dy * l
    gridf.append(Line(x, y, xx, yy))
    x, y = xx, yy

gridx = sorted(set(x for x, y, xx, yy in grid if x == xx))
gridy = sorted(set(y for x, y, xx, yy in grid if y == yy))

W = 2 * len(gridx) - 1
H = 2 * len(gridy) - 1
grid2 = [[1] * W for _ in range(H)]
for i, (x, xx) in enumerate(zip(gridx, gridx[1:])):
    for j, (y, yy) in enumerate(zip(gridy, gridy[1:])):
        grid2[2 * j + 1][2 * i + 1] = (xx - x - 1) * (yy - y - 1)
for j, (y, yy) in enumerate(zip(gridy, gridy[1:])):
    for i in range(0, W, 2):
        grid2[2 * j + 1][i] = yy - y - 1
for i, (x, xx) in enumerate(zip(gridx, gridx[1:])):
    for j in range(0, H, 2):
        grid2[j][2 * i + 1] = xx - x - 1


def flood_fill(x0, y0):
    q, basin = [(x0, y0)], {(x0, y0)}
    count = grid2[y0][x0]
    while q:
        x, y = q.pop()
        for xx, yy in ((x - 1, y), (x + 1, y), (x, y - 1), (x, y + 1)):
            if 0 <= xx < W and 0 <= yy < H and (xx, yy) not in basin and not in_lines(xx, yy):
                basin.add((xx, yy))
                q.append((xx, yy))
                count += grid2[yy][xx]
    if is_example:
        for y in range(H):
            print("".join("#" if (x, y) in basin else "." for x in range(W)))
        print()
    return count


def in_lines(xx, yy):
    xxx = sum(grid2[0][i] for i in range(xx)) + gridx[0]
    yyy = sum(grid2[i][0] for i in range(yy)) + gridy[0]
    return any((xxx, yyy) in line for line in gridf)


if is_example:
    print(*("\t".join(str(c) for c in line) for line in grid2), sep="\n")

    print(*gridf, sep="\n")
    # mw = min(min(line[0::2]) for line in gridf)
    # mh = min(min(line[1::2]) for line in gridf)
    # w = max(max(line[0::2]) for line in gridf)
    # h = max(max(line[1::2]) for line in gridf)
    # print(mw, mh, w, h)
    for y in range(H):
        print("".join("#" if in_lines(x, y) else "." for x in range(W)))
    print()

    print(gridx)
    print(gridy)
    print()

x0 = bisect_right(gridx, 0) + 1
y0 = bisect_right(gridy, 0) + 1
# print(x0, y0)
count = flood_fill(x0 * 2 - 1, y0 * 2 - 1)
sum1 = sum(c for yy, line in enumerate(grid2) for xx, c in enumerate(line) if in_lines(xx, yy))
# print(sum1, coun)
ans = sum1 + count
# print("diff:", (952408144115 if is_example else 63806916814808) - ans)
print(ans)
