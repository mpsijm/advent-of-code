data = [[a, int(b), c] for a, b, c in (line.strip().split() for line in open("data.in").readlines())]
n = len(data)

grid = {(0, 0)}
x, y = 0, 0
for dir, l, _ in data:
    d = "RDLU".index(dir)
    dx, dy = [(1, 0), (0, 1), (-1, 0), (0, -1)][d]
    for i in range(1, l + 1):
        grid.add((x + dx * i, y + dy * i))
    x += dx * l
    y += dy * l

mw = min(x for x, _ in grid)
mh = min(y for _, y in grid)
w = max(x for x, _ in grid)
h = max(y for _, y in grid)
# print(mw, mh, w, h)

def flood_fill(x0, y0):
    q, basin = [(x0, y0)], {(x0, y0)}
    while q:
        x, y = q.pop()
        for xx, yy in ((x - 1, y), (x + 1, y), (x, y - 1), (x, y + 1)):
            if mw <= xx <= w and mh <= yy <= h and (xx, yy) not in basin and (xx, yy) not in grid:
                basin.add((xx, yy))
                q.append((xx, yy))
    return basin

# for y in range(mh, h):
#     print("".join("#" if (x, y) in grid else "." for x in range(mw, w)))

basin = flood_fill(1, 1)
print(len(grid) + len(basin))
