import string

data = [x.strip() for x in open("data.in").readlines()]
d = ["asefliaushefkuahsekluh", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]
dd = [str(i) for i in range(10)]

print(sum(int(ds[0] + ds[-1]) for ds in ([c for c in b if c in "0123456789"] for b in data)))

da = []
db = []
for line in data:
    s = min([(99999, 10), *((line.find(di), i) for i, di in enumerate(d) if line.find(di) != -1)])
    ss = min([(99999, 10), *((line.find(di), i) for i, di in enumerate(dd) if line.find(di) != -1)])
    da.append(s[1] if s[0] < ss[0] else ss[1])
    s = max([(-1, -1), *((line.rfind(di), i) for i, di in enumerate(d) if line.rfind(di) != -1)])
    ss = max([(-1, -1), *((line.rfind(di), i) for i, di in enumerate(dd) if line.rfind(di) != -1)])
    db.append(s[1] if s[0] > ss[0] else ss[1])
print(sum(int(str(a) + str(b)) for a, b in zip(da, db)))
