import heapq
from math import pi, cos, sin

# Sneaky trick: my ./run script automatically replaces "datb.in" with "data.in", so use that to switch
is_example = "b" in """open("datb.in")"""

data = [x.strip() for x in open("data.in").readlines()]
h = len(data)
w = len(data[0])

queue = [(0, 0, 0, 0), (0, 0, 0, 1)]
cost = [[[-1] * 4 for _ in range(w)] for _ in range(h)]
cost[0][0] = [0] * 4


def getdxy(d, dl):
    return int(cos(d * pi / 2) * dl), int(sin(d * pi / 2) * dl)


while queue:
    curr, x, y, d = heapq.heappop(queue)
    if cost[y][x][d] != -1:
        assert curr >= cost[y][x][d]
    if is_example:
        print(curr, x, y, d)
    if (x, y) == (w - 1, h - 1):
        if is_example:
            for line in cost:
                print("\t".join(str(min((c for c in cell if c != -1), default=-1)) for cell in line))
        print(curr)
        break
    for dl in range(1, 4):
        dx, dy = getdxy(d, dl)
        xx, yy = x + dx, y + dy
        if 0 <= xx < w and 0 <= yy < h:
            newc = curr + (
                sum(int(c) for c in (data[y][x + 1: xx + 1] if x < xx else data[y][xx: x]))
                if y == yy else
                sum(int(data[yyy][x]) for yyy in (range(y + 1, yy + 1) if y < yy else range(yy, y)))
            )
            if is_example:
                print(x, y, dx, dy, xx, yy, curr, newc)
            for dd in {0: (3, 1), 1: (0, 2), 2: (1, 3), 3: (2, 0)}[d]:
                if cost[yy][xx][dd] == -1 or newc < cost[yy][xx][dd]:
                    cost[yy][xx][dd] = newc
                    heapq.heappush(queue, (newc, xx, yy, dd))
