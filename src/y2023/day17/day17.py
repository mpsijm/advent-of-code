import heapq

data = [[int(c) for c in x.strip()] for x in open("data.in").readlines()]
h, w = len(data), len(data[0])

queue = [(0, 0, 0, 0), (0, 0, 0, 1)]
cost = [[[1e9] * 4 for _ in range(w)] for _ in range(h)]
cost[0][0] = [0] * 4

while queue:
    curr, x, y, d = heapq.heappop(queue)  # `d` is the direction in which we should walk next
    if (x, y) == (w - 1, h - 1):
        print(curr)
        break
    for dl in range(4, 11):
        dx, dy = abs(2 - d) - 1, 1 - abs(1 - d)
        xx, yy = x + dx * dl, y + dy * dl
        if 0 <= xx < w and 0 <= yy < h:
            new_cost = curr + sum(
                data[y + dy * i][x + dx * i]
                for i in range(1, dl + 1)
            )
            # For the next step, we should turn left or right, so add both to the queue
            for dd in (d - 1) % 4, (d + 1) % 4:
                if new_cost < cost[yy][xx][dd]:
                    cost[yy][xx][dd] = new_cost
                    heapq.heappush(queue, (new_cost, xx, yy, dd))
