from collections import defaultdict, deque

data = [x.strip() for x in open("data.in").readlines()]
h, w = len(data), len(data[0])

directions = defaultdict(tuple, {
    "|": ((0, -1), (0, 1)),
    "-": ((-1, 0), (1, 0)),
    "L": ((0, -1), (1, 0)),
    "J": ((0, -1), (-1, 0)),
    "7": ((0, 1), (-1, 0)),
    "F": ((0, 1), (1, 0)),
})
# Allowed pipe shapes when moving in the given direction (e.g. when moving south (0, 1), the top pipe needs to exist).
# Basically the inverse of the `directions` dictionary.
allowed = {(0, 1): "|LJ", (0, -1): "|7F", (-1, 0): "-LF", (1, 0): "-J7"}

# Build the graph: for every position, add an edge between neighbours in `directions`.
graph = defaultdict(set)
animal = (-1, -1)
for y in range(h):
    for x in range(w):
        curr = data[y][x]
        if curr == "S":
            animal = (x, y)
        for dx, dy in directions[curr]:
            xx, yy = x + dx, y + dy
            if 0 <= xx < w and 0 <= yy < h and data[yy][xx] in allowed[(dx, dy)] + "S":  # moving to "S" is also allowed
                graph[(x, y)].add((xx, yy))
                graph[(xx, yy)].add((x, y))

# Preparing for part 2: create a grid that is twice the size of the original.
pipes = [[False] * (2 * w - 1) for _ in range(2 * h - 1)]
H = len(pipes)
W = len(pipes[0])
pipes[animal[1] * 2][animal[0] * 2] = True

# Find distances: using breadth-first search (BFS).
dists = [[-1] * w for _ in range(h)]
dists[animal[1]][animal[0]] = 0
q = deque([animal])
while q:
    x, y = q.popleft()
    for xx, yy in graph[(x, y)]:
        # Preparing for part 1: connect two grid cells for every pipe.
        # One cell is on the average between (x, y) and (xx, yy), the other is at (xx, yy), both multiplied by 2.
        pipes[y + yy][x + xx] = True
        pipes[2 * yy][2 * xx] = True

        if dists[yy][xx] > -1:
            continue
        dists[yy][xx] = dists[y][x] + 1
        q.append((xx, yy))

# Answer for part 1: the maximum of all distances.
print(max(max(line) for line in dists))

# Find tiles enclosed by the loop: flood-fill using BFS, starting from the tiles outer edges that do not have a pipe.
q = deque()
for x, y in [(x, y) for y in range(H) for x in (0, W - 1)] + [(x, y) for x in range(1, W - 1) for y in (0, H - 1)]:
    if not pipes[y][x]:
        pipes[y][x] = True
        q.append((x, y))
while q:
    x, y = q.popleft()
    for dx, dy in [(0, -1), (0, 1), (-1, 0), (1, 0)]:
        xx, yy = x + dx, y + dy
        if 0 <= xx < W and 0 <= yy < H and not pipes[yy][xx]:
            pipes[yy][xx] = True
            q.append((xx, yy))

# Answer for part 2: the number of non-filled cells in the original grid (i.e. skip every other cell in the new grid).
print(sum(not pipes[y][x] for y in range(0, H, 2) for x in range(0, W, 2)))
