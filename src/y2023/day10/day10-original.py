from collections import defaultdict

data = [x.strip() for x in open("data.in").readlines()]
h, w = len(data), len(data[0])

allowed = {(0, 1): "|LJ", (0, -1): "|7F", (-1, 0): "-LF", (1, 0): "-J7"}
graph = defaultdict(set)
animal = (-1, -1)
for y in range(h):
    for x in range(w):
        curr = data[y][x]
        if curr == "S":
            animal = (x, y)
        for (xx, yy) in {
            "|": ((0, -1), (0, 1)),
            "-": ((-1, 0), (1, 0)),
            "L": ((0, -1), (1, 0)),
            "J": ((0, -1), (-1, 0)),
            "7": ((0, 1), (-1, 0)),
            "F": ((0, 1), (1, 0)),
            ".": [],
            "S": [(0, -1), (0, 1), (-1, 0), (1, 0)],
        }[curr]:
            xxx, yyy = x + xx, y + yy
            if 0 <= xxx < w and 0 <= yyy < h and (data[yyy][xxx] in allowed[(xx, yy)] or data[yyy][xxx] == "S"):
                graph[(x, y)].add((xxx, yyy))
                graph[(xxx, yyy)].add((x, y))
# print(*graph.items(), sep="\n")

dists = [[-1] * w for _ in range(h)]
dists[animal[1]][animal[0]] = 0
pipes = [[False] * (2 * w - 1) for _ in range(2 * h - 1)]
H = len(pipes)
W = len(pipes[0])
pipes[animal[1] * 2][animal[0] * 2] = True
q = [animal]
# print(q)
while q:
    nextq = []
    for curr in q:
        x, y = curr
        for xx, yy in graph[curr]:
            if xx == x:
                for yyy in (yy * 2, (y + yy)):
                    pipes[yyy][x * 2] = True
            if yy == y:
                for xxx in (xx * 2, (x + xx)):
                    pipes[y * 2][xxx] = True
            if dists[yy][xx] > -1:
                continue
            dists[yy][xx] = dists[y][x] + 1
            nextq.append((xx, yy))
    q = nextq
# print(*dists, sep="\n")
# print(*("".join("X" if p else "." for p in line) for line in pipes), sep="\n")
print(max(max(d for d in line) for line in dists))

q = [(x, y) for y in range(H) for x in range(W) if (x in (0, W - 1) or y in (0, H - 1)) and not pipes[y][x]]
for x, y in q:
    pipes[y][x] = True
while q:
    nextq = []
    for curr in q:
        x, y = curr
        for xxx, yyy in [(0, -1), (0, 1), (-1, 0), (1, 0)]:
            xx = x + xxx
            yy = y + yyy
            if 0 <= xx < W and 0 <= yy < H:
                if not pipes[yy][xx]:
                    pipes[yy][xx] = True
                    nextq.append((xx, yy))
    q = nextq
# print(*("".join("X" if p else "." for p in line) for line in pipes), sep="\n")
print(sum(not pipes[y][x] for y in range(0, H, 2) for x in range(0, W, 2)))
