data = [line.strip().split(",") for line in open("data.in").readlines()][0]
n = len(data)

ans = 0


def hash(part):
    a = 0
    for c in part:
        a += ord(c)
        a *= 17
        a %= 256
    return a


boxes = [[] for _ in range(256)]
for part in data:
    a = hash(part)
    ans += a
print(ans)

for part in data:
    label = part.split("=")[0]
    if "=" in part:
        foc = int(part[-1])
    label = label.replace("-", "")
    h = hash(label)
    if "-" in part and any(label == lens[0] for lens in boxes[h]):
        for i, lens in enumerate(boxes[h]):
            if lens[0] == label:
                break
        boxes[h] = boxes[h][:i] + boxes[h][i + 1:]
    elif "=" in part:
        if not any(lens[0] == label for lens in boxes[h]):
            boxes[h].append((label, foc))
        else:
            for i, lens in enumerate(boxes[h]):
                if lens[0] == label:
                    break
            boxes[h][i] = (label, foc)
    # print(boxes)
ans = 0
for i, box in enumerate(boxes, start=1):
    for j, (label, foc) in enumerate(box, start=1):
        ans += i * j * foc
print(ans)
