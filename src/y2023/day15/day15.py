data = [line.strip().split(",") for line in open("data.in").readlines()][0]
n = len(data)


def hash(part):
    res = 0
    for c in part:
        res = ((res + ord(c)) * 17 % 256)
    return res


print(sum(map(hash, data)))


def lens_index(box, label):
    for i, (l, f) in enumerate(box):
        if l == label:
            return i


boxes = [[] for _ in range(256)]
for part in data:
    label = "".join(c for c in part if "a" <= c <= "z")
    box = boxes[hash(label)]
    i = lens_index(box, label)  # Index of the previous lens with this label, or `None` if it was not present
    if "-" in part:
        if i is not None:
            box.pop(i)
    elif "=" in part:
        foc = int(part[-1])
        if i is None:
            box.append((label, foc))
        else:
            box[i] = (label, foc)

print(sum(
    i * j * foc
    for i, box in enumerate(boxes, start=1)
    for j, (label, foc) in enumerate(box, start=1)
))
