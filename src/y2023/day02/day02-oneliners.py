data = [x.strip().split(":") for x in open("data.in").readlines()]

print(sum(
    int(game[0].split()[1])
    for game in data
    if all(int(x) <= M
           for C, M in (("red", 12), ("green", 13), ("blue", 14))
           for x, c in (cube.split() for sub in (game[1].split("; ")) for cube in sub.split(", "))
           if c == C)))

print(sum(
    __import__("math").prod(
        max(int(x) for x, c in (cube.split() for sub in (game[1].split("; ")) for cube in sub.split(", ")) if c == C)
        for C in ("red", "green", "blue"))
    for game in data))
