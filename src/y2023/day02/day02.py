data = [x.strip() for x in open("data.in").readlines()]

R, G, B = 12, 13, 14
games = [row.split(":") for row in data]
count = 0
powers = 0
for game in games:
    ID = int(game[0].split()[1])
    subs = game[1].split("; ")
    r, g, b = 0, 0, 0
    possible = True
    for sub in subs:
        for cube in sub.split(", "):
            c, col = cube.split()
            c = int(c)
            if col == "red":
                if c > R:
                    possible = False
                r = max(c, r)
            if col == "green":
                if c > G:
                    possible = False
                g = max(g, c)
            if col == "blue":
                if c > B:
                    possible = False
                b = max(c, b)
    if possible:
        count += ID
    powers += r * g * b
print(count)
print(powers)
