data = [[x for x in block.replace("\n", " ").split()] for block in open("data.in").read().strip().split("\n\n")]
n = len(data)


# Source: https://stackoverflow.com/a/48116944 (Also used in BAPC 2023 Preliminaries, problem Anti-Tetris)
def rot_90(blk):
    return [list(reversed(x)) for x in zip(*blk)]


def find_mirror(grid, prev_mirror=-1):
    for grid, factor in [[grid, 100], [rot_90(grid), 1]]:
        for y in range(1, len(grid)):
            if all(a == b for a, b in zip(reversed(grid[:y]), grid[y:])):
                score = y * factor
                if score != prev_mirror:
                    return score
    return None


def find_mirror2(grid, prev_mirror):
    for y in range(len(grid)):
        for x in range(len(grid[0])):
            mirror = find_mirror([
                [c if x != xx or y != yy else ("." if c == "#" else "#") for xx, c in enumerate(line)]
                for yy, line in enumerate(grid)
            ], prev_mirror)
            if mirror:
                return mirror


ans = 0
ans2 = 0
for grid in data:
    prev_mirror = find_mirror(grid)
    ans += prev_mirror
    ans2 += find_mirror2(grid, prev_mirror)
print(ans)
print(ans2)
