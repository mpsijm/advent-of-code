data = [[x for x in block.replace("\n", " ").split()] for block in open("data.in").read().strip().split("\n\n")]
n = len(data)


# Source: https://stackoverflow.com/a/48116944
def rot_90(blk):
    return [list(reversed(x)) for x in zip(*blk)]



def find_mirror(grid, prev_mirror):
    h, w = len(grid), len(grid[0])
    for y in range(1, h):
        # print(*reversed(grid[:y]), " ", *grid[y:], sep="\n")
        # print("---")
        if all(a == b for a, b in zip(reversed(grid[:y]), grid[y:])) and y * 100 != prev_mirror:
            return y * 100
    grid = rot_90(grid)
    h, w = w, h
    for y in range(1, h):
        if all(a == b for a, b in zip(reversed(grid[:y]), grid[y:])) and y != prev_mirror:
            return y
    return None


def fix_smudge(grid, x, y):
    return [[c if x != xx or y != yy else ("." if c == "#" else "#") for xx, c in enumerate(line)]
            for yy, line in enumerate(grid)]


ans = 0
ans2 = 0
for grid in data:
    prev_mirror = find_mirror(grid, -1)
    ans += prev_mirror

    grid = [list(line) for line in grid]
    h, w = len(grid), len(grid[0])
    mirror = 0
    mirror2 = 0
    for y in range(h):
        for x in range(w):
            grid2 = fix_smudge(grid, x, y)
            mirror = find_mirror(grid2, prev_mirror)
            if mirror and mirror != prev_mirror:
                if mirror2 and mirror == mirror2:
                    continue
                if mirror2 and mirror != mirror2:
                    print("AAA")
                if not mirror2:
                    ans2 += mirror
                mirror2 = mirror
                grid2[y][x] = "@" if grid2[y][x] == "#" else "-"
                # print(prev_mirror, mirror, x, y)
                # print(*("".join(line) for line in grid), sep="\n")
                # print("0123456789 12345")
                # print(*("".join(line) + " " + str(i) for i, line in enumerate(grid2)), sep="\n")
                # print()
                # break
    # if not mirror2:
    #     print(prev_mirror, mirror, x, y)
    #     print("AAA")
    #     print(*("".join(line) for line in grid), sep="\n")
    #     print()
print()
print(ans)
print(ans2)
