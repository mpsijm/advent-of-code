from math import isqrt

data = [line.strip() for line in open("data.in").readlines()]
n = len(data)

# Sneaky trick: my ./run script automatically replaces "datb.in" with "data.in", so use that to switch
is_example = "b" in """open("data.in")"""


# Source: https://stackoverflow.com/a/20677983
def line_intersection(line1, line2):
    xdiff = (line1[0][0] - line1[1][0], line2[0][0] - line2[1][0])
    ydiff = (line1[0][1] - line1[1][1], line2[0][1] - line2[1][1])

    def det(a, b):
        return a[0] * b[1] - a[1] * b[0]

    div = det(xdiff, ydiff)
    if div == 0:
        return None

    d = (det(*line1), det(*line2))
    x = det(d, xdiff) / div
    y = det(d, ydiff) / div
    return x, y


start, end = (7, 27) if is_example else (200000000000000, 400000000000000)
lines = []
for line in data:
    p, v = line.split("@")
    lines.append((
        tuple(map(int, p.split(", "))),
        tuple(map(int, v.split(", "))),
    ))
ans = 0
for i, ((px, py, pz), (vx, vy, vz)) in enumerate(lines):
    assert vx != 0 and vy != 0
    for (px2, py2, pz2), (vx2, vy2, vz2) in lines[i + 1:]:
        intersection = line_intersection(
            ((px, py), (px + vx, py + vy)),
            ((px2, py2), (px2 + vx2, py2 + vy2)),
        )
        if intersection and all(start <= x <= end for x in intersection):
            if vx > 0 and intersection[0] > px or vx < 0 and intersection[0] < px:
                if vx2 > 0 and intersection[0] > px2 or vx2 < 0 and intersection[0] < px2:
                    # if is_example:
                    #     print(px, py, px2, py2, intersection)
                    ans += 1
print(ans)

ZERO = (0, 0, 0)


def dot(a, b):
    return sum(x * y for x, y in zip(a, b))


# Source: https://stackoverflow.com/a/1984823
def cross(a, b):
    return [a[1] * b[2] - a[2] * b[1],
            a[2] * b[0] - a[0] * b[2],
            a[0] * b[1] - a[1] * b[0]]


def minus(a, b):
    return [x - y for x, y in zip(a, b)]


# Source: https://web.archive.org/web/20180927042445/http://mathforum.org/library/drmath/view/62814.html
def three_intersection(line1, line2):
    # intersection = line_intersection(*([p[:2] for p in line] for line in (line1, line2)))
    # if intersection:
    #     ix, iy = intersection
    #     t = (ix - line2[0][0]) // line2[1][0]
    #     return line1[0][2] + t * line1[1][2] == line2[0][2] + t * line2[1][2]
    p1, v1 = line1
    p2, v2 = line2
    v_cross = cross(v1, v2)
    r_cross = cross(minus(p2, p1), v2)
    f = sum(v ** 2 for v in v_cross)
    r = sum(c ** 2 for c in r_cross)
    diff = f * r - dot(r_cross, v_cross) ** 2
    if f == 0:
        return False  # No intersection if speed vectors are parallel
    # if diff != 0:
    return diff  # No intersection if cross vectors are not parallel, so intersection when 0 is returned
    div = r // f
    a = isqrt(div)
    if a ** 2 != div:
        # assert False, "intersection, but not integer :("
        return False  # No intersection if it's not on integer coordinates
    c = tuple(p + a * v for p, v in zip(*line1))
    # return c
    # # t * (v - vr) == xr - x
    b = next((c[i] - line2[0][i]) // line2[1][i] for i in range(3) if line2[1][i] != 0)
    return b, c


# https://en.wikipedia.org/wiki/Line%E2%80%93plane_intersection#Parametric_form
def line_intersect_plane(plane, line):
    cross_plane = cross(*plane[1:])
    det = dot(minus(ZERO, line[1]), cross_plane)
    if det == 0:
        return None
    d = dot(cross_plane, minus(line[0], plane[0]))
    if d % det != 0:
        return None
    t = d // det
    return t


# for rvx in range(-5, 6):
#     for rvy in range(-5, 6):
#         for rvz in range(-5, 6):
#             vr = (rvx, rvy, rvz)
#             if vr == (0, 0, 0): continue
#             for t in range(-5, 6):
#                 pr, vh = lines[0]
#                 pr = tuple(p + v * t for p, v in zip(pr, vh))
#                 for h in lines:
#                     if not three_intersection((pr, vr), h):
#                         break
#                 else:
#                     if is_example:
#                         print(pr, vr)
a, b = lines[:2]
# L = 6 if is_example else 100  # TODO
# for t in range(-L, L):
#     pa, va = a
#     pa = tuple(p + v * t for p, v in zip(pa, va))
#     for t2 in range(-L, t):
#         pb, vb = b
#         pb = tuple(p + v * t2 for p, v in zip(pb, vb))
#         dt = t2 - t
#         vr = tuple((x - y) // dt for x, y in zip(pa, pb))
#         # pr = three_intersection(a, b)
#         pr = tuple(p + t * v for p, v in zip(pa, vr))  # pa instead of pr is intentional
#         # t * (v - vr) == xr - x
#         if all(three_intersection((pr, vr), line) for line in lines):
#             print(pr, vr, sum(pr))
#         # if is_example and t == 5 and t2 == 3:
#         #     print(pa)
#         #     print(pb)
#         #     print(pr)
#         #     print(vr)

# L = 100 if is_example else int(1e15)
# L2 = 6 if is_example else 1000
# for rvx in range(-L2, L2 + 1):
#     print(rvx)
#     for rvy in range(-L2, L2 + 1):
#         for rvz in range(-L2, L2 + 1):
#             vr = (rvx, rvy, rvz)
#             if vr == (0, 0, 0): continue
#             low, high = -L, L
#             while low < high - 1:
#                 t = (low + high) // 2
#                 pa, va = a
#                 ca = tuple(p + v * t for p, v in zip(pa, va))  # ca is now collision point of a
#                 pr = tuple(p - t * v for p, v in zip(ca, vr))  # find rock position from collision point of a
#                 intersection = three_intersection((pr, vr), b)
#                 if is_example and vr == (-3, 1, 2) and 0 < t < 10:
#                     print(pa, va, ca, pr, vr, t, intersection)
#                 if intersection == 0:
#                     break
#
#                 t += 1
#                 pa, va = a
#                 ca = tuple(p + v * t for p, v in zip(pa, va))  # ca is now collision point of a
#                 pr = tuple(p - t * v for p, v in zip(ca, vr))  # find rock position from collision point of a
#                 intersection2 = three_intersection((pr, vr), b)
#                 if is_example and vr == (-3, 1, 2) and 0 <= t < 10:
#                     print(pa, va, ca, pr, vr, t, intersection2)
#                 if intersection2 == 0:
#                     break
#
#                 if intersection < intersection2:
#                     high = t - 1
#                 else:
#                     low = t
#                 # break
#                 # if is_example and (vr == (-3, 1, 2) or vr == (3, -1, -2)) and t == 5:
#                 #     print(pa, va, t)
#                 #     print(pr, vr, t)
#                 #     print(three_intersection((pr, vr), b))
#                 # break
#             if any(i == 0 for i in (intersection, intersection2)) and \
#                     all(three_intersection((pr, vr), line) == 0 for line in lines):
#                 print(intersection, intersection2)
#                 print(pr, vr, sum(pr))

# if is_example:
#     # print(three_intersection(((-1, 0, 0), (1, 0, 0)), ((-1, -1, 0), (1, 1, 0))))
#     print(three_intersection(((24, 13, 10), (-3, 1, 2)), ((18, 19, 22), (-1, -1, -2))))
L2 = 6 if is_example else 1000
for rvx in range(1, L2 + 1):  # Start searching such that one coordinate is the lowest
    print(rvx)  # Progress printer. It printed something at iteration rvx=70 for me, after 20 minutes
    for rvy in (*range(-L2, -rvx + 1), *range(rvx, L2 + 1)):
        for rvz in (*range(-L2, -rvx + 1), *range(rvx, L2 + 1)):
            for vr in [
                (rvx, rvy, rvz),
                (-rvx, rvy, rvz),
                (rvy, rvx, rvz),
                (rvy, -rvx, rvz),
                (rvy, rvz, rvx),
                (rvy, rvz, -rvx),
            ]:
                if vr == (0, 0, 0): continue
                pa, va = a
                t = line_intersect_plane((pa, va, vr), b)
                if t is None:
                    continue

                # if is_example and vr == (-3, 1, 2) and 0 < t < 10:
                #     print(pa, va, vr, t)

                pb, vb = b
                cb = tuple(p + t * v for p, v in zip(pb, vb))  # cb is collision point of b with plane
                pr = tuple(p - t * v for p, v in zip(cb, vr))  # find rock position from collision point of b
                if all(three_intersection((pr, vr), line) == 0 for line in lines):
                    print(pr, vr, sum(pr))
