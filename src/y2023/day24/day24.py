from math import isqrt

lines = [tuple(tuple(map(int, p.split(", "))) for p in line.split("@")) for line in open("data.in").readlines()]

# Sneaky trick: my ./run script automatically replaces "datb.in" with "data.in", so use that to switch
is_example = "b" in """open("data.in")"""


def dot(a, b):
    return sum(x * y for x, y in zip(a, b))


def minus(a, b):
    return [x - y for x, y in zip(a, b)]


# Source: https://stackoverflow.com/a/20677983
# Transformed to allow lines to be passed in parametric form: point + t * direction
def line_intersection(line1, line2):
    xdiff = (-line1[1][0], -line2[1][0])
    ydiff = (-line1[1][1], -line2[1][1])

    def det(a, b):
        return a[0] * b[1] - a[1] * b[0]

    div = det(xdiff, ydiff)
    if div == 0:
        return False

    d = tuple(det(line[0], tuple(p + v for p, v in zip(*line))) for line in (line1, line2))
    return det(d, xdiff) / div, det(d, ydiff) / div


# Returns True if `line` points towards `point`.
def towards(line, point):
    p, v = line
    return dot(minus(point, p), v) > 0


lines_2d = [tuple(p[:2] for p in line) for line in lines]
start, end = (7, 27) if is_example else (200000000000000, 400000000000000)
print(sum(
    (intersection := line_intersection(a, b))
    and all(start <= x <= end for x in intersection)
    and all(towards(line, intersection) for line in (a, b))
    for i, a in enumerate(lines_2d) for b in lines_2d[i + 1:]
))


# Source: https://stackoverflow.com/a/1984823
def cross(a, b):
    return [a[1] * b[2] - a[2] * b[1],
            a[2] * b[0] - a[0] * b[2],
            a[0] * b[1] - a[1] * b[0]]


# Source: https://web.archive.org/web/20180927042445/http://mathforum.org/library/drmath/view/62814.html
def line_intersection_3d(line1, line2):
    p1, v1 = line1
    p2, v2 = line2
    v_cross = cross(v1, v2)
    r_cross = cross(minus(p2, p1), v2)
    f = sum(v ** 2 for v in v_cross)
    if f == 0:
        return  # No intersection if speed vectors are parallel
    r = sum(c ** 2 for c in r_cross)
    diff = f * r - dot(r_cross, v_cross) ** 2
    if diff != 0:
        return  # No intersection if cross vectors are not parallel, so we only have an intersection when diff == 0

    a = isqrt(r // f)
    c = tuple(p + a * v for p, v in zip(*line1))
    return towards(line2, c)


# https://en.wikipedia.org/wiki/Line%E2%80%93plane_intersection#Parametric_form
def line_intersect_plane(plane, line):
    cross_plane = cross(*plane[1:])
    det = dot(minus((0, 0, 0), line[1]), cross_plane)
    if det == 0:
        return None
    d = dot(cross_plane, minus(line[0], plane[0]))
    if d % det != 0:
        return None
    t = d // det
    return t


a, b = lines[:2]
L = 10 if is_example else 1000
for rvx in range(1, L + 1):  # Start searching such that one coordinate is the lowest
    for rvy in (*range(-L, -rvx + 1), *range(rvx, L + 1)):
        for rvz in (*range(-L, -rvx + 1), *range(rvx, L + 1)):
            for vr in [
                (rvx, rvy, rvz),
                (-rvx, rvy, rvz),
                (rvy, rvx, rvz),
                (rvy, -rvx, rvz),
                (rvy, rvz, rvx),
                (rvy, rvz, -rvx),
            ]:
                pa, va = a
                t = line_intersect_plane((pa, va, vr), b)
                if t is None:
                    continue

                pb, vb = b
                cb = tuple(p + t * v for p, v in zip(pb, vb))  # cb is collision point of b with plane
                pr = tuple(p - t * v for p, v in zip(cb, vr))  # find rock position from collision point of b
                if all(line_intersection_3d((pr, vr), line) for line in lines):
                    print(sum(pr))
