data = [[x for x in block.replace("\n", " ").split()] for block in open("data.in").read().strip().split("\n\n")]

seeds = list(map(int, data[0][1:]))
maps = [list(map(int, line[2:])) for line in data[1:]]
ans = 1e18
for seed in seeds:
    for m in maps:
        for i in range(0, len(m), 3):
            if seed in range(m[i + 1], m[i + 1] + m[i + 2]):
                seed += m[i] - m[i + 1]
                break
    ans = min(seed, ans)
print(ans)


def normalize(ranges):
    new_ranges = ranges[:1]
    for r in ranges[1:]:
        if new_ranges[-1][1] >= r[0]:
            o = new_ranges.pop()
            new_ranges.append((o[0], max(o[1], r[1])))
        else:
            new_ranges.append(r)
    return new_ranges


ranges = sorted((seed, seed + l) for seed, l in zip(seeds[::2], seeds[1::2]))
initial_sum = sum(b - a for a, b in ranges)
for r1, r2 in zip(ranges[:-1], ranges[1:]):
    assert r1[1] <= r2[0]  # whew.

for m in maps:
    new_ranges = []
    for i in range(0, len(m), 3):
        mr = m[i + 1], m[i + 1] + m[i + 2]
        md = m[i] - m[i + 1]
        next_ranges = []
        for r in ranges:
            cr = (max(r[0], mr[0]), min(r[1], mr[1]))
            if cr[0] < cr[1]:
                nr = tuple(x + md for x in cr)
                new_ranges.append(nr)
                if r[0] < cr[0]:
                    nr = (r[0], cr[0])
                    next_ranges.append(nr)
                if r[1] > cr[1]:
                    nr = (cr[1], r[1])
                    next_ranges.append(nr)
            else:
                next_ranges.append(r)
        ranges = next_ranges

    # Sorting and normalizing turned out to be unneeded after finally correctly implementing range intersections
    ranges = sorted([*ranges, *new_ranges])
    ranges = normalize(ranges)

    assert sum(b - a for a, b in ranges) == initial_sum  # This finally saved me
print(min(sorted(ranges))[0])
