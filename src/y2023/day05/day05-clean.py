data = [[x for x in block.replace("\n", " ").split()] for block in open("data.in").read().strip().split("\n\n")]

seeds = list(map(int, data[0][1:]))
mappers = [list(map(int, line[2:])) for line in data[1:]]
mappers = [[((m[i + 1], m[i + 1] + m[i + 2]), m[i] - m[i + 1]) for i in range(0, len(m), 3)] for m in mappers]
ans = 1e18
for seed in seeds:
    for mapper in mappers:
        for mapper_range, mapper_diff in mapper:
            if seed in range(*mapper_range):
                seed += mapper_diff
                break
    ans = min(seed, ans)
print(ans)

ranges = [(seed, seed + l) for seed, l in zip(seeds[::2], seeds[1::2])]
for mapper in mappers:
    mapped_ranges = []
    for mr, md in mapper:  # mapper_range, mapper_diff
        next_ranges = []
        # Who would think it's a bad idea to write `for range in ranges`? 🤔
        for r in ranges:
            covered_range = (max(r[0], mr[0]), min(r[1], mr[1]))
            if covered_range[0] < covered_range[1]:
                # We found a non-empty covered range, let's add it to the results of this mapper
                mapped_ranges.append(tuple(x + md for x in covered_range))
                # We should still process the remaining non-covered ranges
                if r[0] < covered_range[0]:
                    next_ranges.append((r[0], covered_range[0]))
                if covered_range[1] < r[1]:
                    next_ranges.append((covered_range[1], r[1]))
            else:
                # This mapper_range does not cover the current range, so try it again for the next mapper_range
                next_ranges.append(r)
        ranges = next_ranges
    # Combine processed and non-processed ranges (the latter ones need to be copied literally, as stated in the story)
    ranges = [*ranges, *mapped_ranges]
print(min(ranges)[0])
