from collections import defaultdict, deque
from itertools import product

data = [x.strip() for x in open("data.in").readlines()]
n = len(data)

bricks = list(

)
for line in data:
    s, e = line.split("~")
    s = list(map(int, s.split(",")))
    e = list(map(int, e.split(",")))
    bricks.append((s, e))
bricks.sort(key=lambda b: min(z for x, y, z in b))


def cells(s, e):
    cs = set()
    if s[0] != e[0]:
        for x in range(*sorted((s[0], e[0]))):
            cs.add((x, s[1], s[2]))
    if s[1] != e[1]:
        for x in range(*sorted((s[1], e[1]))):
            cs.add((s[0], x, s[2]))
    if s[2] != e[2]:
        for x in range(*sorted((s[2], e[2]))):
            cs.add((s[0], s[1], x))
    cs.add(tuple(e))
    return cs


filled = set()
brick_cells = []
for s, e in bricks:
    # assert sum(a != b for a, b in zip(s, e)) <= 1
    brick = cells(s, e)
    while not any((x, y, z - 1) in filled for x, y, z in brick) and all(z > 1 for x, y, z in (s, e)):
        # assert len(brick & filled) == 0
        s[2] -= 1
        e[2] -= 1
        brick = cells(s, e)
    # assert len(brick & filled) == 0
    filled |= brick
    brick_cells.append(brick)
    # print(filled)
ans = 0
supports = defaultdict(list)
rev_supports = defaultdict(list)
for i, b1 in enumerate(brick_cells):
    for j, b2 in enumerate(brick_cells):
        if i >= j:
            continue
        if any(z1 == z2 - 1 for (x1, y1, z1), (x2, y2, z2) in product(b1, b2) if x1 == x2 and y2 == y1):
            supports[j].append(i)
            rev_supports[i].append(j)
to_check = []
for i in range(n):
    if all([i] != supports[j] for j in rev_supports[i]):
        ans += 1
    else:
        to_check.append(i)
print(ans)

ans = 0
for brick in to_check:
    q = deque([brick])
    falling = set()
    while q:
        curr = q.popleft()
        falling.add(curr)
        for j in rev_supports[curr]:
            if all(k in falling for k in supports[j]):
                q.append(j)
    ans += len(falling) - 1
print(ans)
