from collections import defaultdict, deque

data = [x.strip() for x in open("data.in").readlines()]
n = len(data)

bricks = sorted((
    tuple(sorted(
        list(map(int, end_point.split(",")))
        for end_point in line.split("~")
    ))
    for line in data
), key=lambda b: b[0][2])


def cells(s, e):
    return {
        (x, y, z)
        for x in range(s[0], e[0] + 1)
        for y in range(s[1], e[1] + 1)
        for z in range(s[2], e[2] + 1)
    }


filled = set()
brick_cells = []
for s, e in bricks:
    brick = cells(s, e)
    while not any((x, y, z - 1) in filled for x, y, z in brick) and all(z > 1 for x, y, z in (s, e)):
        s[2] -= 1
        e[2] -= 1
        brick = cells(s, e)
    filled |= brick
    brick_cells.append(brick)

below = defaultdict(list)
above = defaultdict(list)
for j, b2 in enumerate(brick_cells):
    for i, b1 in enumerate(brick_cells[:j]):
        if any((x2, y2, z2 - 1) in b1 for x2, y2, z2 in b2):
            below[j].append(i)
            above[i].append(j)

to_obliterate = []  # Prepare for part 2
ans = 0
for i in range(n):
    if all(below[j] != [i] for j in above[i]):
        ans += 1
    else:
        to_obliterate.append(i)
print(ans)

ans = 0
for brick in to_obliterate:
    q = deque([brick])
    falling = set()
    while q:
        curr = q.popleft()
        falling.add(curr)
        q.extend(j for j in above[curr] if all(k in falling for k in below[j]))
    ans += len(falling) - 1
print(ans)
