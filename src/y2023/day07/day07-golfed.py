from collections import *

d = [[a, int(b)] for a, b in (line.strip().split() for line in open("data.in").readlines())]

t = lambda hand: 1 + [[1, 1, 1, 1, 1], [1, 1, 1, 2], [1, 2, 2], [1, 1, 3], [2, 3], [1, 4], [5]].index(
    sorted(Counter(hand).values()))

print(
    sum((i + 1) * b for i, (_, b) in enumerate(sorted(
        ((t(h), list(map("23456789TJQKA".index, h))), b)
        for h, b in d))),
    sum((i + 1) * b for i, (_, b) in enumerate(sorted(
        max(((t(h.replace("J", r)), list(map("J23456789TQKA".index, h))), b) for r in "23456789TQKA")
        for h, b in d))))
