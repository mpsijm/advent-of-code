from collections import Counter

data = [[a, int(b)] for a, b in (line.strip().split() for line in open("data.in").readlines())]
n = len(data)

c = {"A": 14, "K": 13, "Q": 12, "J": 11, "T": 10, **{str(i): i for i in range(2, 10)}}


def conv(hand):
    counts = Counter(hand)
    cd = [c[k] for k in hand]
    if any(v == 5 for v in counts.values()):
        return 7, cd
    if any(v == 4 for v in counts.values()):
        return 6, cd
    if set(counts.values()) == {2, 3}:
        return 5, cd
    if any(v == 3 for v in counts.values()):
        return 4, cd
    if sorted(counts.values()) == [1, 2, 2]:
        return 3, cd
    if sorted(counts.values()) == [1, 1, 1, 2]:
        return 2, cd
    return 1, cd


data_ = [(conv(h), b, h) for h, b in data]
# print(*data_, sep="\n")
ranked = sorted(data_)
# print(*ranked, sep="\n")
start_ = [i * b for i, (_, b, _) in enumerate(ranked, start=1)]
print(sum(start_))




def conv(hand):
    counts = Counter(hand)
    cd = 0
    if any(v == 5 for v in counts.values()):
        return 7, cd
    if any(v == 4 for v in counts.values()):
        return 6, cd
    if set(counts.values()) == {2, 3}:
        return 5, cd
    if any(v == 3 for v in counts.values()):
        return 4, cd
    if sorted(counts.values()) == [1, 2, 2]:
        return 3, cd
    if sorted(counts.values()) == [1, 1, 1, 2]:
        return 2, cd
    return 1, cd



c = {"A": 14, "K": 13, "Q": 12, "J": 1, "T": 10, **{str(i): i for i in range(2, 10)}}
data_ = [max((conv(h.replace("J", r)), [c[k] for k in h],  b) for r in c) for h, b in data]
# print(*data_, sep="\n")
ranked = sorted(data_)
# print(*ranked, sep="\n")
start_ = [i * b for i, (_, _, b) in enumerate(ranked, start=1)]
print(sum(start_))
