from collections import Counter

data = [[a, int(b)] for a, b in (line.strip().split() for line in open("data.in").readlines())]

c = {"A": 14, "K": 13, "Q": 12, "J": 11, "T": 10, **{str(i): i for i in range(2, 10)}}


def hand_type(hand):
    return 1 + [
        [1, 1, 1, 1, 1],
        [1, 1, 1, 2],
        [1, 2, 2],
        [1, 1, 3],
        [2, 3],
        [1, 4],
        [5]
    ].index(sorted(Counter(hand).values()))


print(sum((i + 1) * b for i, (_, _, b) in enumerate(sorted(
    (hand_type(h), [c[k] for k in h], b)
    for h, b in data
))))

c["J"] = 1
print(sum((i + 1) * b for i, (_, _, b) in enumerate(sorted(
    (hand_type(h.replace("J", max(h.replace("J", ""), key=h.count, default="A"))), [c[k] for k in h], b)
    for h, b in data
))))
