from collections import defaultdict
from subprocess import run

data = [line.strip().split() for line in open("data.in").readlines()]

# Sneaky trick: my ./run script automatically replaces "datb.in" with "data.in", so use that to switch
is_example = "b" in """open("datb.in")"""
letter = """open("datb.in")"""[9]

graph = defaultdict(set)
for line in data:
    s, *ts = line
    s = s.replace(":", "")
    for t in ts:
        graph[s].add(t)
        graph[t].add(s)


# This is not "cheating", this is "using the tools that you can work with" 😇
run(["neato", "-Tpdf", f"-ograph{letter}.pdf"],
    input=f"graph {{{';'.join(f'{s} -- {t}' for s in graph for t in graph[s])}}}", encoding="ascii")

for s, t in [
    ("hfx", "pzl"),
    ("bvb", "cmg"),
    ("nvd", "jqt"),
] if is_example else [
    ("sxx", "zvk"),
    ("njx", "pbx"),
    ("pzr", "sss"),
]:
    graph[s].remove(t)
    graph[t].remove(s)

q = [next(iter(graph))]
seen = set(q)
while q:
    curr = q.pop()
    for neigh in graph[curr]:
        if neigh not in seen:
            seen.add(neigh)
            q.append(neigh)
print((len(graph) - len(seen)) * len(seen))
