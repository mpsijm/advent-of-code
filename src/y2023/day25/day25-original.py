from collections import defaultdict

data = [line.strip().split() for line in open("data.in").readlines()]
n = len(data)

# Sneaky trick: my ./run script automatically replaces "datb.in" with "data.in", so use that to switch
is_example = "b" in """open("datb.in")"""

graph = defaultdict(set)
# print("graph {")
for line in data:
    s, *ts = line
    s = s.replace(":", "")
    for t in ts:
        graph[s].add(t)
        graph[t].add(s)
        # print(f"{s} -- {t};")
# print("}")
# Command to generate graph: `neato -Tpdf -ograph.pdf`

for s, t in [
    ("hfx", "pzl"),
    ("bvb", "cmg"),
    ("nvd", "jqt"),
] if is_example else [
    ("sxx", "zvk"),
    ("njx", "pbx"),
    ("pzr", "sss"),
]:
    graph[s].remove(t)
    graph[t].remove(s)

q = [list(graph.keys())[0]]
seen = {q[0]}
print(seen)
while q:
    curr = q.pop()
    for neigh in graph[curr]:
        if neigh not in seen:
            seen.add(neigh)
            q.append(neigh)
if is_example:
    print(graph)
    print(seen)
print(len(graph))
print(len(seen))
print((len(graph) - len(seen)) * len(seen))
