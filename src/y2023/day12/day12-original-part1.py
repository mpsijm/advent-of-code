import itertools

data = [x.strip() for x in open("data.in").readlines()]
n = len(data)

ans = 0
for line in data:
    line, order = line.split(" ")
    order = list(map(int, order.split(",")))
    a = 0
    quests = line.count("?")
    for comb in itertools.product(list(".#"), repeat=quests):
        qi = 0
        new_line = []
        for i, c in enumerate(line):
            if c == "?":
                new_line.append(comb[qi])
                qi += 1
            else:
                new_line.append(c)
        new_line = "".join(new_line).replace(".", " ")
        parts = [len(part) for part in new_line.strip().split()]
        if len(parts) == len(order) and all(c == d for c, d in zip(parts, order)):
            a += 1
    ans += a
print(ans)
