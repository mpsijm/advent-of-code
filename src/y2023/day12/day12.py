import functools

data = [x.strip().split() for x in open("data.in").readlines()]
n = len(data)


@functools.cache
def check(row, groups, i, j):
    if j == len(groups):
        return "#" not in row[i:]  # The remaining part of the row may not contain "#".
    if i >= len(row) and j < len(groups):
        return 0  # If we reached the end of the row, but still have remaining groups, abort.

    # Recursive dynamic programming: try all positions of the current group.
    ans = 0
    group = groups[j]
    for ii in range(i, len(row)):
        if ii + group > len(row):  # Stop searching when the current group wouldn't fit.
            break
        if ii > 0 and row[ii - 1] == "#":  # Stop searching when we skipped over a "#".
            break

        # To continue, the current group must not cover a "." and there may not be a "#" following it.
        if "." not in row[ii:ii + group] and row[ii + group] != "#":
            # Continue searching recursively _after_ the gap following the current group.
            ans += check(row, groups, ii + group + 1, j + 1)

    return ans


for repeat in (1, 5):
    print(sum(
        check(
            "?".join(row for _ in range(repeat)) + ".",  # Add trailing "." to simplify some out-of-bounds checks
            tuple(map(int, groups.split(","))) * repeat,
            0, 0)
        for row, groups in data
    ))
