import functools

data = [x.strip() for x in open("date.in").readlines()]
n = len(data)


@functools.lru_cache(None)
def check(line, order, i, j):
    if j == len(order):
        return "#" not in line[i:]
    if i >= len(line) and j < len(order):
        return 0
    if j > 0 and line[i - 1] == "#":
        return 0
    curr = order[j]
    a = 0
    for ii in range(i, len(line)):
        if ii + curr > len(line):
            break
        if ii > 0 and line[ii - 1] == "#":
            break
        # print(line, curr, ii, j, line[ii:ii + curr])
        if "." not in line[ii:ii + curr] and (ii + curr >= len(line) or line[ii + curr] != "#"):
            a += check(line, order, ii + curr + 1, j + 1)
    return a


for repeat in (1, 5):
    ans = 0
    for line in data:
        line, order = line.split(" ")
        order = tuple(map(int, order.split(",")))
        line = "?".join(line for _ in range(repeat))
        order *= repeat
        # print(line, order)
        ans += check(line, order, 0, 0)
    print(ans)
