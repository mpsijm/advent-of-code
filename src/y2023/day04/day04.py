from collections import defaultdict

data = [line.strip() for line in open("data.in").readlines()]
n = len(data)

num_wins = dict()  # Preparation for part 2
ans = 0
for line in data:
    id, nums = line.split(": ")
    id = int(id.split()[1])
    winning, yours = nums.split(" | ")
    winning = set(map(int, winning.split()))
    yours = list(map(int, yours.split()))
    count = sum(y in winning for y in yours)
    num_wins[id] = count
    if count:
        ans += 2 ** (count - 1)
print(ans)

new_cards = {i: 1 for i in range(1, n + 1)}
cards = defaultdict(int)
while any(v > 0 for v in new_cards.values()):
    for c in new_cards:
        cards[c] += new_cards[c]
        for cc in range(c + 1, min(n + 1, c + 1 + num_wins[c])):
            new_cards[cc] += new_cards[c]
        new_cards[c] = 0
print(sum(cards.values()))
