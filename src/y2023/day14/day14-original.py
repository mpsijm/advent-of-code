data = [list(x.strip()) for x in open("data.in").readlines()]
h, w = len(data), len(data[0])


def cycle():
    for y in range(h):
        for x in range(w):
            if data[y][x] == "O":
                data[y][x] = "."
                for yy in range(y - 1, -1, -1):
                    if data[yy][x] != ".":
                        data[yy + 1][x] = "O"
                        break
                else:
                    data[0][x] = "O"
    for x in range(w):
        for y in range(h):
            if data[y][x] == "O":
                data[y][x] = "."
                for xx in range(x - 1, -1, -1):
                    if data[y][xx] != ".":
                        data[y][xx + 1] = "O"
                        break
                else:
                    data[y][0] = "O"
    for y in range(h - 1, -1, -1):
        for x in range(w):
            if data[y][x] == "O":
                data[y][x] = "."
                for yy in range(y + 1, h):
                    if data[yy][x] != ".":
                        data[yy - 1][x] = "O"
                        break
                else:
                    data[h - 1][x] = "O"
    for x in range(w - 1, -1, -1):
        for y in range(h):
            if data[y][x] == "O":
                data[y][x] = "."
                for xx in range(x + 1, w):
                    if data[y][xx] != ".":
                        data[y][xx - 1] = "O"
                        break
                else:
                    data[y][w - 1] = "O"


C = 1000000000
counter = dict()
i = 0
while i < C:
    cycle()
    key = "\n".join("".join(line) for line in data)
    if key in counter and i < 1000:
        diff = i - counter[key]
        while i + diff < C:
            i += diff
    counter[key] = i
    i += 1
    # print(*("".join(line) for line in data), sep="\n")
    # print()

ans = 0
for y in range(h):
    count = data[y].count("O")
    wei = h - y
    ans += count * (h - y)
print(ans)
