data = [list(x.strip()) for x in open("data.in").readlines()]
h, w = len(data), len(data[0])


# Source: https://stackoverflow.com/a/48116944 (Also used in BAPC 2023 Preliminaries, problem Anti-Tetris, and yesterday)
def rot_90(blk):
    return [list(reversed(x)) for x in zip(*blk)]


def ans():
    return sum(data[y].count("O") * (h - y) for y in range(h))


C = 1000000000
counter = dict()
i = 0
while i < C:
    for j in range(4):
        for y in range(h):
            for x1 in range(w):
                if data[y][x1] == "O":
                    data[y][x1] = "."
                    for yy in range(y - 1, -1, -1):
                        if data[yy][x1] != ".":
                            data[yy + 1][x1] = "O"
                            break
                    else:
                        data[0][x1] = "O"
        if i == 0 and j == 0:
            print(ans())
        data = rot_90(data)

    key = "\n".join("".join(line) for line in data)
    if key in counter and i < 1000:
        diff = i - counter[key]
        i += diff * ((C - i) // diff)
    counter[key] = i
    i += 1

print(ans())
