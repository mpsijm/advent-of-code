# My first solution used brute force, ran with PyPy in 0.25 seconds 😇

data = [x.strip().split() for x in open("data.in").readlines()]
n = len(data)
w = len(data[0])

ans = 1
for i in range(1, w):
    t = int(data[0][i])
    d = int(data[1][i])
    c = 0
    for x in range(t):
        q = x * (t - x)
        if q > d:
            c += 1
    ans *= c
print(ans)

t = int("".join(data[0][1:]))
d = int("".join(data[1][1:]))
c = 0
for x in range(t):
    q = x * (t - x)
    if q > d:
        c += 1
print(c)
