from math import prod, sqrt

data = [x.strip().split() for x in open("data.in").readlines()]


def solve(t, d):
    # Solving x * (t - x) > d
    #    <=>  x² - tx - d > 0
    #    <=>  x is in range <s, e> = (-b ± √(b² - 4d)) / 2 (excluding boundaries)
    #    <=>  ceil(s) < x < floor(e)
    # Note that:
    #    ceil(x-1+ε..x) = x
    # But since we want strict equality, x should not map to x. We want a function `f` such that:
    #       f(x-1..x-ε) = x
    # So we use `floor` instead and add 1:
    #   floor(x-1..x-ε) = x - 1
    # Now we have:
    #   floor(s + 1) <= x <= floor(e)
    # Note that `floor` ≡ `int` in Python.
    s = int((t - sqrt(t * t - 4 * d)) / 2 + 1)

    # The number of times we win is equal to:
    #   e - s + 1
    # Note the +1, because the bounds are both inclusive.
    # Because the quadratic formula is symmetric around t/2, we can substitute e = t - s:
    return t - 2 * s + 1


print(prod(solve(int(data[0][i]), int(data[1][i])) for i in range(1, len(data[0]))))
print(solve(int("".join(data[0][1:])), int("".join(data[1][1:]))))
