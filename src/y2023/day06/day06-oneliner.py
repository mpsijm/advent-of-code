(m := __import__("math")), \
    (data := [x.strip().split() for x in open("data.in").readlines()]), \
    (solve := lambda t, d: t - 2 * int((t - m.sqrt(t * t - 4 * d)) / 2) - 1), \
    print(m.prod(solve(int(data[0][i]), int(data[1][i])) for i in range(1, len(data[0])))), \
    print(solve(int("".join(data[0][1:])), int("".join(data[1][1:]))))
