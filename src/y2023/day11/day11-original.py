data = [x.strip() for x in open("data.in").readlines()]
h, w = len(data), len(data[0])

expandX = set()
expandY = set()
for y in range(h):
    if all(c == "." for c in data[y]):
        expandY.add(y)
for x in range(w):
    if all(data[y][x] == "." for y in range(h)):
        expandX.add(x)

# print(expandX, expandY)
D = 999999
galaxies = [(x, y) for y in range(h) for x in range(w) if data[y][x] == "#"]
ans = 0
for i, g1 in enumerate(galaxies):
    for j, g2 in enumerate(galaxies):
        if i == j:
            continue
        d = abs(g1[0] - g2[0]) + abs(g1[1] - g2[1])
        for x in range(*sorted([g1[0], g2[0]])):
            if x in expandX:
                d += D
        for y in range(*sorted([g1[1], g2[1]])):
            if y in expandY:
                d += D

        ans += d
        # print(i, j, g1, g2, d)
print(ans // 2)
