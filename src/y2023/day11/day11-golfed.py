from itertools import accumulate

data = [x.strip() for x in open("data.in").readlines()]
n = len(data)  # Grid has same size in both directions

expand = [
    list(accumulate(
        all(
            data[i if d else j][j if d else i] == "."
            for j in range(n)
        )
        for i in range(n)
    ))
    for d in (0, 1)
]

galaxies = [
    [y if d else x for y in range(n) for x in range(n) if data[y][x] == "#"]
    for d in (0, 1)
]

print(*(sum(
    g1 < g2 and g2 - g1 + D * (e[g2] - e[g1])
    for d, e in enumerate(expand)
    for g1 in galaxies[d]
    for g2 in galaxies[d]
) for D in (1, 999999)))
