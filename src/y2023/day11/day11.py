from itertools import accumulate

data = [x.strip() for x in open("data.in").readlines()]
h, w = len(data), len(data[0])

expandX = list(accumulate(all(data[y][x] == "." for y in range(h)) for x in range(w)))
expandY = list(accumulate(all(c == "." for c in data[y]) for y in range(h)))
galaxies = [(x, y) for y in range(h) for x in range(w) if data[y][x] == "#"]


def solve(d):
    return sum(
        (x1 < x2 and abs(x1 - x2) + d * (expandX[x2] - expandX[x1])) +
        (y1 < y2 and abs(y1 - y2) + d * (expandY[y2] - expandY[y1]))
        for x1, y1 in galaxies
        for x2, y2 in galaxies
    )


print(solve(1))
print(solve(999999))
