import math

data = [[x for x in block.replace("\n", " ").split()] for block in open("data.in").read().strip().split("\n\n")]

instrs = data[0][0]
net = dict()
for i in range(0, len(data[1]), 4):
    a = data[1][i]
    l = data[1][i + 2][1:4]
    r = data[1][i + 3][:3]
    net[a] = (l, r)

s = 0
curr = "AAA"
while curr != "ZZZ":
    j = 0 if instrs[s % len(instrs)] == "L" else 1
    curr = net[curr][j]
    s += 1
print(s)

currs = [k for k in net if k[2] == "A"]
ms = 1
for curr in currs:
    s = 0
    while curr[2] != "Z":
        j = 0 if instrs[s % len(instrs)] == "L" else 1
        curr = net[curr][j]
        s += 1
    ms = math.lcm(ms, s)
print(ms)
