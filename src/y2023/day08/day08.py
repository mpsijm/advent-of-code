import itertools
import math

data = open("data.in").read().strip().split("\n\n")
instructions = data[0]
network = {line[:3]: (line[7:10], line[12:15]) for line in data[1].split("\n")}


def find(current, target):
    for step, instr in enumerate(itertools.cycle(instructions), start=1):
        current = network[current][instr == "R"]
        if target(current):
            return step


print(find("AAA", lambda c: c == "ZZZ"))
print(math.lcm(*(find(start, lambda c: c[2] == "Z") for start in network if start[2] == "A")))
