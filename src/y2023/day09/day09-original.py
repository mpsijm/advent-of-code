data = [[int(x) for x in line.strip().split()] for line in open("data.in").readlines()]

ans = 0
for line in data:
    a = 0
    lasts = [line[-1]]
    while True:
        diffs = [b - a for a, b in zip(line[:-1], line[1:])]
        lasts.append(diffs[-1])
        line = diffs
        if all(d == 0 for d in diffs):
            break
    ans += sum(lasts)
print(ans)

ans = 0
for line in data:
    firsts = [line[0]]
    while True:
        diffs = [a - b for a, b in zip(line[:-1], line[1:])]
        firsts.append(diffs[0])
        line = diffs
        if all(d == 0 for d in diffs):
            break
    ans += sum(firsts)
print(ans)
