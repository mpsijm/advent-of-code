data = [[int(x) for x in line.strip().split()] for line in open("data.in").readlines()]


def solve_line(line, index, op):
    ans = 0
    while True:
        ans += line[index]
        line = [op(a, b) for a, b in zip(line[:-1], line[1:])]
        if not any(line):
            break
    return ans


print(sum(solve_line(line, -1, lambda a, b: b - a) for line in data))
print(sum(solve_line(line, 0, lambda a, b: a - b) for line in data))
