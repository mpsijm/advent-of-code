import itertools

print(*(
    sum(
        sum(
            itertools.takewhile(
                lambda _: any(row),
                (
                    (row := [a - b for a, b in zip(row, row[1:])])[0]
                    for _ in itertools.cycle([0])
                )
            ),
            (row := [int(x) for x in line.split()][::d])[0]  # start value of `sum()`
        )
        for line in open("data.in").readlines()
    ) for d in [-1, 1]
))
