# Source: https://www.reddit.com/r/adventofcode/comments/18e5ytd/comment/kct99sb/
import math

print(*(
    sum(
        sum(
            int(m) * math.comb(n := line.count(" "), k) * (-1) ** (n + 1 + k)
            for k, m in enumerate(line.split()[::d][1:])
        )
        for line in open("data.in").readlines()
    ) for d in (1, -1)
))
