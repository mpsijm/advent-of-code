from collections import defaultdict, deque
from math import lcm, prod

data = [line.strip().split(" -> ") for line in open("data.in").readlines()]
n = len(data)

pulses = [0, 0]
starts = []
types = dict()
outputs = defaultdict(list)
states = defaultdict(bool)
inputs = defaultdict(dict)
for source, destinations in data:
    destinations = destinations.split(", ")
    if source == "broadcaster":
        starts = destinations
    else:
        t, source = source[0], source[1:]
        types[source] = t
        outputs[source] = destinations
        for m in destinations:
            inputs[m][source] = False

# For part 2, `lows` remembers how often each module _receives_ a "low" signal.
# For only four modules, this happens once every 3k-5k button presses.
# Turns out that these four modules are each connected to "rx" via two conjunction modules.
# Thus, multiplying the period of how often these four modules _send a low pulse_ gives the final answer.
lows = defaultdict(list)

for i in range(1, 10000):
    pulses[0] += 1  # Low pulse for the button
    q = deque([("broadcaster", s, False) for s in starts])
    while q:
        source, destination, pulse = q.popleft()
        pulses[pulse] += 1
        if destination not in types:
            continue
        if types[destination] == "%":  # flip-flop module
            if not pulse:
                states[destination] = not states[destination]
                for n in outputs[destination]:
                    q.append((destination, n, states[destination]))
        elif types[destination] == "&":  # conjunction module
            inputs[destination][source] = pulse
            new_pulse = not all(inputs[destination].values())
            if not new_pulse:
                lows[destination].append(i)
            for n in outputs[destination]:
                q.append((destination, n, new_pulse))
    if i == 1000:
        print(prod(pulses))

print(lcm(*(rec[0] for rec in lows.values() if len(rec) < 100)))
