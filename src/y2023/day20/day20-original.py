from collections import defaultdict, deque

data = [line.strip().split() for line in open("data.in").readlines()]
n = len(data)

pulses = [0, 0]
modules = dict()
inputs = defaultdict(dict)
starts = []
flops = defaultdict(list)
conjs = defaultdict(list)
for start, _, *to in data:
    to = [m.replace(",", "") for m in to]
    if start == "broadcaster":
        t = ""
        starts = to
    else:
        t = start[0]
        start = start[1:]
    modules[start] = False
    for m in to:
        modules[m] = False
        inputs[m][start] = False
    if t == "&":
        conjs[start] = to
    if t == "%":
        flops[start] = to

# print(inputs)
# print(flops)
# print(conjs)
received = defaultdict(lambda: defaultdict(list))
lows = defaultdict(list)

for i in range(1, 10000):
    if i == 1000:
        print(pulses)
        print(pulses[0] * pulses[1])
        if "rx" not in modules:
            exit()
    if i % 100000 == 0:
        print(i)
    pulses[0] += 1
    q = deque([("broadcaster", s, False) for s in starts])
    while q:
        source, curr, state = q.popleft()
        received[curr][source].append((state, i))
        if "rx" in modules and curr == "rx" and not state:  # Foiled attempt at brute-forcing
            print(i)
            exit()
        # if is_example and i < 4:
        #     print(source, "-high->" if state else "-low->", curr)
        pulses[state] += 1
        if curr in flops:
            if not state:
                modules[curr] = not modules[curr]
                for n in flops[curr]:
                    q.append((curr, n, modules[curr]))
        if curr in conjs:
            inputs[curr][source] = state
            new_state = not all(inputs[curr].values())
            if not new_state:
                lows[curr].append(i)
            for n in conjs[curr]:
                q.append((curr, n, new_state))

seen = set()
seen_twice = set()


def tree(curr, indent=0):
    if curr in {"qx", "zt", "pn", "jg"}:
        return
    if curr in seen:
        print("  " * indent + "seen")
        seen_twice.add(curr)
        return
    seen.add(curr)
    for n in inputs[curr]:
        print("  " * indent + n)
        tree(n, indent + 1)


tree("rx")
print(seen)
print(seen_twice)

ans = 1
for m, rec in lows.items():
    if len(rec) < 100:
        ans *= rec[0]
        print(m, rec)
print(ans)
