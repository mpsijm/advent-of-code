from collections import deque, defaultdict

data = [line.strip() for line in open("data.in").readlines()]
h, w = len(data), len(data[0])

allowed = {
    ">": (1, 0),
    "<": (-1, 0),
    "^": (0, -1),
    "v": (0, 1),
}

q = deque([(1, 0, {(1, 0)})])
while q:
    x, y, seen = q.popleft()
    if (x, y) == (w - 2, h - 1):
        print(len(seen) - 1)
    for d in range(4):
        dx, dy = [(1, 0), (0, 1), (-1, 0), (0, -1)][d]
        xx, yy = x + dx * 1, y + dy * 1
        if 0 <= xx < w and 0 <= yy < h:
            c = data[yy][xx]
            if (xx, yy) not in seen and c != "#" and (c not in allowed or allowed[c] == (dx, dy)):
                q.append((xx, yy, seen | {(xx, yy)}))
