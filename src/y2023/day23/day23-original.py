from collections import deque, defaultdict

data = [line.strip() for line in open("data.in").readlines()]
h, w = len(data), len(data[0])

allowed = {
    ">":
        (1, 0),
    "<": (-1, 0),
    "^": (0, -1),
    "v": (0, 1),
}

q = deque([(1, 0, 0, 0, 1, 0, 0)])
graph = defaultdict(list)
while q:
    x, y, px, py, sx, sy, dist = q.popleft()
    if (x, y) == (w - 2, h - 1):
        graph[(x, y)].append((sx, sy, dist))
        graph[(sx, sy)].append((x, y, dist))
        continue
    possible = []
    for d in range(4):
        dx, dy = [(1, 0), (0, 1), (-1, 0), (0, -1)][d]
        xx, yy = x + dx * 1, y + dy * 1
        if (xx, yy) == (px, py):
            continue
        if 0 <= xx < w and 0 <= yy < h:
            c = data[yy][xx]
            if c != "#":
                possible.append((xx, yy))
    if len(possible) == 1:
        xx, yy = possible[0]
        q.append((xx, yy, x, y, sx, sy, dist + 1))
    elif len(possible) >= 2:
        if (sx, sy, dist) in graph[(x, y)]:
            assert (x, y, dist) in graph[(sx, sy)]
            continue
        graph[(x, y)].append((sx, sy, dist))
        graph[(sx, sy)].append((x, y, dist))
        for xx, yy in possible:
            q.append((xx, yy, x, y, x, y, 1))
# print(*graph.items(), sep="\n")

# Find all paths in the new graph and return the longest one.
# There must be a better way, but PyPy finished after 70 seconds while I was thinking of one, so I guess it's fine. 🤷
maxans = 0
q = deque([(1, 0, 0, {(1, 0)})])
while q:
    x, y, dist, seen2 = q.popleft()
    if (x, y) == (w - 2, h - 1):
        maxans = max(maxans, dist)
        # print(dist)
    for neigh in sorted(graph[(x, y)], key=lambda t: t[2], ):
        xx, yy, d = neigh
        c = data[yy][xx]
        if (xx, yy) not in seen2:
            q.append((xx, yy, dist + d, seen2 | {(xx, yy)}))
print(maxans)
