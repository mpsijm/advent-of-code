from collections import defaultdict
from sys import argv

data = [line.strip() for line in open("data.in").readlines()]
h, w = len(data), len(data[0])
allowed = {">": (1, 0), "<": (-1, 0), "^": (0, -1), "v": (0, 1)}

for undirected in range(2):
    q = [(1, 0, (0, 0), (1, 0), 0)]
    graph = defaultdict(dict)
    while q:
        x, y, prev, start, dist = q.pop()
        if (x, y) == (w - 2, h - 1):
            graph[start][(x, y)] = dist
            continue  # Party! 🥳
        possible = []
        for dx, dy in (1, 0), (0, 1), (-1, 0), (0, -1):
            xx, yy = x + dx, y + dy
            if (xx, yy) == prev:
                continue  # We shouldn't walk backwards, so skip it
            if 0 <= xx < w and 0 <= yy < h:
                c = data[yy][xx]
                if c != "#" and (undirected or c not in allowed or allowed[c] == (dx, dy)):
                    possible.append((xx, yy))
        if len(possible) == 1:
            xx, yy = possible[0]
            q.append((xx, yy, (x, y), start, dist + 1))
        elif len(possible) >= 2:
            if (x, y) in graph[start]:
                continue  # We've already recorded this edge, so skip it
            graph[start][(x, y)] = dist
            for xx, yy in possible:
                q.append((xx, yy, (x, y), (x, y), 1))

    if len(argv) > 1 and argv[1] == "graph":
        open(f"graph{undirected}.dot", "w").write(
            f"""graph {{ layout=neato;overlap=false {';'.join(f'"{s}" -- "{t}" [label={d}]' for s in graph for t, d in graph[s].items() if s < t)}}}"""
            if undirected else
            f"""digraph {{ layout=dot {';'.join(f'"{s}" -> "{t}" [label={d}]' for s in graph for t, d in graph[s].items())}}}"""
        )
        continue

    # Find all paths in the new graph and return the longest one.
    # There must be a better way, but PyPy finishes within 4 seconds, so I guess it's fine. 🤷
    def dfs(curr, dist):
        if curr == len(indices) - 1:
            return dist
        seen[curr] = True
        # Unfortunately, this max-comprehension is 3½ times slower than a simple for-loop...
        # ans = max((dfs(neigh, dist + d, seen) for neigh, d in graph[curr] if not seen[neigh]), default=0)
        ans = 0
        for neigh, d in graph[curr]:
            if not seen[neigh]:
                ans = max(ans, dfs(neigh, dist + d))
        seen[curr] = False
        return ans

    # Transform the graph from dicts into lists, by identifying each crossing with a unique index.
    crossings = [*graph.keys(), (w - 2, h - 1)]
    indices = {pos: i for i, pos in enumerate(crossings)}
    graph = [[(indices[t], d) for t, d in graph[s].items()] for s in crossings]
    seen = [False] * len(crossings)
    print(dfs(0, 0))
