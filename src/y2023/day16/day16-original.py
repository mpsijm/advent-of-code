from collections import deque

data = [x.strip() for x in open("data.in").readlines()]
h, w = len(data), len(data[0])

d = {(1, 0): 0, (0, 1): 1, (0, -1): 2, (-1, 0): 3}


def in_range(x, y, dx, dy, energized):
    xx, yy = x + dx, y + dy
    if 0 <= xx < w and 0 <= yy < h and not energized[yy][xx][d[(dx, dy)]]:
        return xx, yy


def solve(x, y, dx, dy):
    o = (x, y, dx, dy)
    queue = deque([(x, y, dx, dy)])
    energized = [[[False] * 4 for _ in range(w)] for _ in range(h)]
    energized[y][x][d[(dx, dy)]] = True
    while queue:
        x, y, dx, dy = queue.popleft()
        c = data[y][x]
        if c == "|" and dx:
            for dx, dy in ((0, 1), (0, -1)):
                ir = in_range(x, y, dx, dy, energized)
                if ir:
                    xx, yy = ir
                    energized[yy][xx][d[(dx, dy)]] = True
                    queue.append((xx, yy, dx, dy))
                continue
        if c == "-" and dy:
            for dx, dy in ((1, 0), (-1, 0)):
                ir = in_range(x, y, dx, dy, energized)
                if ir:
                    xx, yy = ir
                    energized[yy][xx][d[(dx, dy)]] = True
                    queue.append((xx, yy, dx, dy))
                continue
        if c == "\\":
            dx, dy = {
                (1, 0): (0, 1),
                (0, 1): (1, 0),
                (-1, 0): (0, -1),
                (0, -1): (-1, 0),
            }[(dx, dy)]
        if c == "/":
            dx, dy = {
                (1, 0): (0, -1),
                (0, 1): (-1, 0),
                (-1, 0): (0, 1),
                (0, -1): (1, 0),
            }[(dx, dy)]
        ir = in_range(x, y, dx, dy, energized)
        if ir:
            xx, yy = ir
            energized[yy][xx][d[(dx, dy)]] = True
            queue.append((xx, yy, dx, dy))
    grid = ["".join("#" if any(cell) else "." for cell in line) for line in energized]
    # print(*grid, sep="\n")
    return sum(line.count("#") for line in grid), o, grid


print(solve(0, 0, 1, 0)[0])
m = 0, ""
for y in range(h):
    m = max(m, solve(0, y, 1, 0))
    m = max(m, solve(w - 1, y, -1, 0))
for x in range(0, w):
    m = max(m, solve(x, 0, 0, 1))
    m = max(m, solve(x, h - 1, 0, -1))
m, o, grid = m
# print(*grid, sep="\n")
# print(o)
print(m)
