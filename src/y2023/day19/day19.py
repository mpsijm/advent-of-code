from math import prod

data = [[x for x in block.replace("\n", " ").split()] for block in open("data.in").read().strip().split("\n\n")]
n = len(data)

flows = dict()
for line in data[0]:
    name, rest = line.split("{")
    *conds, last = rest[:-1].split(",")
    flows[name] = ([(c[0], c[1], int(c[2:]), d) for c, d in (cond.split(":") for cond in conds)], last)

ans = 0
for line in data[1]:
    vars = dict()
    for var in line[1:-1].split(","):
        v, val = var.split("=")
        vars[v] = int(val)

    curr = "in"
    while curr not in "AR":
        conds, last = flows[curr]
        for v, c, vv, d in conds:
            if "<" in c and vars[v] < vv or ">" in c and vars[v] > vv:
                curr = d
                break
        else:
            curr = last
    if curr == "A":
        ans += sum(vars.values())
print(ans)

stack = [({c: (1, 4000) for c in "xmas"}, "in")]
acc = []
while stack:
    vars, curr = stack.pop()
    if curr in "AR":
        if curr == "A":
            acc.append(vars)
        continue

    conds, last = flows[curr]
    for v, c, vv, d in conds:
        s, e = vars[v]
        if "<" in c and s <= vv < e:
            vars[v] = (s, vv - 1)
            stack.append((dict(vars), d))  # Append vars to stack by _value_ using `dict(vars)`, not by _reference_.
            vars[v] = (vv, e)
        if ">" in c and s < vv <= e:
            vars[v] = (vv + 1, e)
            stack.append((dict(vars), d))
            vars[v] = (s, vv)
    stack.append((dict(vars), last))
print(sum(prod(e - b + 1 for b, e in vars.values()) for vars in acc))
