data = [line.strip() for line in open("data.in").readlines()]
h, w = len(data), len(data[0])

S = 26501365
sy = next(i for i, line in enumerate(data) if "S" in line)
sx = data[sy].index("S")
grids = dict()


# Returns a list, where the $i$th element in the list is the number of reachable plots after $i$ steps.
def reachable_plots(sx, sy):
    queue = [(sx, sy)]
    plot_counts = []
    while True:
        next_queue = set()
        for x, y in queue:
            for xx, yy in (x + 1, y), (x, y + 1), (x - 1, y), (x, y - 1):
                if 0 <= xx < w and 0 <= yy < h and data[yy][xx] != "#":
                    next_queue.add((xx, yy))
        plot_counts.append(len(queue))
        # This solution hinges on the fact that the number of reachable plots starts oscillating at some point.
        # For example, if the garden would an empty 3×3 grid, this number would oscillate between 4 and 5.
        if len(plot_counts) > 3 and plot_counts[-1] == plot_counts[-3] and plot_counts[-2] == plot_counts[-4]:
            return plot_counts
        queue = next_queue


# This assumes that w == h. For the big input, it is equal to 65.
# Note that sx == sy == half_garden, I used them interchangeably in my code.
half_garden = w // 2
# Count the number of reachable plots when starting in any corner or center.
# This assumes that (sx, sy) = (half_garden, half_garden).
for y in (0, half_garden, h - 1):
    for x in (0, half_garden, h - 1):
        grids[(x, y)] = reachable_plots(x, y)


# Answer for part 1
print(grids[(sx, sy)][64])


# Calculate the number of reachable plots after saturating a garden with steps.
# In particular, this needs to calculate the parity of the number of steps taken.
def plots_after_steps(steps, sx, sy):
    parity = (steps - len(grids[(sx, sy)]) - 1) % 2
    return grids[(sx, sy)][-1 - parity]


# The number of steps is exactly 202300 * 131 + 65.
# Thus, the number of gardens that can be reached when walking straight in one direction is 202300.
# This assumption can only be made because the middle row and column of the input garden are empty.
# During debugging, I set `num_gardens = 2` (or `4` or `6`) and compared the printed answers with the spreadsheet.
num_gardens = S // w

# The width of a garden is odd. Thus, every other garden has a different number of reachable plots after saturating it.
first = plots_after_steps(S, sx, sy)
second = plots_after_steps(S - half_garden - 1, sx, 0)

# Formulas to count each type of garden are extrapolated from the spreadsheet.
# full_gardens are the fully saturated gardens in the center.
full_gardens = ((num_gardens - 1) ** 2) * first + num_gardens ** 2 * second
# straight_gardens are the gardens reachable when going straight in any direction. They have 130 steps left.
ps = half_garden * 2
straight_gardens = sum(grids[start][ps] for start in ((sx, 0), (sx, h - 1), (0, sy), (w - 1, sy)))
# diag_gardens are the first type of gardens reachable when going diagonally. They have 195 steps left.
ps = half_garden * 3
diag_gardens = sum(grids[start][ps] for start in ((0, 0), (0, h - 1), (w - 1, 0), (w - 1, h - 1))) * (num_gardens - 1)
# diag_gardens_2 are the second type of gardens reachable when going diagonally. They have 64 steps left.
ps = half_garden - 1
diag_gardens_2 = sum(grids[start][ps] for start in ((0, 0), (0, h - 1), (w - 1, 0), (w - 1, h - 1))) * num_gardens

# Add everything together, and voilà, a very specially crafted answer for this very specially crafted input 😄
print(full_gardens + straight_gardens + diag_gardens + diag_gardens_2)
