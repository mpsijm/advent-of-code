data = [line.strip() for line in open("data.in").readlines()]
h, w = len(data), len(data[0])

S = 26501365
sy = next(i for i, line in enumerate(data) if "S" in line)
sx = data[sy].index("S")
grids = dict()


def grid_lengths(sx, sy):
    # if (gx,gy,x,y) in grids: return
    queue = [(sx, sy)]
    lengths = []
    while True:
        next_queue = set()
        for x, y in queue:
            for d in range(4):
                dx, dy = [(1, 0), (0, 1), (-1, 0), (0, -1)][d]
                xx, yy = x + dx, y + dy
                if 0 <= xx < w and 0 <= yy < h and data[yy][xx] != "#":
                    next_queue.add((xx, yy))
        lengths.append(len(queue))
        if len(lengths) > 3 and lengths[-1] == lengths[-3] and lengths[-2] == lengths[-4]:
            grids[(sx, sy)] = lengths
            return lengths
        queue = next_queue


print(sx, sy, grid_lengths(sx, sy)[-2:], len(grids[(sx, sy)]))
for x, y in [
    *((0, y) for y in (0, h // 2, h - 1)),
    *((x, 0) for x in (0, w // 2, w - 1)),
    *((w - 1, y) for y in (0, h // 2, h - 1)),
    *((x, h - 1) for x in (0, w // 2, w - 1)),
]:
    print(x, y, grid_lengths(x, y)[-2:], len(grids[(x, y)]))


def plots_after_steps(steps, sx, sy):
    parity = (steps - len(grids[(sx, sy)]) - 1) % 2
    return grids[(sx, sy)][-1 - parity]


first = plots_after_steps(S, sx, sy)
print(first)
second = plots_after_steps(S - 66, sx, 0)
print(second)
third = plots_after_steps(S - 66 - 66, 0, 0)
print(third)
# print(*[(i, c) for i, c in enumerate(grids[(sx, sy)])], sep="\n")
num_plots =  S // w
full_plots = ((num_plots - 1) ** 2) * first + (num_plots) ** 2 * second
print(full_plots)
ps = 65 * 2
half_plots = [grids[(sx, 0)][ps], grids[(sx, h - 1)][ps], grids[(0, sy)][ps], grids[(w - 1, sy)][ps]]
print(half_plots)
print(sum(half_plots))
ps = 65 * 3
diag_plots = [grids[(0, 0)][ps], grids[(0, h - 1)][ps], grids[(w - 1, 0)][ps], grids[(w - 1, h - 1)][ps]]
print(diag_plots)
diag_count = sum(diag_plots) * (num_plots - 1)
print(diag_count)
ps = 65 - 1
diag_plots_2 = [grids[(0, 0)][ps], grids[(0, h - 1)][ps], grids[(w - 1, 0)][ps], grids[(w - 1, h - 1)][ps]]
print(diag_plots_2)
diag_count_2 = sum(diag_plots_2) * (num_plots)
print(diag_count_2)
print(full_plots + sum(half_plots) + diag_count + diag_count_2)
