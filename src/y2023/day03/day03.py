data = [x.strip() for x in open("data.in").readlines()]
h, w = len(data), len(data[0])


def is_ok(c):
    return c != "." and not c.isdigit()


digits = dict()  # Preparation for part 2

sum = 0
y = 0
while y < h:
    x = 0
    while x < w:
        if data[y][x].isdigit():
            d = data[y][x]
            firstx = x
            while x + 1 < w and data[y][x + 1].isdigit():
                x += 1
                d += data[y][x]
            if (any(is_ok(data[yy][xx]) for yy in (y - 1, y + 1) for xx in range(firstx - 1, x + 2)
                    if 0 <= xx < w and 0 <= yy < h)
                    or firstx > 0 and is_ok(data[y][firstx - 1])
                    or x < w - 2 and is_ok(data[y][x + 1])):
                sum += int(d)
            for xx in range(firstx, x + 1):
                digits[(xx, y)] = int(d)
        x += 1
    y += 1
print(sum)

sum = 0
y = 0
while y < h:
    x = 0
    while x < w:
        if data[y][x] == "*":
            ds = set()
            for yyy in range(-1, 2):
                for xxx in range(-1, 2):
                    p = (x + xxx, y + yyy)
                    if p != (0, 0) and p in digits:
                        ds.add(digits[p])
            if len(ds) == 2:
                ds = list(ds)
                sum += ds[0] * ds[1]
        x += 1
    y += 1
print(sum)
