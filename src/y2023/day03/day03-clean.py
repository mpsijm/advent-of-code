import re
from math import prod

data = [x.strip() for x in open("data.in").readlines()]
h, w = len(data), len(data[0])

digits = dict()  # Preparation for part 2

numbers = [
    [(int(m.group()), m.span()) for m in re.finditer("\\d+", line)]
    for line in data
]

ans = 0
for y, line in enumerate(numbers):
    for number, (start, end) in line:
        if any(
                c != "." and not c.isdigit()
                for line in data[max(0, y - 1):y + 2]
                for c in line[max(0, start - 1): end + 1]
        ):
            ans += number
        for xx in range(start, end):
            digits[(xx, y)] = number
print(ans)

ans = 0
for y in range(h):
    for x in range(w):
        if data[y][x] == "*":
            ds = {
                digits[(xx, yy)]
                for yy in range(y - 1, y + 2)
                for xx in range(x - 1, x + 2)
                if (xx, yy) in digits
            }
            if len(ds) == 2:
                ans += prod(ds)
print(ans)
