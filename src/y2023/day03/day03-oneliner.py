(data := [x.strip() for x in open("data.in").readlines()]), \
    (numbers := [
        [
            (int(m.group()), m.span())
            for m in __import__("re").finditer("\\d+", line)
        ]
        for line in data
    ]), \
    print(sum(
        number
        for y, line in enumerate(numbers)
        for number, (start, end) in line
        if any(
            c != "." and not c.isdigit()
            for line in data[max(0, y - 1):y + 2]
            for c in line[max(0, start - 1): end + 1]
        )
    )), \
    print(sum(
        __import__("math").prod(ds)
        for y, line in enumerate(data)
        for x, c in enumerate(line)
        if c == "*" and len(ds := {
            number
            for l in numbers[max(0, y - 1):y + 2]
            for (number, span) in l
            if any(xx in range(*span) for xx in range(x - 1, x + 2))
        }) == 2
    ))
