data = [x.strip() for x in open("data.in").readlines()]

ans = 0
for line in data:
    l = len(line)
    f = line[:l // 2]
    s = line[l // 2:]
    for c in f:
        if c in s:
            if "a" <= c <= "z":
                ans += ord(c) - 96
            else:
                ans += ord(c) - ord("A") + 27
            break
print(ans)

done = False
ans = 0
for i in range(0, len(data), 3):
    for c in data[i]:
        if c in data[i + 1] and c in data[i + 2]:
            if "a" <= c <= "z":
                ans += ord(c) - 96
            else:
                ans += ord(c) - ord("A") + 27
            break
print(ans)
