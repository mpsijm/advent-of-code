data = [x.strip() for x in open("data.in").readlines()]

print(sum(
    next(
        ord(c) - (96 if c >= "a" else 38)
        for c in line[:len(line) // 2]
        if c in line[len(line) // 2:]
    )
    for line in data
))

print(sum(
    next(
        ord(c) - (96 if c >= "a" else 38)
        for c in data[i]
        if c in data[i + 1] and c in data[i + 2]
    )
    for i in range(0, len(data), 3)
))
