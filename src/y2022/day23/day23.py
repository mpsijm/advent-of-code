data = [x.strip() for x in open("data.in").readlines()]
elves = set((x, y) for y, line in enumerate(data) for x, c in enumerate(line) if c == "#")


def eight(x, y):
    return [
        (x - 1, y - 1),
        (x, y - 1),
        (x + 1, y - 1),
        (x - 1, y + 1),
        (x, y + 1),
        (x + 1, y + 1),
        (x - 1, y),
        (x + 1, y),
    ]


props = [
    (
        lambda x, y: all((xx, y - 1) not in elves for xx in range(x - 1, x + 2)),
        lambda x, y: (x, y - 1)
    ), (
        lambda x, y: all((xx, y + 1) not in elves for xx in range(x - 1, x + 2)),
        lambda x, y: (x, y + 1)
    ), (
        lambda x, y: all((x - 1, yy) not in elves for yy in range(y - 1, y + 2)),
        lambda x, y: (x - 1, y)
    ), (
        lambda x, y: all((x + 1, yy) not in elves for yy in range(y - 1, y + 2)),
        lambda x, y: (x + 1, y)
    )
]

i = 0
while True:
    proposals = dict()
    propcount = dict()
    for x, y in elves:
        xx, yy = x, y
        if all(p not in elves for p in eight(x, y)):
            xx, yy = x, y
        else:
            for j in range(4):
                cond, newpos = props[(i + j) % 4]
                if cond(x, y):
                    xx, yy = newpos(x, y)
                    break
        proposals[(x, y)] = (xx, yy)
        if (xx, yy) not in propcount:
            propcount[(xx, yy)] = 0
        propcount[(xx, yy)] += 1
    elves = set(p if propcount[q] != 1 else q for p, q in proposals.items())
    # print(elves)
    minx = min(x for x, y in elves)
    maxx = max(x for x, y in elves)
    miny = min(y for x, y in elves)
    maxy = max(y for x, y in elves)
    # if i % 100 == 0:
    #     print(i, minx, maxx, miny, maxy, len(elves))
    #     for y in range(miny, maxy + 1):
    #         for x in range(minx, maxx + 1):
    #             print("#" if (x, y) in elves else ".", end="")
    #         print()
    #     print()

    if i == 9:
        print((maxx - minx + 1) * (maxy - miny + 1) - len(elves))
    if all(p == q for p, q in proposals.items()):
        break
    i += 1
print(i + 1)
