data = [int(x) for x in open("data.in").readlines()]
data_copy = data.copy()

data = [(i, x) for i, x in enumerate(data)]
n = len(data)
for k in range(n):
    j, (i, x) = [(j, (i, x)) for j, (i, x) in enumerate(data) if i == k][0]
    data = [*data[:j], *data[j+1:]]
    j = (j + x) % (n - 1)
    data = [*data[:j], (i, x), *data[j:]]
ind = [(j, (i, x)) for j, (i, x) in enumerate(data) if x == 0][0][0]
print(sum(data[(ind + k) % n][1] for k in (1000, 2000, 3000)))

data = data_copy
key = 811589153
data = [(i, x * key) for i, x in enumerate(data)]
n = len(data)
for k in range(n * 10):
    j, (i, x) = [(j, (i, x)) for j, (i, x) in enumerate(data) if i == k % n][0]
    data = [*data[:j], *data[j+1:]]
    j = (j + x) % (n - 1)
    data = [*data[:j], (i, x), *data[j:]]
ind = [(j, (i, x)) for j, (i, x) in enumerate(data) if x == 0][0][0]
print(sum(data[(ind + k) % n][1] for k in (1000, 2000, 3000)))
