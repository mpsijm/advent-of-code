from functools import cmp_to_key


def compare(a, b):
    for c, d in zip(a, b):
        if type(c) == int and type(d) == int:
            if c - d != 0: return c - d
            continue
        if type(c) == int: c = [c]
        if type(d) == int: d = [d]
        o = compare(c, d)
        if o != 0: return o
    return len(a) - len(b)


data = [eval(block.replace("\n", ",")) for block in open("data.in").read().split("\n\n")]
print(sum(i + 1 for i, (a, b) in enumerate(data) if compare(a, b) < 0))
packets = sorted(
    [[], [[2]], [[6]], *(x for line in data for x in line)],
    key=cmp_to_key(compare))
print(packets.index([[2]]) * packets.index([[6]]))
