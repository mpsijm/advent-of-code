print(*(f(
    c := lambda a, b: next((o for o in (
        x - y if type(x) is int and type(y) is int
        else c([x] if type(x) is int else x, [y] if type(y) is int else y)
        for x, y in zip(a, b)
    ) if o != 0), len(a) - len(b)),
    [eval(b.replace("\n", ",")) for b in open("data.in").read().split("\n\n")]
) for f in (
    lambda c, d: sum(i + 1 for i, (a, b) in enumerate(d) if c(a, b) < 0),
    lambda c, d: __import__("math").prod(
        i + 1 for i, p in enumerate(sorted(
            [[[2]], [[6]], *(x for l in d for x in l)], key=__import__("functools").cmp_to_key(c)
        )) if p in ([[2]], [[6]]))
)))
