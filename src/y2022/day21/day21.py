data = [line.strip().split(": ") for line in open("data.in").readlines()]

# Sneaky trick: my ./run script automatically replaces "datb.in" with "data.in", so use that to switch
is_example = "b" in """open("datb.in")"""

one, two = None, None
monkeys = dict()
for line in data:
    m, job = line
    if job.isdigit():
        monkeys[m] = (lambda i: lambda: i)(int(job))
    else:
        monkeys[m] = (lambda left, op, right: {
            "+": lambda: monkeys[left]() + monkeys[right](),
            "-": lambda: monkeys[left]() - monkeys[right](),
            "*": lambda: monkeys[left]() * monkeys[right](),
            "/": lambda: monkeys[left]() / monkeys[right](),
        }[op])(*job.split())
        if m == "root":
            one, _, two = job.split()
print(int(monkeys["root"]()))

low, high = 1, int(1e15)
while low <= high:
    mid = (low + high) // 2
    monkeys["humn"] = lambda: mid
    one_val = monkeys[one]()
    two_val = monkeys[two]()
    if one_val == two_val:
        print(mid)
        break
    elif one_val < two_val if is_example else one_val > two_val:
        low = mid + 1
    else:
        high = mid - 1
