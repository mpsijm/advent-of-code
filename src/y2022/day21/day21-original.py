# Code used for my original submission. `day21.py` is optimised after-the-fact.
def calc(m):
    job = monkeys[m]
    if type(job) == int:
        return job
    else:
        c, d, e = job
        c = calc(c)
        e = calc(e)
        return eval(f"{c} {d} {e}")


data = [line.strip().split(": ") for line in open("data.in").readlines()]

# Sneaky trick: my ./run script automatically replaces "datb.in" with "data.in", so use that to switch
is_example = "b" in """open("datb.in")"""

monkeys = dict()
for line in data:
    m, job = line
    if job.isdigit():
        i = int(job)
        monkeys[m] = i
    else:
        monkeys[m] = job.split()

print(int(calc("root")))

c, d, e = monkeys["root"]
low, high = 1, int(1e15)
while low <= high:
    mid = (low + high) // 2
    monkeys["humn"] = mid
    calc1 = calc(c)
    calc2 = calc(e)
    if calc1 == calc2:
        print(mid)
        break
    elif calc1 < calc2 if is_example else calc1 > calc2:
        low = mid + 1
    else:
        high = mid - 1
