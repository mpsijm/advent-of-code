data = [x.strip() for x in open("data.in").readlines()][0]
n = len(data)

w = 7
t = 100000
grid = [["."] * w for _ in range(t * 2)]
j = 0
blocks = [["####"], [".#.", "###", ".#."], ["###", "..#", "..#"], ["#", "#", "#", "#"], ["##", "##"]]
blocks = [[(x, y) for y, line in enumerate(block) for x, c in enumerate(line) if c == "#"] for block in blocks]
tops = [0]
top = 0
save = {}
first, second = None, None
tt = 1000000000000
for b in range(t):
    if b % 5 == 0:
        if (j % n) in save:
            if not first:
                first = b
            elif save[j % n] == first:
                second = b
        save[j % n] = b
    if second and b > second + (tt - first) % (second - first) and b > 2022:
        break

    block = [(x + 2, y + top + 3) for x, y in blocks[b % 5]]
    while True:
        if data[j % n] == ">":
            if all(x < w - 1 and grid[y][x + 1] == "." for x, y in block):
                block = [(x + 1, y) for x, y in block]
        else:
            if all(x > 0 and grid[y][x - 1] == "." for x, y in block):
                block = [(x - 1, y) for x, y in block]
        j += 1
        if all(y > 0 and grid[y - 1][x] == "." for x, y in block):
            block = [(x, y - 1) for x, y in block]
        else:
            for x, y in block:
                grid[y][x] = "#"
                top = max(top, y + 1)
            break
    tops.append(top)

print(tops[2022])
mult = (tt - first) // (second - first)
rest = (tt - first) % (second - first)
last = second + rest
print(tops[first] + (tops[second] - tops[first]) * mult + (tops[last] - tops[second]))
