# Code used for my original submission. `day17.py` is optimised after-the-fact.
data = [x.strip() for x in open("data.in").readlines()][0]
n = len(data)


w = 7
t = 100000
grid = [[" "] * w for _ in range(t * 2)]
j = 0
blocks = [["####"], [".#.", "###", ".#."], ["###", "..#", "..#"], ["#", "#", "#", "#"], ["##", "##"]]
blocks = [[(x, y) for y, line in enumerate(block) for x, c in enumerate(line) if c == "#"] for block in blocks]
tops = []
top = 0
save = {}
tt = 1000000000000
for b in range(t):
    # if b % 5 == 0:
    #     if (j % n) in save:
    #         print(b, j, save[j % n], b - save[j%n])
    #     save[j % n] = b
    block = blocks[b % 5]
    tops.append(top)
    block = [(x + 2, y + top + 3) for x, y in block]
    while True:
        if data[j % n] == ">":
            if all(x < w - 1 and grid[y][x + 1] == " " for x, y in block):
                block = [(x + 1, y) for x, y in block]
        if data[j % n] == "<":
            if all(x > 0 and grid[y][x - 1] == " " for x, y in block):
                block = [(x - 1, y) for x, y in block]
        j += 1
        if all(y > 0 and grid[y - 1][x] == " " for x, y in block):
            block = [(x, y - 1) for x, y in block]
        else:
            for x, y in block:
                grid[y][x] = "#"
                top = max(top, y + 1)
            break
print(tops[2022])
tops.append(top)
a, b = 1560, 22200  # Real input
# a, b = 50, 155  # Example
mult = (tt - a) // (b - a)
rest = (tt - a) % (b - a)
c = b + rest
# print(mult, tops[b] - tops[a])
print(tops[a] + (tops[b] - tops[a]) * mult + (tops[c] - tops[b]))

# for y in range(tops[a], tops[a] - 10, -1):
#     print("".join(grid[y]))
# print()
# for y in range(tops[b], tops[b] - 10, -1):
#     print("".join(grid[y]))
