config, moves = open("data.in").read().split("\n\n")

n = int(config[-2])
boxes = [[] for _ in range(n)]
config = config.split("\n")
for x in range(n):
    for line in config[-2::-1]:
        p = 1 + x * 4
        if line[p] != " ":
            boxes[x].append(line[p])

boxes2 = [[c for c in b] for b in boxes]
for line in moves.strip().split("\n"):
    _, n, _, a, _, b = line.split()
    n, a, b = int(n), int(a), int(b)
    boxes2[b - 1].extend(boxes2[a - 1][-n:])
    for _ in range(n):
        boxes2[a - 1].pop()
        boxes[b - 1].append(boxes[a - 1].pop())
print(*("".join(b[-1] for b in bs) for bs in (boxes, boxes2)), sep="\n")
