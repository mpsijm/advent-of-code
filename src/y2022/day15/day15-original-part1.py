# Code used for my original submission for part 1. `day15.py` is optimised after-the-fact.
data = [tuple(tuple(int(s.split("=")[1]) for s in p.split(", ")) for p in line.split(": "))
        for line in open("data.in").readlines()]

# Sneaky trick: my ./run script automatically replaces "datb.in" with "data.in", so use that to switch
y = 10 if "b" in """open("datb.in")""" else 2_000_000

ans = 0
for x in range(-10_000_000, 10_000_000):
    for (sx, sy), (bx, by) in data:
        if x == bx and y == by:
            continue
        if abs(x - sx) + abs(y - sy) <= abs(sx - bx) + abs(sy - by):
            ans += 1
            break
print(ans)
