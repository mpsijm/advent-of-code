# Code used for my original submission for part 2. `day15.py` is optimised after-the-fact.
def edge(sx, sy, bx, by, add):
    dist = abs(sx - bx) + abs(sy - by) + add
    for x in range(dist):
        yield sx + x, sy + dist - x
    for x in range(dist):
        yield sx + dist - x, sy - x
    for x in range(dist):
        yield sx - x, sy - dist + x
    for x in range(dist):
        yield sx - dist + x, sy + x


data = [tuple(tuple(int(s.split("=")[1]) for s in p.split(", ")) for p in line.split(": "))
        for line in open("data.in").readlines()]

# Sneaky trick: my ./run script automatically replaces "datb.in" with "data.in", so use that to switch
t = 20 if "b" in """open("data.in")""" else 4_000_000

for (sx, sy), (bx, by) in data:
    for x, y in edge(sx, sy, bx, by, 1):
        if not (0 <= x <= t and 0 <= y <= t):
            continue
        for (sx, sy), (bx, by) in data:
            if x == bx and y == by:
                break
            if abs(x - sx) + abs(y - sy) <= abs(sx - bx) + abs(sy - by):
                break
        else:
            print(x * 4_000_000 + y)
            exit()
print("borked")
