import itertools

data = [tuple(tuple(int(s.split("=")[1]) for s in p.split(", ")) for p in line.split(": "))
        for line in open("data.in").readlines()]

# Sneaky trick: my ./run script automatically replaces "datb.in" with "data.in", so use that to switch
t = 20 if "b" in """open("datb.in")""" else 4_000_000
y = t // 2

minx, maxx, beacons = 0, 0, set()
for (sx, sy), (bx, by) in data:
    if by == t // 2:
        beacons.add(by)
    dist = abs(sx - bx) + abs(sy - by)
    if abs(y - sy) <= dist:
        for x in (sx - dist + abs(y - sy), sx + dist - abs(y - sy)):
            minx = min(minx, x)
            maxx = max(maxx, x)
print(maxx - minx + 1 - len(beacons))

data = [((sx, sy), (bx, by), abs(sx - bx) + abs(sy - by)) for (sx, sy), (bx, by) in data]
for ((x1, y1), _, r1), ((x2, y2), _, r2), ((x3, y3), _, r3), ((x4, y4), _, r4) \
        in itertools.permutations(data, 4):
    if x1 < x2 and y1 < y2 and x3 < x4 and y3 > y4:
        if abs(x2 - x1) + abs(y2 - y1) - r1 - r2 == 2 \
                and abs(x4 - x3) + abs(y4 - y3) - r3 - r4 == 2:
            # Line 1: (x1 + r1 + 1, y1) -> (x1, y1 + r1 + 1)  (bottom-right to top-left)
            # Line 2: (x3 + r3 + 1, y3) -> (x3, y3 - r3 - 1)  (top-right to bottom-left)
            # y = -x + y1 + x1 + r1 + 1
            # y = x + y3 - x3 - r3 - 1
            # x + y3 - x3 - r3 - 1 = -x + y1 + x1 + r1 + 1
            # 2x = y1 + x1 + r1 + 1 - y3 + x3 + r3 + 1
            # x = (y1 + x1 + r1 - y3 + x3 + r3) // 2 + 1
            x = (y1 + x1 + r1 - y3 + x3 + r3) // 2 + 1
            y = x + y3 - x3 - r3 - 1
            if abs(x - x1) + abs(y - y1) == r1 + 1 \
                    and abs(x - x2) + abs(y - y2) == r2 + 1 \
                    and abs(x - x3) + abs(y - y3) == r3 + 1 \
                    and abs(x - x4) + abs(y - y4) == r4 + 1:
                print(x * 4_000_000 + y)
                break
else:
    print("borked")
