grid, instrs = [[x for x in block.split("\n")] for block in open("data.in").read().split("\n\n")]
instrs = instrs[0]

# Sneaky trick: my ./run script automatically replaces "datb.in" with "data.in", so use that to switch
is_example = "b" in """open("datb.in")"""

h = len(grid)
w = len(grid[0])

nums = []
dirs = []
i = 0
while i < len(instrs):
    j = i
    while j < len(instrs) and instrs[j] not in "LR":
        j += 1
    nums.append(int(instrs[i:j]))
    i = j
    if j < len(instrs):
        dirs.append(instrs[j])
        j += 1
        i += 1

x, y = 0, 0
while grid[y][x] == " ":
    x += 1
facing = 0


def face(x, y, facing):
    if facing == 0:
        return x + 1, y
    if facing == 1:
        return x, y + 1
    if facing == 2:
        return x - 1, y
    if facing == 3:
        return x, y - 1
    print("unknown", facing)


for n, d in zip(nums, ["", *dirs]):
    if d == "R":
        facing = (facing + 1) % 4
    if d == "L":
        facing = (facing - 1) % 4
    for _ in range(n):
        xx, yy = face(x, y, facing)
        if (not (0 <= yy < h and 0 <= xx < len(grid[yy]))) or grid[yy][xx] == " ":
            if facing == 0:
                xx = next(i for i in range(len(grid[yy])) if grid[yy][i] != " ")
            if facing == 2:
                xx = next(i for i in range(len(grid[yy]) - 1, -1, -1) if grid[yy][i] != " ")
            if facing == 1:
                yy = next(i for i in range(h) if xx <= len(grid[i]) and grid[i][xx] != " ")
            if facing == 3:
                yy = next(i for i in range(h - 1, -1, -1)
                          if xx < len(grid[i])
                          and grid[i][xx] != " ")
        if grid[yy][xx] == "#":
            break
        x, y = xx, yy

print(1000 * (y + 1) + 4 * (x + 1) + facing)

x, y = 0, 0
while grid[y][x] == " ":
    x += 1
facing = 0

for n, d in zip(nums, ["", *dirs]):
    if d == "R":
        facing = (facing + 1) % 4
    if d == "L":
        facing = (facing - 1) % 4
    for _ in range(n):
        xx, yy = face(x, y, facing)
        old_facing = facing
        if is_example:
            s = 4
            if facing == 0:
                if xx == 12 and 0 <= yy < 4:
                    xx, yy, facing = 15, 11 - yy % s, 2
                if xx == 12 and 4 <= yy < 8:
                    xx, yy, facing = 15 - yy % s, 8, 1
                if xx == 16 and 8 <= yy < 12:
                    xx, yy, facing = 11, 3 - yy % s, 2
            if facing == 1:
                if yy == 8 and 0 <= xx < 4:
                    xx, yy, facing = 11 - xx % s, 11, 3
                if yy == 8 and 4 <= xx < 8:
                    xx, yy, facing = 8, 11 - xx % s, 0
                if yy == 12 and 8 <= xx < 12:
                    xx, yy, facing = 3 - xx % s, 7, 3
                if yy == 12 and 12 <= xx < 16:
                    xx, yy, facing = 8 + xx % s, 0, 1
            if facing == 2:
                if xx == 7 and 0 <= yy < 4:
                    xx, yy, facing = 7 - yy % s, 4, 2
                if xx == -1 and 4 <= yy < 8:
                    xx, yy, facing = 15 - yy % s, 11, 1
                if xx == 7 and 8 <= yy < 12:
                    xx, yy, facing = 7 - yy % s, 7, 3
            if facing == 3:
                if yy == 3 and 0 <= xx < 4:
                    xx, yy, facing = 11 - xx % s, 0, 1
                if yy == 3 and 4 <= xx < 8:
                    xx, yy, facing = 8, xx % s, 0
                if yy == -1 and 8 <= xx < 12:
                    xx, yy, facing = 15 - xx % s, 11, 3
                if yy == 7 and 12 <= xx < 16:
                    xx, yy, facing = 11, 7 - xx % s, 2
        else:
            s = 50
            if facing == 0:
                if xx == 150 and 0 <= yy < 50:
                    xx, yy, facing = 99, 149 - yy % s, 2
                if xx == 100 and 50 <= yy < 100:
                    xx, yy, facing = 100 + yy % s, 49, 3
                if xx == 100 and 100 <= yy < 150:
                    xx, yy, facing = 149, 49 - yy % s, 2
                if xx == 50 and 150 <= yy < 200:
                    xx, yy, facing = 50 + yy % s, 149, 3
            if facing == 1:
                if yy == 200 and 0 <= xx < 50:
                    xx, yy, facing = 100 + xx % s, 0, 1
                if yy == 150 and 50 <= xx < 100:
                    xx, yy, facing = 49, 150 + xx % s, 2
                if yy == 50 and 100 <= xx < 150:
                    xx, yy, facing = 99, 50 + xx % s, 2
            if facing == 2:
                if xx == 49 and 0 <= yy < 50:
                    xx, yy, facing = 0, 149 - yy % s, 0
                if xx == 49 and 50 <= yy < 100:
                    xx, yy, facing = yy % s, 100, 1
                if xx == -1 and 100 <= yy < 150:
                    xx, yy, facing = 50, 49 - yy % s, 0
                if xx == -1 and 150 <= yy < 200:
                    xx, yy, facing = 50 + yy % s, 0, 1
            if facing == 3:
                if yy == 99 and 0 <= xx < 50:
                    xx, yy, facing = 50, 50 + xx % s, 0
                if yy == -1 and 50 <= xx < 100:
                    xx, yy, facing = 0, 150 + xx % s, 0
                if yy == -1 and 100 <= xx < 150:
                    xx, yy, facing = xx % s, 199, 3

        if grid[yy][xx] == " ":
            print("borked")
            exit()
        if grid[yy][xx] == "#":
            facing = old_facing
            break
        x, y = xx, yy

print(1000 * (y + 1) + 4 * (x + 1) + facing)
