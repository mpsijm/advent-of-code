from itertools import takewhile

data = [[int(c) for c in x.strip()] for x in open("data.in").readlines()]
h, w = len(data), len(data[0])

print(sum(all(data[y][xx] < data[y][x] for xx in range(x)) or
          all(data[y][xx] < data[y][x] for xx in range(x + 1, w)) or
          all(data[yy][x] < data[y][x] for yy in range(y)) or
          all(data[yy][x] < data[y][x] for yy in range(y + 1, h))
          for x in range(w) for y in range(h)))

print(max((((l := len(list(takewhile(lambda xx: data[y][xx] < data[y][x], range(x - 1, -1, -1))))) != x) + l) *
          (((r := len(list(takewhile(lambda xx: data[y][xx] < data[y][x], range(x + 1, w))))) != w - x - 1) + r) *
          (((t := len(list(takewhile(lambda yy: data[yy][x] < data[y][x], range(y - 1, -1, -1))))) != y) + t) *
          (((b := len(list(takewhile(lambda yy: data[yy][x] < data[y][x], range(y + 1, h))))) != h - y - 1) + b)
          for x in range(w) for y in range(h)))
