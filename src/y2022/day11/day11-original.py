# Code used for my original submission. `day11.py` is optimised after-the-fact.
data = [[x for x in block.split("\n")] for block in open("data.in").read().strip().split("\n\n")]

ops = [line[2].split(" = ")[1] for line in data]
divs = [int(line[3].split(" by ")[1]) for line in data]
mod = 1
for div in divs:
    mod *= div
to1 = [int(line[4].split()[-1]) for line in data]
to0 = [int(line[5].split()[-1]) for line in data]
for rounds in (20, 10000):
    monkeys = [list(map(int, line[1].split(": ")[1].split(", "))) for line in data]
    inspects = [0] * len(data)
    for _ in range(rounds):
        # if _ == 1 or _ == 20 or _ % 1000 == 0:
        #     print(_, inspects)
        for i, monkey in enumerate(monkeys):
            for item in monkey:
                inspects[i] += 1
                op = ops[i]
                item = eval(op.replace("old", str(item)))
                item %= mod
                if rounds == 20:
                    item //= 3
                if item % divs[i] == 0:
                    monkeys[to1[i]].append(item)
                else:
                    monkeys[to0[i]].append(item)
            monkey.clear()
    inspects.sort()
    print(inspects[-1] * inspects[-2])
