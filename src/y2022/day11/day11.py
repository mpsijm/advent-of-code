import math

data = [[x for x in block.split("\n")] for block in open("data.in").read().strip().split("\n\n")]
monkeys = [(
    (lambda x: x * x) if "old * old" in op else (
        lambda operand: (lambda x: x + operand) if "+" in op else (lambda x: x * operand)
    )(int(op.split()[-1])),
    int(div.split(" by ")[1]),
    int(to1.split()[-1]),
    int(to0.split()[-1]),
) for _, init, op, div, to1, to0 in data]
mod = math.prod(monkey[1] for monkey in monkeys)

for rounds in (20, 10000):
    monkeys_items = [list(map(int, line[1].split(": ")[1].split(", "))) for line in data]
    inspects = [0] * len(data)
    for _ in range(rounds):
        # if _ == 1 or _ == 20 or _ % 1000 == 0:
        #     print(_, inspects)
        for i, (items, (op, div, to1, to0)) in enumerate(zip(monkeys_items, monkeys)):
            for item in items:
                inspects[i] += 1
                item = op(item)
                item %= mod
                if rounds == 20:
                    item //= 3
                if item % div == 0:
                    monkeys_items[to1].append(item)
                else:
                    monkeys_items[to0].append(item)
            items.clear()
    inspects.sort()
    print(inspects[-1] * inspects[-2])
