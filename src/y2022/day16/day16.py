import functools

data = [line.strip().split() for line in open("data.in").readlines()]
data = {line[1]: (int(line[4].split("=")[1].split(";")[0]), [x.replace(",", "") for x in line[9:]]) for line in data}

useful = [x for x, (p, _) in data.items() if p]
dist = {x: {y: 100000 for y in data} for x in data}
for x in data:
    dist[x][x] = 0
for x in data:
    for y in data[x][1]:
        dist[x][y] = 1
        dist[y][x] = 1
for k in data:
    for i in data:
        for j in data:
            if dist[i][j] > dist[i][k] + dist[k][j]:
                dist[i][j] = dist[i][k] + dist[k][j]


@functools.cache
def pressure1(valve, valves, minutes):
    best = 0
    for neigh in valves:
        rest = tuple(x for x in valves if x != neigh)
        if dist[valve][neigh] >= minutes:
            continue
        rate = data[neigh][0]
        d = dist[valve][neigh] + 1
        add = rate * (minutes - d)
        best = max(best, add + pressure1(neigh, rest, minutes - d))
    return best


@functools.cache
def pressure2(valve, valves, minutes):
    best = 0
    for neigh in valves:
        rest = tuple(x for x in valves if x != neigh)
        if dist[valve][neigh] >= minutes:
            best = max(best, pressure1("AA", valves, 26))
            continue
        rate = data[neigh][0]
        d = dist[valve][neigh] + 1
        add = rate * (minutes - d)
        best = max(best, add + pressure2(neigh, rest, minutes - d))
    return best


print(pressure1("AA", tuple(useful), 30))
print(pressure2("AA", tuple(useful), 26))
