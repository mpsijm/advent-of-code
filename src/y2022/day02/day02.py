d = [(ord(l[0]) - 65, ord(l[2]) - 88) for l in open("data.in").readlines()]
print(sum(1 + x + 3 * ((x - a + 1) % 3) for a, x in d), sum(1 + x * 3 + (a + x + 2) % 3 for a, x in d))
