print([sum(f(*p) for p in ((ord(l[0]) - 65, ord(l[2]) - 88) for l in open("data.in").readlines()))
       for f in (lambda a, x: 1 + x + 3 * ((x - a + 1) % 3), lambda a, x: 1 + x * 3 + (a + x + 2) % 3)])
