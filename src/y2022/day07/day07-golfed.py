a, s = [], [[]]
["$ cd" in l and ((a.append(x := sum(s.pop())), s[-1].append(x)) if ".." in l else s.append([])) if "$" in l else
 l[0] != "d" and s[-1].append(int(l.split()[0]))
 for t in (open("data.in").readlines()[1:], iter(lambda: len(s) > 1 and "$ cd ..", 0)) for l in t]
print(sum(x for x in a if x < 1e5), next(x for x in sorted(a) if x > sum(s[-1]) - 4e7))
