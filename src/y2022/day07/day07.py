import bisect

data, sizes, stack = [line.strip() for line in open("data.in").readlines()], [], [[]]

for it in (data[1:], iter(lambda: len(stack) > 1 and "$ cd ..", 0)):
    for line in it:
        if "$" in line:
            if ".." in line:
                sizes.append(s := sum(stack.pop())), stack[-1].append(s)
            elif "$ cd" in line:
                stack.append([])
        elif line[0] != "d":
            stack[-1].append(int(line.split()[0]))

sizes.sort()
print(sum(x for x in sizes if x <= 1e5))
print(sizes[bisect.bisect(sizes, sum(stack[-1]) - 4e7)])
