print(__import__("functools").reduce(
    lambda acc, i: [
        m := acc[2] % 5,
        ("-" if m == 4 else "=" if m == 3 else str(m)) + acc[1],
        acc[2] // 5 + (m > 2)
    ],
    range(int(__import__("math").log(ans := sum(
        sum(
            int(-2 if c == "=" else -1 if c == "-" else c) * 5 ** i
            for i, c in enumerate(reversed(line.strip()))
        )
        for line in open("data.in").readlines()
    )) / __import__("math").log(5)) + 1),
    [0, "", ans]
)[1])
