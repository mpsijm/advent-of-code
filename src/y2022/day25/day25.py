data = [x.strip() for x in open("data.in").readlines()]

ans = 0
for line in data:
    q = 0
    for i, c in enumerate(reversed(line)):
        q += int(-2 if c == "=" else -1 if c == "-" else c) * 5 ** i
    ans += q

r = []
while ans:
    ans, m = (ans // 5, ans % 5)
    if m > 2:
        m -= 5
        ans += 1
    r.append(str(m) if m >= 0 else "-" if m == -1 else "=")
print("".join(reversed(r)))
