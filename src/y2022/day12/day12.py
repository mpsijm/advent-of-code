data = [x.strip() for x in open("data.in").readlines()]

for y, line in enumerate(data):
    for x, c in enumerate(line):
        if c == "S": Sx, Sy = x, y
        if c == "E": Ex, Ey = x, y

data = [[1 if c == "S" else 26 if c == "E" else ord(c) - ord("a") + 1 for c in line] for line in data]
h, w = len(data), len(data[0])
dist = [[0] * w for _ in range(h)]
queue = [(Ex, Ey)]
first = False
while queue:
    new_queue = []
    while queue:
        x, y = queue.pop()
        if not first and data[y][x] == 1:
            first = dist[y][x]
        if x == Sx and y == Sy:
            print(dist[y][x], first)
            new_queue.clear()
            break
        for xx, yy in ((x - 1, y), (x + 1, y), (x, y - 1), (x, y + 1)):
            if 0 <= xx < w and 0 <= yy < h and dist[yy][xx] == 0 and data[y][x] <= data[yy][xx] + 1:
                dist[yy][xx] = dist[y][x] + 1
                new_queue.append((xx, yy))
    queue = new_queue
