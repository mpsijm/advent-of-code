# Code used for my original submission. `day12.py` is optimised after-the-fact.
data = [x.strip() for x in open("data.in").readlines()]

for y, line in enumerate(data):
    for x, c in enumerate(line):
        if c == "S": Sx, Sy = x, y
        if c == "E": dx, dy = x, y

data = [[1 if c == "S" else 26 if c == "E" else ord(c) - ord("a") + 1 for c in line] for line in data]
h, w = len(data), len(data[0])
for starts in ([(Sx, Sy)], [(x, y) for y, line in enumerate(data) for x, c in enumerate(line) if c == 1]):
    res = 1e9
    for sx, sy in starts:
        dist = [[0] * w for _ in range(h)]
        queue = [(sx, sy)]
        while queue:
            new_queue = []
            while queue:
                x, y = queue.pop()
                for xx, yy in ((x - 1, y), (x + 1, y), (x, y - 1), (x, y + 1)):
                    if 0 <= xx < w and 0 <= yy < h and dist[yy][xx] == 0 and data[yy][xx] <= data[y][x] + 1:
                        dist[yy][xx] = dist[y][x] + 1
                        new_queue.append((xx, yy))
            queue = new_queue
        if dist[dy][dx] != 0:
            res = min(res, dist[dy][dx])
    print(res)
