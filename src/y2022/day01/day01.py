data = [[x for x in block.replace("\n", " ").split()] for block in open("data.in").read().strip().split("\n\n")]

elves = sorted(sum(int(x) for x in elf) for elf in data)
print(elves[-1])
print(sum(elves[-3:]))
