# Code used for my original submission. `day17.py` is optimised after-the-fact.
data = [[int(a), int(b), int(c)] for a, b, c in (line.strip().split(",") for line in open("data.in").readlines())]

n = len(data)
w = max(max(x, y, z) for x, y, z in data) + 1
grid = {z: {y: {x: False for x in range(-1, w + 1)} for y in range(-1, w + 1)} for z in range(-1, w + 1)}
for x, y, z in data:
    grid[z][y][x] = True

ans = n * 6
for z in range(w):
    for y in range(w):
        for x in range(w - 1):
            if grid[z][y][x] and grid[z][y][x + 1]:
                ans -= 2
for z in range(w):
    for y in range(w - 1):
        for x in range(w):
            if grid[z][y][x] and grid[z][y + 1][x]:
                ans -= 2
for z in range(w - 1):
    for x in range(w):
        for y in range(w):
            if grid[z][y][x] and grid[z + 1][y][x]:
                ans -= 2
print(ans)

ans = 0
stack = [(-1, -1, -1)]
seen = {(-1, -1, -1)}
while stack:
    x, y, z = stack.pop()
    for xx, yy, zz in (x - 1, y, z), (x + 1, y, z), (x, y - 1, z), (x, y + 1, z), (x, y, z - 1), (x, y, z + 1):
        if -1 <= xx <= w and -1 <= yy <= w and -1 <= zz <= w:
            if grid[zz][yy][xx]:
                ans += 1
            elif (xx, yy, zz) not in seen:
                seen.add((xx, yy, zz))
                stack.append((xx, yy, zz))
print(ans)
