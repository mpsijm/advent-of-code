import itertools

data = set((int(a), int(b), int(c)) for a, b, c in (line.strip().split(",") for line in open("data.in").readlines()))
w = max(max(pos) for pos in data) + 1

print(len(data) * 6 - sum(2 for dim in range(3) for pos in itertools.product(range(w), repeat=3)
                          if pos in data and tuple(p + (d == dim) for d, p in enumerate(pos)) in data))

ans = 0
stack = [(-1, -1, -1)]
seen = {(-1, -1, -1)}
while stack:
    x, y, z = stack.pop()
    for neigh in (x - 1, y, z), (x + 1, y, z), (x, y - 1, z), (x, y + 1, z), (x, y, z - 1), (x, y, z + 1):
        if all(-1 <= p <= w for p in neigh):
            if neigh in data:
                ans += 1
            elif neigh not in seen:
                seen.add(neigh)
                stack.append(neigh)
print(ans)
