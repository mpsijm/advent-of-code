import math

data = [[a, int(b)] for a, b in (line.strip().split() for line in open("data.in").readlines())]


def follow_knot(a, b):
    dx, dy = a[0] - b[0], a[1] - b[1]
    if abs(dx) > 1 or abs(dy) > 1:  # if too far:
        angle = math.atan2(dy, dx)
        b[0], b[1] = a[0] - round(math.cos(angle)), a[1] - round(math.sin(angle))


ropes1, ropes2, rope = {(0, 0)}, {(0, 0)}, [[0, 0] for _ in range(10)]
for d, x in data:
    for i in range(x):
        if d == "R": rope[0][0] += 1
        if d == "L": rope[0][0] -= 1
        if d == "D": rope[0][1] += 1
        if d == "U": rope[0][1] -= 1
        for j in range(9): follow_knot(*rope[j:j + 2])
        ropes1.add(tuple(rope[1]))
        ropes2.add(tuple(rope[-1]))

print(len(ropes1), len(ropes2))
