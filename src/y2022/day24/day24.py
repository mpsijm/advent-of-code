import math

data = [x.strip() for x in open("data.in").readlines()]
h = len(data)
w = len(data[0])
q = math.lcm((w - 2), (h - 2))

blizzards1 = set((x, y, c) for y, line in enumerate(data) for x, c in enumerate(line) if c in "><v^")
blizz = blizzards1
blizzards = [blizz]
for i in range(q):
    n = set()
    for x, y, c in blizz:
        xx, yy = {
            ">": (x + 1, y),
            "<": (x - 1, y),
            "^": (x, y - 1),
            "v": (x, y + 1),
        }[c]
        if xx < 1: xx += w - 2
        if yy < 1: yy += h - 2
        if xx > w - 2: xx -= w - 2
        if yy > h - 2: yy -= h - 2
        n.add((xx, yy, c))
    blizz = n
    if i == q - 1:
        assert blizz == blizzards1
    else:
        blizzards.append(blizz)
blizzards = [set((x, y) for x, y, _ in blizz) for blizz in blizzards]

x, y = 1, 0
queue = [(x, y, 0)]
seen = set()
parent = dict()
phase = 0
while queue:
    new_queue = []
    for x, y, t in queue:
        if (x, y) == (w - 2, h - 1) and phase == 0:
            print(t)
            phase += 1
            seen = set()
            new_queue = [(x, y, t)]
            break
        if (x, y) == (1, 0) and phase == 1:
            phase += 1
            seen = set()
            new_queue = [(x, y, t)]
            break
        if (x, y) == (w - 2, h - 1) and phase == 2:
            print(t)
            exit()
        for xx, yy in [(x - 1, y), (x, y), (x + 1, y), (x, y - 1), (x, y + 1)]:
            if 0 <= xx < w and 0 <= yy < h and data[yy][xx] != "#" and (xx, yy) not in blizzards[(t + 1) % q]:
                if (xx, yy, (t + 1) % q) not in seen:
                    new_queue.append((xx, yy, t + 1))
                    seen.add((xx, yy, (t + 1) % q))
                    parent[(xx, yy, t + 1)] = (x, y, t)
    queue = new_queue
