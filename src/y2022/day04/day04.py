data = [line.strip().split(",") for line in open("data.in").readlines()]

ans, ans2 = 0, 0
for f, s in data:
    a, b = map(int, f.split("-"))
    c, d = map(int, s.split("-"))
    if a <= c and d <= b or c <= a and b <= d:
        ans += 1
    if a <= c <= b or a <= d <= b or c <= a <= d or c <= b <= d:
        ans2 += 1
print(ans, ans2)
