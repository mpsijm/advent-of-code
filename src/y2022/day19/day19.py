import math


def geodes(costs, minutes):
    maxore = max(cost[0] for cost in costs)
    maxclay = costs[2][1]
    maxobs = costs[3][1]

    initial = ((0, 0, 0, 0), (1, 0, 0, 0))
    queue = [initial]
    seen = set()
    for time in range(minutes):
        new_queue = []
        for curr in queue:
            if curr in seen:
                continue
            seen.add(curr)
            (ore, clay, obs, geo), (orebot, claybot, obsbot, geobot) = curr

            if ore >= costs[3][0] and obs >= costs[3][1]:
                new_queue.append(((
                                      min(maxore * 2, ore + orebot - costs[3][0]),
                                      min(maxclay * 2, clay + claybot),
                                      min(maxobs * 2, obs + obsbot - costs[3][1]),
                                      geo + geobot
                                  ), (orebot, claybot, obsbot, geobot + 1)))
            else:
                if orebot < maxore and ore >= costs[0][0]:
                    new_queue.append(((
                                          min(maxore * 2, ore + orebot - costs[0][0]),
                                          min(maxclay * 2, clay + claybot),
                                          min(maxobs * 2, obs + obsbot),
                                          geo + geobot
                                      ), (orebot + 1, claybot, obsbot, geobot)))
                if claybot < maxclay and ore >= costs[1][0]:
                    new_queue.append(((
                                          min(maxore * 2, ore + orebot - costs[1][0]),
                                          min(maxclay * 2, clay + claybot),
                                          min(maxobs * 2, obs + obsbot),
                                          geo + geobot
                                      ), (orebot, claybot + 1, obsbot, geobot)))
                if obsbot < maxobs and ore >= costs[2][0] and clay >= costs[2][1]:
                    new_queue.append(((
                                          min(maxore * 2, ore + orebot - costs[2][0]),
                                          min(maxclay * 2, clay + claybot - costs[2][1]),
                                          min(maxobs * 2, obs + obsbot),
                                          geo + geobot
                                      ), (orebot, claybot, obsbot + 1, geobot)))

                new_queue.append((
                    (
                        min(maxore * 2, ore + orebot),
                        min(maxclay * 2, clay + claybot),
                        min(maxobs * 2, obs + obsbot),
                        geo + geobot
                    ), (orebot, claybot, obsbot, geobot)))
        queue = new_queue

    return max(geo for (_, _, _, geo), _ in queue)


data = [tuple(tuple(int(x) for x in sentence.split() if x.isdigit()) for sentence in line.strip().split(".")[:4])
        for line in open("data.in").readlines()]

print(sum(i * geodes(costs, 24) for i, costs in enumerate(data, start=1)))
print(math.prod(geodes(costs, 32) for costs in data[:3]))
