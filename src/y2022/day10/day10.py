data = [x.strip() for x in open("data.in").readlines()]

ans = 0
val = 1
vals = []
for line in data:
    if line == "noop":
        vals.append(val)
    else:
        vals.append(val)
        vals.append(val)
        val += int(line[5:])
for cycle, val in enumerate(vals, start=1):
    if (cycle - 20) % 40 == 0:
        ans += val * cycle
print(ans)

sprite = ["."] * 240
for cycle, val in enumerate(vals):
    if val - 1 <= cycle % 40 <= val + 1:
        sprite[cycle] = "#"

for i in range(0, 240, 40):
    print("".join(sprite[i:i+40]))
