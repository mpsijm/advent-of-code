data = [[tuple(map(int, c.split(","))) for c in x.strip().split(" -> ")] for x in open("data.in").readlines()]

maxx = max(x for line in data for x, y in line)
maxy = max(y for line in data for x, y in line)

grid = [[" "] * (maxx * 2) for _ in range(maxy + 2)]
for line in data:
    for (xa, ya), (xb, yb) in zip(line, line[1:]):
        if xa == xb:
            for y in range(min(ya, yb), max(ya, yb) + 1):
                grid[y][xa] = "#"
        else:
            for x in range(min(xa, xb), max(xa, xb) + 1):
                grid[ya][x] = "#"

sands = 0
first = False
stack = [(500, 0)]
while stack:
    x, y = stack[-1]
    while True:
        if y == maxy + 1:
            if not first:
                print(sands)
                first = True
            break
        elif grid[y + 1][x] == " ":
            y += 1
        elif grid[y + 1][x - 1] == " ":
            x -= 1
            y += 1
        elif grid[y + 1][x + 1] == " ":
            x += 1
            y += 1
        else:
            break
        stack.append((x, y))
    stack.pop()
    grid[y][x] = "o"
    sands += 1
    # print(*("".join("~" if (x, y) in stack else c for x, c in enumerate(line[460:540], start=460))
    #         for y, line in enumerate(grid)), sep="\n")

print(sands)
