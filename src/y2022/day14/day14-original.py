# Code used for my original submission. `day14.py` is optimised after-the-fact.
data = [[tuple(map(int, c.split(","))) for c in x.strip().split(" -> ")] for x in open("data.in").readlines()]

maxy = max(y for line in data for x, y in line)

grid = dict()
for line in data:
    i = 1
    while i < len(line):
        if line[i - 1][0] == line[i][0]:
            ya = line[i - 1][1]
            yb = line[i][1]
            for y in range(min(ya, yb), max(ya, yb) + 1):
                grid[(line[i][0], y)] = "#"
        else:
            xa = line[i - 1][0]
            xb = line[i][0]
            for x in range(min(xa, xb), max(xa, xb) + 1):
                grid[(x, line[i][1])] = "#"
        i += 1

sands = 0
first = False
while True:
    x, y = 500, 0
    if (x, y) in grid:
        break
    while True:
        if y == maxy + 1:
            if not first:
                print(sands)
                first = True
            break
        elif (x, y + 1) not in grid:
            y += 1
        elif (x - 1, y + 1) not in grid:
            x -= 1
            y += 1
        elif (x + 1, y + 1) not in grid:
            x += 1
            y += 1
        else:
            break
    grid[(x, y)] = "o"
    sands += 1
print(sands)
