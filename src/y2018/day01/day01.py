with open("data.in") as f:
    data = [int(x) for x in f.readlines()]
print(sum(data))

seen = set()
seen.add(0)
last = 0
while True:
    for x in data:
        last += x
        if last in seen:
            print(last)
            exit(0)
        seen.add(last)
