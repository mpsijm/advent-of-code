with open("data.in") as f:
    data = [x.strip() for x in f.readlines()]

claims = [(i, (tuple(map(int, pos.split(","))), tuple(map(int, size.split("x")))))
          for i, (pos, size) in ((i, rest.split(": ")) for i, rest in (claim.split(" @ ") for claim in data))]
grid = [[0] * 1000 for _ in range(1000)]

for id, (pos, size) in claims:
    for x in range(pos[0], pos[0] + size[0]):
        for y in range(pos[1], pos[1] + size[1]):
            grid[y][x] += 1
print(sum(v > 1 for line in grid for v in line))

for id, (pos, size) in claims:
    if all(grid[y][x] == 1
           for x in range(pos[0], pos[0] + size[0])
           for y in range(pos[1], pos[1] + size[1])):
        print(id)
        exit()
