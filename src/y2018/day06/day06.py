data = [x.strip() for x in open("datb.in").readlines()]

data = [tuple(map(int, line.split(","))) for line in data]
w, h = [max(x) for x in zip(*data)]
grid = [[-2 for _ in range(w + 1)] for _ in range(h + 1)]

for y in range(h + 1):
    for x in range(w + 1):
        dists = sorted((abs(x - xx) + abs(y - yy), i) for i, (xx, yy) in enumerate(data))
        if dists[0][0] == dists[1][0]:
            grid[y][x] = -1
        else:
            grid[y][x] = dists[0][1]

edge = {*grid[0], *grid[-1], *(grid[y][0] for y in range(h)), *(grid[y][-1] for y in range(h))}
print(max(sum(c == i for line in grid for c in line) for i in range(len(data)) if i not in edge))


# Sneaky trick: my ./run script automatically replaces "datb.in" with "data.in", so use that to switch
is_example = "b" in """open("datb.in")"""

D = 32 if is_example else 10000

for y in range(h + 1):
    for x in range(w + 1):
        dists = sum((abs(x - xx) + abs(y - yy)) for _, (xx, yy) in enumerate(data))
        if dists < D:
            grid[y][x] = 1
        else:
            grid[y][x] = 0
print(sum(sum(line) for line in grid))
