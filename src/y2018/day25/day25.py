from collections import defaultdict

data = [[int(x) for x in line.strip().split(",")] for line in open("data.in").readlines()]
n = len(data)

edges = defaultdict(list)
for i, p in enumerate(data):
    for j, q in enumerate(data[:i]):
        if sum(abs(a - b) for a, b in zip(p, q)) <= 3:
            edges[i].append(j)
            edges[j].append(i)
# print(edges)
todo = set(range(n))
ans = 0
while todo:
    ans += 1
    curr = next(iter(todo))
    todo.remove(curr)
    stack = [curr]
    while stack:
        curr = stack.pop()
        if curr in todo:
            todo.remove(curr)
        for n in edges[curr]:
            if n in todo:
                stack.append(n)
print(ans)
