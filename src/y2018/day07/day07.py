import heapq
from collections import defaultdict

# Sneaky trick: my ./run script automatically replaces "datb.in" with "data.in", so use that to switch
is_example = "b" in """open("datb.in")"""

data = [line.strip().split() for line in open("datb.in").readlines()]
n = len(data)

deps = defaultdict(set)
keys = set()
for line in data:
    deps[line[7]].add(line[1])
    keys.add(line[7])
    keys.add(line[1])

max_workers = 2 if is_example else 5
workers = []
curr = 0
while keys:
    opts = sorted(
        a
        for a in list(keys)
        if not deps[a]
    )
    for a in opts:
        if len(workers) == max_workers:
            break
        print(a, end="")
        heapq.heappush(workers, (curr + (not is_example) * 60 + ord(a) - ord("A") + 1, a))
        print(workers)
        keys.remove(a)
    curr, job = heapq.heappop(workers)
    if job:
        for c in deps:
            if job in deps[c]:
                deps[c].remove(job)
print()
print(curr)

