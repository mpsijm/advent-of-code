from collections import Counter

with open("data.in") as f:
    data = [x.strip() for x in f.readlines()]

print(sum(2 in dict(Counter(line)).values() for line in data) *
      sum(3 in dict(Counter(line)).values() for line in data))

print(next("".join(c1 for c1, c2 in zip(line1, line2) if c1 == c2)
           for line1 in data for line2 in data if sum(c1 != c2 for c1, c2 in zip(line1, line2)) == 1))

two = sum(2 in dict(Counter(line)).values() for line in data)
three = sum(3 in dict(Counter(line)).values() for line in data)
print(two * three)

for line1 in data:
    for line2 in data:
        if sum(c1 != c2 for c1, c2 in zip(line1, line2)) == 1:
            print("".join(c1 for c1, c2 in zip(line1, line2) if c1 == c2))
            exit()
