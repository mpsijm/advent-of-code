data = [[int(x) for x in line.strip().split()] for line in open("datb.in").readlines()][0]
n = len(data)


def parse(start=0):
    childs, meta, *_ = data[start:]
    ans, curr = 0, start + 2
    for _ in range(childs):
        curr, s = parse(curr)
        ans += s

    return curr + meta, sum(data[curr:curr + meta]) + ans


print(parse()[1])


def parse2(start=0):
    childs, meta, *_ = data[start:]
    ans, curr = 0, start + 2
    if childs == 0:
        return curr + meta, sum(data[curr:curr + meta])
    vals = []
    for _ in range(childs):
        curr, s = parse2(curr)
        vals.append(s)

    return curr + meta, sum(c != 0 and c <= len(vals) and vals[c - 1] for c in data[curr:curr + meta])


print(parse2()[1])
