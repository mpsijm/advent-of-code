data = [x.strip() for x in open("datf.in").readlines()][0]

m = {(chr(i), chr(i + 32)) for i in range(65, 65 + 26)} | {(chr(i + 32), chr(i)) for i in range(65, 65 + 26)}


def run(data):
    stack = []
    for c in data:
        if stack and (stack[-1], c) in m:
            stack.pop()
        else:
            stack.append(c)
    return stack


print(len(run(data)))
print(min(len(run([c for c in data if c not in (chr(65 + i), chr(97 + i))])) for i in range(26)))

