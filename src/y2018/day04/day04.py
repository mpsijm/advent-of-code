from datetime import datetime

data = sorted([line.strip() for line in open("data.in").readlines()])

guards = {}
guard = -1
last_t = -1
for line in data:
    date = line.split("]")[0][1:]
    t = datetime.fromisoformat(date)
    if "#" in line:
        guard = int(line.split("#")[1].split()[0])
    if "asleep" in line:
        last_t = t
    if "wakes" in line:
        if guard not in guards:
            guards[guard] = [0] * 60
        for i in range(last_t.minute, t.minute):
            guards[guard][i] += 1

best = max((sum(v), max((j, i) for i, j in enumerate(v)), k) for k, v in guards.items())
print(best[1][1] * best[2])
best = max((j, i, k) for k, v in guards.items() for i, j in enumerate(v))
print(best[1] * best[2])
