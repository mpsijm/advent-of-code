data = [[x for x in block.replace("\n", " ").split()] for block in open("data.in").read().strip().split("\n\n")]
n = len(data)

# Sneaky trick: my ./run script automatically replaces "datb.in" with "data.in", so use that to switch
is_example = "a." not in """open("datb.in")"""

grid = [list(line) for line in data[0]]
new_grid = []
for line in grid:
    l = []
    for c in line:
        match c:
            case "#":
                l.append("#")
                l.append("#")
            case "O":
                l.append("[")
                l.append("]")
            case ".":
                l.append(".")
                l.append(".")
            case "@":
                l.append("@")
                l.append(".")
    new_grid.append(l)
grid = new_grid

moves = "".join(data[1])
x, y = next((x, y) for y, line in enumerate(grid) for x, c in enumerate(line) if c == "@")


def attempt(x, y, dx, dy):
    xx, yy = x + dx, y + dy
    c = grid[yy][xx]
    if c == "#":
        return []
    res = [((x, y), (xx, yy))]
    if c == ".":
        return res
    if a := attempt(xx, yy, dx, dy):
        b = []
        if dy and c in "[]":
            b = (attempt(xx - 1 if c == "]" else xx + 1, yy, dx, dy))
            if not b:
                return []
        return a + b + res


for move in moves:
    dx, dy = {"^": (0, -1), "v": (0, 1), "<": (-1, 0), ">": (1, 0)}[move]
    xx, yy = x + dx * 1, y + dy * 1
    if grid[yy][xx] != "#":
        if l := attempt(x, y, dx, dy):
            for (c, d), (a, b) in dict.fromkeys(l):  # Note that `set` preserves order in PyPy, but not in CPython
                grid[d][c], grid[b][a] = grid[b][a], grid[d][c]
            x, y = xx, yy
    # if "b." in """open("datb.in")""":
    #     print(move)
    #     for line in grid:
    #         print("".join(line))

if is_example:
    for line in grid:
        print("".join(line))

ans = 0
for y, line in enumerate(grid):
    for x, c in enumerate(line):
        if c == "[":
            ans += 100 * y + x
print(ans)
