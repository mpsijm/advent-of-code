data = [[x for x in block.replace("\n", " ").split()] for block in open("data.in").read().strip().split("\n\n")]
n = len(data)

grid = [list(line) for line in data[0]]
moves = "".join(data[1])
x, y = next((x, y) for y, line in enumerate(grid) for x, c in enumerate(line) if c == "@")


def attempt(x, y, dx, dy):
    xx, yy = x + dx, y + dy
    if grid[yy][xx] == "#":
        return False
    if grid[yy][xx] == "." or attempt(xx, yy, dx, dy):
        grid[yy][xx], grid[y][x] = grid[y][x], grid[yy][xx]
        return True


for move in moves:
    dx, dy = {"^": (0, -1), "v": (0, 1), "<": (-1, 0), ">": (1, 0)}[move]
    if attempt(x, y, dx, dy):
        xx, yy = x + dx, y + dy
        x, y = xx, yy

ans = 0
for y, line in enumerate(grid):
    for x, c in enumerate(line):
        if c == "O":
            ans += 100 * y + x
print(ans)
