import re
from math import prod

data = open("data.in").read()

print(sum(
    prod(int(x) for x in re.match(r"mul\((\d+),(\d+)\)", instruction).groups())
    for instruction in re.findall(r"mul\(\d+,\d+\)", data)
))

(enabled := True), print(sum(
    (enabled := True) - 1 if instruction == "do()" else
    (enabled := False) if instruction == "don't()" else
    enabled * prod(int(x) for x in re.match(r"mul\((\d+),(\d+)\)", instruction).groups())
    for instruction in re.findall(r"mul\(\d+,\d+\)|do\(\)|don't\(\)", data)
))
