import re
from math import prod

data = open("data.in").read().replace("\n", " ")

print(sum(
    prod(int(x) for x in re.match(r"mul\((\d+),(\d+)\)", instruction).groups())
    for instruction in re.findall(r"mul\(\d+,\d+\)", data)
))

ans = 0
enabled = True
for instruction in re.findall(r"mul\(\d+,\d+\)|do\(\)|don't\(\)", data):
    if instruction == "do()":
        enabled = True
        continue
    if instruction == "don't()":
        enabled = False
        continue
    if enabled:
        ans += prod(int(x) for x in re.match(r"mul\((\d+),(\d+)\)", instruction).groups())
print(ans)
