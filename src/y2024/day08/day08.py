import string

data = [line.strip() for line in open("data.in").readlines()]
h, w = len(data), len(data[0])

locs = set()
for freq in string.ascii_letters + string.digits:
    if freq not in "".join(data):
        continue
    nodes = [(x, y) for y, line in enumerate(data) for x, c in enumerate(line) if c == freq]
    for node in nodes:
        other_nodes = [n for n in nodes if node != n]
        for n in other_nodes:
            x, y = node
            dy, dx = n[1] - node[1], n[0] - node[0]
            xx, yy = x - dx, y - dy
            if 0 <= xx < w and 0 <= yy < h:
                locs.add((xx, yy))
print(len(locs))

locs = set()
for freq in string.ascii_letters + string.digits:
    if freq not in "".join(data):
        continue
    nodes = [(x, y) for y, line in enumerate(data) for x, c in enumerate(line) if c == freq]
    for node in nodes:
        other_nodes = [n for n in nodes if node != n]
        for n in other_nodes:
            x, y = node
            dy, dx = n[1] - node[1], n[0] - node[0]
            xx, yy = x, y
            i = 0
            while 0 <= xx < w and 0 <= yy < h:
                locs.add((xx, yy))
                i += 1
                xx, yy = x - dx * i, y - dy * i
print(len(locs))
