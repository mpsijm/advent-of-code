from collections import defaultdict

data = [[x for x in block.replace("\n", "\n").split("\n")] for block in open("data.in").read().strip().split("\n\n")]

init_values = dict()
for line in data[0]:
    name, v = line.split(": ")
    init_values[name.replace(":", "")] = int(v)

graph = defaultdict(list)
deps = dict()
for line in data[1]:
    a, op, b, _, c = line.split()
    graph[c] = [op, a, b]
    assert c not in deps
    deps[c] = set()
    if a not in init_values:
        deps[c].add(a)
    if b not in init_values:
        deps[c].add(b)


def calc(a, b, graph, deps):
    values = dict(init_values)
    for k in values:
        values[k] = 0
    for i, x in enumerate(reversed(bin(a)[2:])):
        values[f"x{i:02}"] = int(x)
    for i, x in enumerate(reversed(bin(b)[2:])):
        values[f"y{i:02}"] = int(x)
    deps = {k: set(v) for k, v in deps.items()}
    while deps:
        for d in list(deps):
            if deps[d]: continue
            op, a, b = graph[d]
            if op == "AND":
                values[d] = values[a] & values[b]
            if op == "OR":
                values[d] = values[a] | values[b]
            if op == "XOR":
                values[d] = values[a] ^ values[b]
            for dd in deps:
                if d in deps[dd]:
                    deps[dd].remove(d)
                if d in deps[dd]:
                    deps[dd].remove(d)
            del deps[d]
    ans = []
    for i in range(100):
        z = f"z{i:02}"
        if z not in values: break
        ans.append(values[z])
    return int("".join(str(x) for x in reversed(ans)), 2)


# Sneaky trick: my ./run script automatically replaces "datb.in" with "data.in", so use that to switch
is_example = "d" in """open("datd.in")"""
x, y = (5, 5) if is_example else (0b111111111_111111111_111111111_111111111_111111111, 0b111111111_111111111_111111111_111111111_111111111)


keys = sorted(list(graph))
for i, a in enumerate(keys):
    for j, b in enumerate(keys[:i]):
        for k, c in enumerate(keys[:j]):
            for l, d in enumerate(keys[:k]):
                for m, e in enumerate(keys):
                    for n, f in enumerate(keys[:i]):
                        for o, g in enumerate(keys[:j]):
                            for p, h in enumerate(keys[:k]):
                                graph[a], graph[b] = graph[b], graph[a]
                                graph[c], graph[d] = graph[d], graph[c]
                                graph[e], graph[f] = graph[f], graph[e]
                                graph[g], graph[h] = graph[h], graph[g]
                                deps[a], deps[b] = deps[b], deps[a]
                                deps[c], deps[d] = deps[d], deps[c]
                                deps[e], deps[f] = deps[f], deps[e]
                                deps[g], deps[h] = deps[h], deps[g]
                                ans = calc(x, y, graph, deps)
                                if is_example:
                                    if ans == 5:
                                        print(a, b, c, d)
                                        exit()
                                else:
                                    if ans == x + y:
                                        print(",".join((a, b, c, d, e, f, g, h)))
                                        exit()
                                deps[a], deps[b] = deps[b], deps[a]
                                deps[c], deps[d] = deps[d], deps[c]
                                deps[e], deps[f] = deps[f], deps[e]
                                deps[g], deps[h] = deps[h], deps[g]
                                graph[g], graph[h] = graph[h], graph[g]
                                graph[e], graph[f] = graph[f], graph[e]
                                graph[c], graph[d] = graph[d], graph[c]
                                graph[a], graph[b] = graph[b], graph[a]
# print("".join(str(x) for x in reversed(ans)))
# print(int("".join(str(x) for x in reversed(ans)), 2))
