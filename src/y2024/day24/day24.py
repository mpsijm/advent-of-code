from collections import defaultdict
from subprocess import run

data = [block.split("\n") for block in open("data.in").read().strip().split("\n\n")]

# Sneaky trick: my ./run script automatically replaces "datb.in" with "data.in", so use that to switch
is_example = "a." not in """open("data.in")"""
letter = """open("data.in")"""[9]

init_values = dict()
for line in data[0]:
    name, v = line.split(": ")
    init_values[name] = int(v)

graph = defaultdict(list)
deps = dict()
for line in data[1]:
    a, op, b, _, c = line.split()
    graph[c] = [op, a, b]
    assert c not in deps  # Ass(ert|ump)tion: every output appears at most once
    deps[c] = set()
    if a not in init_values:
        deps[c].add(a)
    if b not in init_values:
        deps[c].add(b)


def calc(graph, deps, inputs=None):
    values = dict(init_values)  # Copy, because we want to reuse the init_values
    if isinstance(inputs, tuple):
        a, b = inputs
        for k in values:
            values[k] = 0
        for i, x in enumerate(reversed(bin(a)[2:])):
            values[f"x{i:02}"] = int(x)
        for i, x in enumerate(reversed(bin(b)[2:])):
            values[f"y{i:02}"] = int(x)
    deps = {k: set(v) for k, v in deps.items()}  # Deep copy, because we want to reuse the initial deps
    while deps:
        for d in list(deps):
            if deps[d]: continue
            op, a, b = graph[d]
            if op == "AND":
                values[d] = values[a] & values[b]
            if op == "OR":
                values[d] = values[a] | values[b]
            if op == "XOR":
                values[d] = values[a] ^ values[b]
            for dd in deps:
                if d in deps[dd]:
                    deps[dd].remove(d)
                if d in deps[dd]:
                    deps[dd].remove(d)
            del deps[d]
    ans = []
    for i in range(100):
        z = f"z{i:02}"
        if z not in values: break
        ans.append(values[z])
    return int("".join(str(x) for x in reversed(ans)), 2)


print(calc(graph, deps))

swappers = (("z00", "z01"), ("z02", "z05")) if is_example else (
    ("mwk", "z10"), ("qgd", "z18"), ("gqp", "z33"), ("jmh", "hsw"))

for a, b in swappers:
    graph[a], graph[b] = graph[b], graph[a]
    deps[a], deps[b] = deps[b], deps[a]


def check():
    keys = sorted(list(graph))
    for k in keys:
        if not k.startswith("z") or k == keys[-1]: continue
        assert graph[k][0] == "XOR", (k, graph[k])
        a, b = graph[k][1:]
        if sorted((a, b)) == ["x00", "y00"]: continue
        if k == "z01": continue
        assert graph[a], a
        assert graph[b], b
        assert sorted((graph[a][0], graph[b][0])) == ["OR", "XOR"], (k, graph[k])


if not is_example:
    check()


def name(x):
    if x in graph:
        return f"{graph[x][0]}_{x}"
    else:
        return x


# This is not "cheating", this is "using the tools that you can work with" 😇
res = run(
    ["dot", "-Tpdf", f"-ograph{letter}.pdf"],
    input=f"""
        digraph {{
            {';'.join(f'{name(t)} -> {name(s)}' for s in sorted(graph) for t in graph[s][1:])};
            {'' if is_example else ''.join(f'''
                subgraph {{
                    rank = same;
                    edge[style=invis];
                    rankdir = LR;
                    x{x:02} -> y{x:02} -> XOR_z{x - 1:02};
                }}
            ''' for x in range(1, 45))}
        }}
    """,
    encoding="ascii")
# print(res.stdout, res.stderr)

x, y = (5, 5) if is_example else tuple(int("1" * 45, 2) for _ in range(2))
ans = calc(graph, deps, (x, y))
if (ans == x & y) if is_example else (ans == x + y):
    print(",".join(sorted(a for pair in swappers for a in pair)))
else:
    print("Awww :(")
