from collections import defaultdict

data = [[x for x in block.replace("\n", "\n").split("\n")] for block in open("data.in").read().strip().split("\n\n")]
n = len(data)

values = dict()
for line in data[0]:
    name, v = line.split(": ")
    values[name.replace(":", "")] = int(v)

graph = defaultdict(list)
deps = dict()
for line in data[1]:
    a, op, b, _, c = line.split()
    graph[c] = [op, a, b]
    assert c not in deps
    deps[c] = set()
    if a not in values:
        deps[c].add(a)
    if b not in values:
        deps[c].add(b)

while deps:
    for d in list(deps):
        if deps[d]: continue
        op, a, b = graph[d]
        if op == "AND":
            values[d] = values[a] & values[b]
        if op == "OR":
            values[d] = values[a] | values[b]
        if op == "XOR":
            values[d] = values[a] ^ values[b]
        for dd in deps:
            if d in deps[dd]:
                deps[dd].remove(d)
            if d in deps[dd]:
                deps[dd].remove(d)
        del deps[d]

ans = []
for i in range(100):
    z = f"z{i:02}"
    if z not in values: break
    ans.append(values[z])
print("".join(str(x) for x in reversed(ans)))
print(int("".join(str(x) for x in reversed(ans)), 2))
