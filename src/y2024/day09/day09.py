data = [line.strip() for line in open("data.in").readlines()][0]
w = len(data)

for part in (1, 2):
    fs = [-1] * (w * 10)
    id = 0
    empty = False
    curr = 0
    if part == 2:
        empties = []
        files = []
    for d in data:
        c = int(d)
        if c > 0:
            if part == 2:
                if empty:
                    empties.append((c, curr))
                else:
                    files.append((c, curr))
            for _ in range(c):
                fs[curr] = id if not empty else -1
                curr += 1
        empty = not empty
        if not empty:
            id += 1

    if part == 1:
        curr2 = 0
        while curr >= curr2:
            while fs[curr2] != -1:
                curr2 += 1
            if curr2 > curr: break
            fs[curr], fs[curr2] = fs[curr2], fs[curr]
            curr -= 1

    if part == 2:
        for size, pos in reversed(files):
            try:
                i, (empty_size, empty_pos) = next((i, (s, p)) for i, (s, p) in enumerate(empties) if s >= size)
            except:
                continue
            if empty_pos > pos: continue
            if empty_size == size:
                empties.remove((empty_size, empty_pos))
            else:
                empties[i] = (empty_size - size, empty_pos + size)
            for _ in range(size):
                fs[empty_pos], fs[pos] = fs[pos], fs[empty_pos]
                pos += 1
                empty_pos += 1

    ans = 0
    for i, id in enumerate(fs):
        if id == -1: continue
        ans += i * id
    print(ans)
