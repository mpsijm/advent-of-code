import heapq
from collections import defaultdict

data = [line.strip() for line in open("data.in").readlines()]
h, w = len(data), len(data[0])

x, y = next((x, y) for y, line in enumerate(data) for x, c in enumerate(line) if c == "S")
end = next((x, y) for y, line in enumerate(data) for x, c in enumerate(line) if c == "E")
q = [(0, x, y, 0, None)]
DIRS = [(1, 0), (0, 1), (-1, 0), (0, -1)]
seen = set()
prevs = defaultdict(list)
score = defaultdict(int)
final = set()


def count(x, y, d):
    final.add((x, y))
    for n in prevs[(x, y, d)]:
        xx, yy, dd = n
        if d != dd and score[(xx, yy, dd)] + 1000 == score[(x, y, d)]:
            count(*n)
        if d == dd and score[(xx, yy, dd)] + 1 == score[(x, y, d)]:
            count(*n)


while q:
    c, x, y, d, prev = heapq.heappop(q)
    if prev:
        prevs[(x, y, d)].append(prev)
    if (x, y) == end:
        score[(x, y, d)] = c
        count(x, y, d)
        print(c)
        print(len(final))
        break
    if (x, y, d) in seen:
        if score[(x, y, d)] == c:
            prevs[(x, y, d)].append(prev)
        continue
    score[(x, y, d)] = c
    seen.add((x, y, d))
    dx, dy = [(1, 0), (0, 1), (-1, 0), (0, -1)][d]
    xx, yy = x + dx * 1, y + dy * 1
    n2 = (xx, yy, d)
    if n2 not in seen and data[yy][xx] != "#":
        heapq.heappush(q, (c + 1, *n2, (x, y, d)))
    for n in [(x, y, dd) for dd in ((d - 1) % 4, (d + 1) % 4)]:
        if n not in seen:
            heapq.heappush(q, (c + 1000, *n, (x, y, d)))
