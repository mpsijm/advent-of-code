import heapq

data = [line.strip() for line in open("data.in").readlines()]
h, w = len(data), len(data[0])

x, y = next((x, y) for y, line in enumerate(data) for x, c in enumerate(line) if c == "S")
end = next((x, y) for y, line in enumerate(data) for x, c in enumerate(line) if c == "E")
q = [(0, x, y, 0)]
DIRS = [(1, 0), (0, 1), (-1, 0), (0, -1)]
seen = set()
while q:
    c, x, y, d = heapq.heappop(q)
    if (x, y) == end:
        print(c)
        break
    if (x, y, d) in seen:
        continue
    seen.add((x, y, d))
    dx, dy = [(1, 0), (0, 1), (-1, 0), (0, -1)][d]
    xx, yy = x + dx * 1, y + dy * 1
    n2 = (xx, yy, d)
    if n2 not in seen and data[yy][xx] != "#":
        heapq.heappush(q, (c + 1, *n2))
    for n in [(x, y, d) for d in ((d - 1) % 4, (d + 1) % 4)]:
        if n not in seen:
            heapq.heappush(q, (c + 1000, *n))

