data = [[x for x in block.replace("\n", " ").split()] for block in open("data.in").read().strip().split("\n\n")]

for part in (1, 2):
    ans = 0
    for m in data:
        # My automatic template generator just simply smashed every machine in one big space-separated line.
        # Oh well, we'll deal with it 😛
        a = int(m[2][2:-1])  # dx_a
        b = int(m[3][2:])  # dy_a
        c = int(m[6][2:-1])  # dx_b
        d = int(m[7][2:])  # dy_b
        e = int(m[9][2:-1]) + (part == 2) * 10000000000000
        f = int(m[10][2:]) + (part == 2) * 10000000000000
        # With a little help from my good friend, Wolfram Alpha:
        # https://www.wolframalpha.com/input?i=system+equation+calculator&assumption=%7B%22F%22%2C+%22SolveSystemOf2EquationsCalculator%22%2C+%22equation1%22%7D+-%3E%22a*x+%2B+c*y+%3D+e%22&assumption=%22FSelect%22+-%3E+%7B%7B%22SolveSystemOf2EquationsCalculator%22%7D%2C+%22dflt%22%7D&assumption=%7B%22F%22%2C+%22SolveSystemOf2EquationsCalculator%22%2C+%22equation2%22%7D+-%3E%22b*x+%2B+d*y+%3D+f%22
        x = (c * f - e * d) / (b * c - a * d)  # Number of presses on button A
        y = (e * b - a * f) / (b * c - a * d)  # Number of presses on button B
        if x == int(x) and y == int(y):
            ans += int(x) * 3 + int(y)
    print(ans)
