data = [[int(x[2:]) for line in block.split("\n") for x in line.split(": ")[1].split(", ")]
        for block in open("data.in").read().strip().split("\n\n")]

for part in (1, 2):
    ans = 0
    for a, b, c, d, e, f in data:
        if part == 2:
            (e, f) = (x + 10000000000000 for x in (e, f))
        # With a little help from my good friend, Wolfram Alpha:
        # https://www.wolframalpha.com/input?i=system+equation+calculator&assumption=%7B%22F%22%2C+%22SolveSystemOf2EquationsCalculator%22%2C+%22equation1%22%7D+-%3E%22a*x+%2B+c*y+%3D+e%22&assumption=%22FSelect%22+-%3E+%7B%7B%22SolveSystemOf2EquationsCalculator%22%7D%2C+%22dflt%22%7D&assumption=%7B%22F%22%2C+%22SolveSystemOf2EquationsCalculator%22%2C+%22equation2%22%7D+-%3E%22b*x+%2B+d*y+%3D+f%22
        x = (c * f - e * d) / (b * c - a * d)  # Number of presses on button A
        y = (e * b - a * f) / (b * c - a * d)  # Number of presses on button B
        if x == int(x) and y == int(y):
            ans += int(x) * 3 + int(y)
    print(ans)
