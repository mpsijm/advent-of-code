from collections import defaultdict

import networkx as nx

data = [line.strip() for line in open("data.in").readlines()]

pcs = set()
graph = defaultdict(set)
for line in data:
    a, b = line.split("-")
    pcs.add(a)
    pcs.add(b)
    graph[a].add(b)
    graph[b].add(a)

sets = set()
for a in pcs:
    if not a.startswith("t"):
        continue
    for b in graph[a]:
        for c in graph[a]:
            if b == c: continue
            if b in graph[c]:
                sets.add(tuple(sorted((a, b, c))))
print(len(sets))

# When reading part 2, I figured: "Python can already do this for me, right...?"
# And then I spent a few minutes installing `networkx` in my venv 😂
print(",".join(max(nx.algorithms.clique.find_cliques(nx.Graph(graph)), key=len)))
