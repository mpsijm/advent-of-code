from collections import defaultdict

import networkx as nx

data = [line.strip().split("-") for line in open("data.in").readlines()]

graph = defaultdict(set)
for a, b in data:
    graph[a].add(b)

# This is slightly slower than day23-original.py, because it needs to enumerate all cliques (also the larger ones).
# But the code is a lot shorter 😇
print(sum(any(pc.startswith("t") for pc in clique)
          for clique in nx.algorithms.clique.enumerate_all_cliques(nx.Graph(graph)) if len(clique) == 3))
print(",".join(sorted(max(nx.algorithms.clique.find_cliques(nx.Graph(graph)), key=len))))
