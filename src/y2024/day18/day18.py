data = [[int(a), int(b)] for a, b in (line.strip().split(",") for line in open("data.in").readlines())]
n = len(data)

# Sneaky trick: my ./run script automatically replaces "datb.in" with "data.in", so use that to switch
is_example = "b" in """open("datb.in")"""

end = 6 if is_example else 70
grid = [[False] * (end + 1) for _ in range(end + 1)]
first = (12 if is_example else 1024) - 1
for x, y in data[:first]:
    grid[y][x] = True
# print("\n".join("".join("#" if grid[y][x] else "." for x in range(end + 1)) for y, line in enumerate(grid)))
for b in range(first, n):
    x, y = data[b]
    grid[y][x] = True
    x, y = 0, 0
    q = [(x, y)]
    seen = set()
    i = 0
    while q:
        nq = []
        for curr in q:
            seen.add(curr)
            x, y = curr
            for d in range(4):
                dx, dy = [(1, 0), (0, 1), (-1, 0), (0, -1)][d]
                xx, yy = x + dx, y + dy
                if 0 <= xx <= end and 0 <= yy <= end and not grid[yy][xx] and (xx, yy) not in seen:
                    nq.append((xx, yy))
                    seen.add((xx, yy))
        q = nq
        i += 1
        if (end, end) in q:
            if b == first:
                print(i)
            break
    else:
        # print(b)
        print(",".join(str(x) for x in data[b]))
        break
