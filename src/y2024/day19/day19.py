import functools


@functools.cache
def count(pat):
    return pat and sum(1 if pat == t else count(pat[len(t):]) if pat.startswith(t) else 0 for t in towels)


towels, patterns = [[x.replace(",", "") for x in block.replace("\n", " ").split()]
                    for block in open("data.in").read().strip().split("\n\n")]

counts = [count(pattern) for pattern in patterns]
print(sum(c > 0 for c in counts))
print(sum(counts))
