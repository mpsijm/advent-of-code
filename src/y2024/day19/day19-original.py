import functools


@functools.cache
def possible(pattern):
    if not pattern:
        return False
    for t in towels:
        if pattern == t:
            return True
        if len(t) > len(pattern):
            continue
        if pattern.startswith(t) and possible(pattern[len(t):]):
            return True
    return False


@functools.cache
def count(pattern):
    if not pattern:
        return 0
    c = 0
    for t in towels:
        if pattern == t:
            c += 1
            continue
        if len(t) > len(pattern):
            continue
        if pattern.startswith(t):
            c += count(pattern[len(t):])
    return c


data = [[x for x in block.replace("\n", " ").split()] for block in open("data.in").read().strip().split("\n\n")]

towels = [x.replace(",", "") for x in data[0]]

ans = 0
total = 0
for pattern in data[1]:
    if possible(pattern):
        ans += 1
        total += count(pattern)
print(ans)
print(total)
