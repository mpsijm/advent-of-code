data = [line.strip() for line in open("data.in").readlines()]
h, w = len(data), len(data[0])

numpad = "789\n456\n123\n#0A".split("\n")
dirpad = "#^A\n<v>".split("\n")
dirbots = 2  # TODO: 25
start = (*((2, 0) for _ in range(dirbots)), (2, 3), "")
dirs = {"^": (0, -1), "v": (0, 1), "<": (-1, 0), ">": (1, 0)}

ans = 0
for code in data:
    q = [start]
    seen = set()
    T = 0
    while q:
        nq = []
        for curr in q:
            seen.add(curr)
            *poses, (x2, y2), c = curr
            if all(dirpad[y1][x1] == "A" for x1, y1 in poses):
                n = *poses, (x2, y2), c + numpad[y2][x2]
                if code.startswith(n[-1]) and n not in seen:
                    nq.append(n)
                    seen.add(n)
            i, (x1, y1) = next(((i, (x1, y1)) for i, (x1, y1) in enumerate(poses) if dirpad[y1][x1] != "A"), (-1, (-1, -1)))
            if i == dirbots - 1:
                dx, dy = dirs[dirpad[y1][x1]]
                xx, yy = x2 + dx, y2 + dy
                n = *poses, (xx, yy), c
                if 0 <= xx < 3 and 0 <= yy < 4 and numpad[yy][xx] != "#" and n not in seen:
                    nq.append(n)
                    seen.add(n)
            elif i >= 0:
                x, y = poses[i]
                x1, y1 = poses[i + 1]
                dx, dy = dirs[dirpad[y][x]]
                xx, yy = x1 + dx, y1 + dy
                ps = list(poses)
                ps[i + 1] = (xx, yy)
                n = *ps, (x2, y2), c
                if 0 <= xx < 3 and 0 <= yy < 2 and dirpad[yy][xx] != "#" and n not in seen:
                    nq.append(n)
                    seen.add(n)
            x, y = poses[0]
            for d in range(4):
                dx, dy = [(1, 0), (0, 1), (-1, 0), (0, -1)][d]
                xx, yy = x + dx, y + dy
                n = (xx, yy), *poses[1:], (x2, y2), c
                if 0 <= xx < 3 and 0 <= yy < 2 and dirpad[yy][xx] != "#" and n not in seen:
                    nq.append(n)
                    seen.add(n)
        q = nq
        T += 1
        if code in [t[-1] for t in nq]:
            ans += int(code[:-1]) * T
            break
print(ans)
