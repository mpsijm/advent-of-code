data = [line.strip() for line in open("data.in").readlines()]
h, w = len(data), len(data[0])

numpad = "789\n456\n123\n#0A".split("\n")
dirpad = "#^A\n<v>".split("\n")
start = ((2, 0), (2, 0), (2, 3), "")
dirs = {"^": (0, -1), "v": (0, 1), "<": (-1, 0), ">": (1, 0)}

ans = 0
for code in data:
    q = [start]
    seen = set()
    i = 0
    while q:
        nq = []
        for curr in q:
            seen.add(curr)
            (x, y), (x1, y1), (x2, y2), c = curr
            if dirpad[y][x] == "A":
                if dirpad[y1][x1] == "A":
                    n = (x, y), (x1, y1), (x2, y2), c + numpad[y2][x2]
                    if code.startswith(n[-1]) and n not in seen:
                        nq.append(n)
                        seen.add(n)
                else:
                    dx, dy = dirs[dirpad[y1][x1]]
                    xx, yy = x2 + dx, y2 + dy
                    n = (x, y), (x1, y1), (xx, yy), c
                    if 0 <= xx < 3 and 0 <= yy < 4 and numpad[yy][xx] != "#" and n not in seen:
                        nq.append(n)
                        seen.add(n)
            else:
                dx, dy = dirs[dirpad[y][x]]
                xx, yy = x1 + dx, y1 + dy
                n = (x, y), (xx, yy), (x2, y2), c
                if 0 <= xx < 3 and 0 <= yy < 2 and dirpad[yy][xx] != "#" and n not in seen:
                    nq.append(n)
                    seen.add(n)
            for d in range(4):
                dx, dy = [(1, 0), (0, 1), (-1, 0), (0, -1)][d]
                xx, yy = x + dx, y + dy
                n = (xx, yy), (x1, y1), (x2, y2), c
                if 0 <= xx < 3 and 0 <= yy < 2 and dirpad[yy][xx] != "#" and n not in seen:
                    nq.append(n)
                    seen.add(n)
        q = nq
        i += 1
        if code in [t[-1] for t in nq]:
            ans += int(code[:-1]) * i
            break
print(ans)
