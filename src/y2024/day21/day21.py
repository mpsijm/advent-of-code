import functools
from itertools import permutations

data = [line.strip() for line in open("data.in").readlines()]

numpad = "789\n456\n123\n#0A".split("\n")
dirpad = "#^A\n<v>".split("\n")
dirs = {"^": (0, -1), "v": (0, 1), "<": (-1, 0), ">": (1, 0)}
num_pos = {v: (x, y) for y, line in enumerate(numpad) for x, v in enumerate(line)}
dir_pos = {v: (x, y) for y, line in enumerate(dirpad) for x, v in enumerate(line)}


@functools.cache
def possible_moves(start, end, gap):
    if start == end:
        return ["A"]

    x1, y1 = start
    x2, y2 = end
    moves = list()
    if y2 > y1:
        for _ in range(y1, y2): moves.append("v")
    if y2 < y1:
        for _ in range(y2, y1): moves.append("^")
    if x2 > x1:
        for _ in range(x1, x2): moves.append(">")
    if x2 < x1:
        for _ in range(x2, x1): moves.append("<")

    valid_moves = []
    for p in permutations(moves):
        x, y = start
        for move in p:
            dx, dy = dirs[move]
            xx, yy = x + dx, y + dy
            if (xx, yy) == gap:
                break
            x, y = xx, yy
        else:
            valid_moves.append("".join(p) + "A")
    return valid_moves


@functools.cache
def shortest(buttons, level):
    if level < 0:
        return len(buttons)
    pos = num_pos if level == limit else dir_pos
    curr = pos["A"]
    total = 0
    for c in buttons:
        neigh = pos[c]
        total += min(shortest(m, level - 1) for m in possible_moves(curr, neigh, pos["#"]))
        curr = neigh
    return total


for limit in (2, 25):
    print(sum(int(code[:-1]) * shortest(code, limit) for code in data))
