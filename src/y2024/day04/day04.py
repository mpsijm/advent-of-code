data = [line.strip() for line in open("data.in").readlines()]
h, w = len(data), len(data[0])

xmas = "XMAS"
ans = 0
for y in range(h):
    for x in range(w):
        for d in range(8):
            dx, dy = [(1, 0), (1, 1), (1, -1), (-1, -1), (-1, 1), (0, 1), (-1, 0), (0, -1)][d]
            found = False
            for i in range(4):
                xx, yy = x + dx * i, y + dy * i
                if 0 <= xx < w and 0 <= yy < h:
                    if data[yy][xx] != xmas[i]:
                        break
                else:
                    break
            else:
                ans += 1
print(ans)

ans = 0
for y in range(h - 2):
    for x in range(w - 2):
        for ll in [
            [(0, 0, 'M'), (0, 2, 'M'), (1, 1, "A"), (2, 2, "S"), (2, 0, "S")],
            [(0, 0, 'S'), (0, 2, 'S'), (1, 1, "A"), (2, 2, "M"), (2, 0, "M")],
            [(0, 0, 'S'), (0, 2, 'M'), (1, 1, "A"), (2, 2, "M"), (2, 0, "S")],
            [(0, 0, 'M'), (0, 2, 'S'), (1, 1, "A"), (2, 2, "S"), (2, 0, "M")],
        ]:
            for dx, dy, c in ll:
                xx, yy = x + dx * 1, y + dy * 1
                if 0 <= xx < w and 0 <= yy < h:
                    if data[yy][xx] != c:
                        break
                else:
                    break
            else:
                ans += 1
print(ans)
