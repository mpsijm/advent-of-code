data = [[x for x in block.replace("\n", " ").split()] for block in open("data.in").read().strip().split("\n\n")]

a, b, c = map(int, data[0][2::3])
program = list(map(int, data[1][1].split(",")))
n = len(program)


def combo():
    x = program[ip + 1]
    if x <= 3:
        return x
    assert x != 7
    return a if x == 4 else b if x == 5 else c


i = 216133400000000
while True:
    i += 1
    if i == 216134600000000:
        i = 216182800000000
    if i == 216186200000000:
        i = 218332500000000
    if i == 218333600000000:
        i = 218381900000000
    if i == 218385200000000:
        print("Sad :(")
        break
    a, b, c = i, 0, 0
    res = []
    ip = 0
    while ip < n:
        command = program[ip]
        if command == 0:
            a //= 2 ** combo()
        if command == 1:
            b ^= program[ip + 1]
        if command == 2:
            b = combo() % 8
        if command == 3:
            if a != 0:
                ip = program[ip + 1]
                continue
        if command == 4:
            b = b ^ c
        if command == 5:
            res.append(combo() % 8)
            if res != program[:len(res)]:
                if len(res) > 8:
                    print(i, res[:-1])
                break
        if command == 6:
            b = a // 2 ** combo()
        if command == 7:
            c = a // 2 ** combo()
        ip += 2

    if res == program:
        break
