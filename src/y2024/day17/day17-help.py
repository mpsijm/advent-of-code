data = [[x for x in block.replace("\n", " ").split()] for block in open("data.in").read().strip().split("\n\n")]
n = len(data)

a, b, c = map(int, data[0][2::3])
program = list(map(int, data[1][1].split(",")))
n = len(program)


def combo():
    x = program[ip + 1]
    if x <= 3:
        return x
    assert x != 7
    return a if x == 4 else b if x == 5 else c


0o6111120000000000
0o6111130000000000
0o6111700000000000
0o6111730000000000
0o6151120000000000
0o6151130000000000
0o6151700000000000
0o6151730000000000
for help in ((216133400000000, 216134500000000), (216182900000000, 216186100000000), 218332600000000, 218382000000000):
    step = 1000000
    prev = 0
    for h in range(help, help + 100000 * step, step):
        a = h
        res = []
        ip = 0
        while ip < n:
            command = program[ip]
            if command == 0:
                a //= 2 ** combo()
            if command == 1:
                b ^= program[ip + 1]
            if command == 2:
                b = combo() % 8
            if command == 3:
                if a != 0:
                    ip = program[ip + 1]
                    continue
            if command == 4:
                b = b ^ c
            if command == 5:
                res.append(combo() % 8)
            if command == 6:
                b = a // 2 ** combo()
            if command == 7:
                c = a // 2 ** combo()
            ip += 2

        aaa = -8
        if len(res) == 16 and res[aaa:] == program[aaa:]:
            if prev + step != h:
                print()
            print(h, len(res), ",".join(str(x) for x in res))
            prev = h
    print(h)
