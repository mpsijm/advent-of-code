data = [[x for x in block.replace("\n", " ").split()] for block in open("data.in").read().strip().split("\n\n")]

a, b, c = map(int, data[0][2::3])
program = list(map(int, data[1][1].split(",")))
n = len(program)


def combo(op):
    assert op != 7
    return op if op <= 3 else a if op == 4 else b if op == 5 else c


for start, end in (
    # (0o6111120000000000, 0o6111130000000000),
    # (0o6111700000000000, 0o6111730000000000),
    # (0o6151120000000000, 0o6151130000000000),
    # (0o6151700000000000, 0o6151730000000000),
    (0o5000000000000000, 0o7700000000000000),
):
    for offset in (0o4257155, 0o4257277):
        for i in range(start + offset, end, 8 ** 7):
            a, b, c = i, 0, 0
            res = []
            ip = 0
            while ip < n:
                command, op = program[ip: ip + 2]
                if command == 0: a //= 2 ** combo(op)
                if command == 1: b ^= op
                if command == 2: b = combo(op) % 8
                if command == 3:
                    if a != 0:
                        ip = op
                        continue
                if command == 4: b = b ^ c
                if command == 5:
                    res.append(combo(op) % 8)
                    if res != program[:len(res)]:
                        if len(res) > 10:
                            print(i, res[:-1])
                        break
                if command == 6: b = a // 2 ** combo(op)
                if command == 7: c = a // 2 ** combo(op)
                ip += 2

            if res == program:
                print(i)
                exit()
