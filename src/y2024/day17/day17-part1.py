data = [[x for x in block.replace("\n", " ").split()] for block in open("data.in").read().strip().split("\n\n")]
n = len(data)

a, b, c = map(int, data[0][2::3])
program = list(map(int, data[1][1].split(",")))
n = len(program)


def combo():
    x = program[ip + 1]
    if x <= 3:
        return x
    assert x != 7
    return a if x == 4 else b if x == 5 else c


res = []
ip = 0
while ip < n:
    command = program[ip]
    if command == 0:
        a //= 2 ** combo()
    if command == 1:
        b ^= program[ip + 1]
    if command == 2:
        b = combo() % 8
    if command == 3:
        if a != 0:
            ip = program[ip + 1]
            continue
    if command == 4:
        b = b ^ c
    if command == 5:
        res.append(combo() % 8)
    if command == 6:
        b = a // 2 ** combo()
    if command == 7:
        c = a // 2 ** combo()
    ip += 2

    # print(a, b, c)
print(",".join(str(x) for x in res))
