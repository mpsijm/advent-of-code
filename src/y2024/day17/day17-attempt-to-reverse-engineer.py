data = [[x for x in block.replace("\n", " ").split()] for block in open("data.in").read().strip().split("\n\n")]
n = len(data)

a, b, c = map(int, data[0][2::3])
program = list(map(int, data[1][1].split(",")))
n = len(program)


def combo(op):
    if op <= 3:
        return op
    assert op != 7
    return a if op == 4 else b if op == 5 else c


for b in range(1):
    a, b, c = 0, b, 0
    ip = n - 4
    back = n - 1
    while True:
        if ip < 0:
            if back < 0:
                break
            ip = n - 4
        command = program[ip]
        op = program[ip + 1]
        if command == 0:
            a *= 2 ** combo(op)
        if command == 1:
            b ^= op
        if command == 2:
            b = combo(op) % 8
        if command == 4:
            b = b ^ c
        if command == 5:
            if op == 4:
                a = a // 8 * 8 + program[back]
            else:
                b = b // 8 * 8 + program[back]
            back -= 1
        if command == 6:
            b = a * 2 ** combo(op)
        if command == 7:
            c = a * 2 ** combo(op)
        print(command, op, a, b, c)
        ip -= 2
    print(a)
