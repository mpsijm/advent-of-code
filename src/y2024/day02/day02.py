data = [[int(x) for x in line.strip().split()] for line in open("data.in").readlines()]

print(sum((all(a < b for a, b in zip(line, line[1:])) or all(a > b for a, b in zip(line, line[1:])))
          and all(abs(a - b) <= 3 for a, b in zip(line, line[1:]))
          for line in data))

print(sum(any((all(a < b for a, b in zip(line, line[1:])) or all(a > b for a, b in zip(line, line[1:])))
              and all(abs(a - b) <= 3 for a, b in zip(line, line[1:]))
              for line in (ll[:i] + ll[i + 1:] for i in range(len(ll)))) for ll in data))
