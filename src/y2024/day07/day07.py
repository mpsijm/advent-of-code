data = [(int(first), list(map(int, rest.split())))
        for first, rest in [line.strip().split(": ") for line in open("data.in").readlines()]]

for part in (1, 2):
    ans = 0
    for first, rest in data:
        answers = [rest[0]]
        for val in rest[1:]:
            answers = [v + val for v in answers] + [v * val for v in answers] + (
                [int(str(v) + str(val)) for v in answers] if part == 2 else [])
        if first in answers:
            ans += first
    print(ans)
