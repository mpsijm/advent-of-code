print(*(
    sum(
        first for first, rest in [
            (int(first), list(map(int, rest.split())))
            for first, rest in [line.strip().split(": ") for line in open("data.in").readlines()]
        ] if first in __import__("functools").reduce(
            lambda answers, val: [v + val for v in answers] + [v * val for v in answers] + (
                [int(str(v) + str(val)) for v in answers] if part == 2 else []),
            rest[1:], [rest[0]])
    )
    for part in (1, 2)
), sep="\n")
