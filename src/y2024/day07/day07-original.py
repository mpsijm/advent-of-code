from itertools import product

data = [line.strip().split() for line in open("data.in").readlines()]
n = len(data)

for part in (1, 2):
    ans = 0
    for line in data:
        first, *rest = line
        first = int(first[:-1])
        rest = list(map(int, rest))
        ops = list(product(["+", "*"] if part == 1 else ["+", "*", "||"], repeat=len(rest) - 1))
        for opz in ops:
            v = rest[0]
            for a, b in zip(opz, rest[1:]):
                if a == "+":
                    v += b
                elif a == "*":
                    v *= b
                else:
                    v = int(str(v) + str(b))
            if first == v:
                ans += first
                break
    print(ans)
