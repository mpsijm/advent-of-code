data = [int(line) for line in open("data.in").readlines()]
n = len(data)

ans = 0
for sec in data:
    for _ in range(2000):
        r = sec * 64
        sec ^= r
        sec %= 16777216
        r = sec // 32
        sec ^= r
        sec %= 16777216
        r = sec * 2048
        sec ^= r
        sec %= 16777216
    ans += sec
print(ans)

secss = []
for sec in data:
    secs = [sec]
    for _ in range(2000):
        r = sec * 64
        sec ^= r
        sec %= 16777216
        r = sec // 32
        sec ^= r
        sec %= 16777216
        r = sec * 2048
        sec ^= r
        sec %= 16777216
        secs.append(sec % 10)
    secss.append(secs)

# This was never gonna work... But I wanted to run it anyway 😂
# (It didn't even get through one iteration of the outer loop within the 20 minutes I needed to solve 🙈)
best = 0
for A in range(-9, 10):
    print(A)
    for B in range(-9, 10):
        for C in range(-9, 10):
            for D in range(-9, 10):
                ans = 0
                for secs in secss:
                    for a, b, c, d, e in zip(secs, secs[1:], secs[2:], secs[3:], secs[4:]):
                        if b - a == A and c - b == B and d - c == C and e - d == D:
                            ans += e
                            break
                best = min(best, ans)
print(best)
