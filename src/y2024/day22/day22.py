from collections import defaultdict

data = [int(line) for line in open("data.in").readlines()]

ans = 0
for sec in data:
    for _ in range(2000):
        r = sec * 64
        sec ^= r
        sec %= 16777216
        r = sec // 32
        sec ^= r
        sec %= 16777216
        r = sec * 2048
        sec ^= r
        sec %= 16777216
    ans += sec
print(ans)

sum_diffs = defaultdict(int)
for sec in data:
    secs = [sec % 10]
    for _ in range(2000):
        r = sec * 64
        sec ^= r
        sec %= 16777216
        r = sec // 32
        sec ^= r
        sec %= 16777216
        r = sec * 2048
        sec ^= r
        sec %= 16777216
        secs.append(sec % 10)

    first_diffs = dict()
    for a, b, c, d, e in zip(secs, secs[1:], secs[2:], secs[3:], secs[4:]):
        tup = (b - a, c - b, d - c, e - d)
        if tup not in first_diffs:
            first_diffs[tup] = e

    for d, v in first_diffs.items():
        sum_diffs[d] += v

print(max(sum_diffs.values()))
