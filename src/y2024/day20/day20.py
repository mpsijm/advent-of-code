data = [list(line.strip()) for line in open("data.in").readlines()]
h, w = len(data), len(data[0])

# Sneaky trick: my ./run script automatically replaces "datb.in" with "data.in", so use that to switch
is_example = "b" in """open("datb.in")"""

start = next((x, y) for y, line in enumerate(data) for x, c in enumerate(line) if c == "S")
end = next((x, y) for y, line in enumerate(data) for x, c in enumerate(line) if c == "E")


prevs = dict()
q = [start]
seen = set()
i = 0
path = [end]
while q:
    nq = []
    for curr in q:
        seen.add(curr)
        x, y = curr
        for d in range(4):
            dx, dy = [(1, 0), (0, 1), (-1, 0), (0, -1)][d]
            xx, yy = x + dx, y + dy
            if 0 <= xx < w and 0 <= yy < h and data[yy][xx] != "#" and (xx, yy) not in seen:
                nq.append((xx, yy))
                seen.add((xx, yy))
                prevs[(xx, yy)] = (x, y)
    q = nq
    i += 1
    if end in nq:
        curr = end
        while curr != start:
            path.append(curr := prevs[curr])
        break


for part in (1, 2):
    ans = 0
    for cend, (x, y) in enumerate(path):
        for cstart, (xx, yy) in enumerate(path[:cend]):
            diff = abs(yy - y) + abs(xx - x)
            if diff <= (20 if part == 2 else 2):
                # The sum of all example cheats in part 2 is 285 (32+31+29+...+22+4+3)
                if cend - cstart - diff >= (50 if is_example else 100):
                    ans += 1
    print(ans)
