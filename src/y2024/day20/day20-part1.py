data = [line.strip() for line in open("data.in").readlines()]
h, w = len(data), len(data[0])
data = [list(line) for line in data]

start = next((x, y) for y, line in enumerate(data) for x, c in enumerate(line) if c == "S")
end = next((x, y) for y, line in enumerate(data) for x, c in enumerate(line) if c == "E")


def race():
    q = [start]
    seen = set()
    i = 0
    while q:
        nq = []
        for curr in q:
            seen.add(curr)
            x, y = curr
            for d in range(4):
                dx, dy = [(1, 0), (0, 1), (-1, 0), (0, -1)][d]
                xx, yy = x + dx, y + dy
                if 0 <= xx < w and 0 <= yy < h and not data[yy][xx] == "#" and (xx, yy) not in seen:
                    nq.append((xx, yy))
                    seen.add((xx, yy))
        q = nq
        i += 1
        if end in nq:
            return i


base = race()
print(base)

ans = 0
for y in range(h):
    for x in range(w):
        if data[y][x] != "#": continue
        data[y][x] = "."
        better = race()
        if base - better >= 100:
            ans += 1
        data[y][x] = "#"

print(ans)

