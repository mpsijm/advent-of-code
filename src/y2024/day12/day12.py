import string

data = [line.strip() for line in open("data.in").readlines()]
h, w = len(data), len(data[0])
DIRS = [(1, 0), (0, 1), (-1, 0), (0, -1)]

ans1, ans2 = 0, 0
for c in string.ascii_uppercase:
    poses = set((x, y) for y, line in enumerate(data) for x, d in enumerate(line) if c == d)
    while poses:
        curr = min(poses)
        sub = {curr}
        poses.remove(curr)
        stack = [curr]
        while stack:
            x, y = stack.pop()
            for d in range(4):
                dx, dy = DIRS[d]
                n = x + dx, y + dy
                if n in poses:
                    sub.add(n)
                    poses.remove(n)
                    stack.append(n)

        perimeter = 0
        for (x, y) in sub:
            for d in range(4):
                dx, dy = DIRS[d]
                n = x + dx, y + dy
                if n not in sub:
                    perimeter += 1
        area = len(sub)
        ans1 += perimeter * area

        total_sides = 0
        for (x, y) in sub:
            for d in range(4):
                dx, dy = DIRS[d]
                ddx, ddy = DIRS[(d - 1) % 4]
                xx, yy = x + ddx, y + ddy
                n = x + dx, y + dy
                if n not in sub and ((xx, yy) in sub and (xx + dx, yy + dy) in sub or (xx, yy) not in sub):
                    total_sides += 1
        ans2 += total_sides * area

print(ans1)
print(ans2)
