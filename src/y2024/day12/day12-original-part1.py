import string

data = [line.strip() for line in open("data.in").readlines()]
h, w = len(data), len(data[0])

ans = 0
for c in string.ascii_uppercase:
    poses = set((x, y) for y, line in enumerate(data) for x, d in enumerate(line) if c == d)
    area = len(poses)
    if area == 0:
        continue
    while poses:
        curr = next(iter(poses))
        sub = {curr}
        poses.remove(curr)
        stack = [curr]
        while stack:
            x, y = stack.pop()
            for d in range(4):
                dx, dy = [(1, 0), (0, 1), (-1, 0), (0, -1)][d]
                xx, yy = x + dx * 1, y + dy * 1
                if (xx, yy) in poses:
                    n = (xx, yy)
                    sub.add(n)
                    poses.remove(n)
                    stack.append(n)
        perimeter = 0
        for (x, y) in sub:
            for d in range(4):
                dx, dy = [(1, 0), (0, 1), (-1, 0), (0, -1)][d]
                xx, yy = x + dx, y + dy
                if (xx, yy) not in sub:
                    perimeter += 1
        area = len(sub)

        print(c, area, perimeter, area * perimeter)
        ans += perimeter * area
print(ans)
