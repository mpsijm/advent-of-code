import string
from math import ceil, floor

data = [line.strip() for line in open("data.in").readlines()]
h, w = len(data), len(data[0])



def valid_direction(d, x, y):
    if d == 0 and (floor(x), floor(y)) in perimeter and (floor(x), ceil(y)) in sub:
        return True
    if d == 1 and (ceil(x), floor(y)) in perimeter and (floor(x), floor(y)) in sub:
        return True
    if d == 2 and (ceil(x), ceil(y)) in perimeter and (ceil(x), floor(y)) in sub:
        return True
    if d == 3 and (floor(x), ceil(y)) in perimeter and (ceil(x), ceil(y)) in sub:
        return True
    return False


ans = 0
DEBUG = False
for c in string.ascii_uppercase:
    poses = set((x, y) for y, line in enumerate(data) for x, d in enumerate(line) if c == d)
    area = len(poses)
    if area == 0:
        continue
    while poses:
        curr = min(poses, key=lambda t: (t[1], t[0]))
        firrrst = curr
        sub = {curr}
        poses.remove(curr)
        stack = [curr]
        while stack:
            x, y = stack.pop()
            for d in range(4):
                dx, dy = [(1, 0), (0, 1), (-1, 0), (0, -1)][d]
                xx, yy = x + dx * 1, y + dy * 1
                if (xx, yy) in poses:
                    n = (xx, yy)
                    sub.add(n)
                    poses.remove(n)
                    stack.append(n)
        perimeter = set()
        # print(c, len(poses), len(sub))
        for (x, y) in sub:
            for d in range(4):
                dx, dy = [(1, 0), (0, 1), (-1, 0), (0, -1)][d]
                xx, yy = x + dx, y + dy
                if (xx, yy) not in sub:
                    perimeter.add((xx, yy))

        # This shape was counted as having one side too many, because on line 69, I had `key=lambda t: t[1]` instead...
        # DEBUG = firrrst == (63, 27)
        if DEBUG:
            mx = min(x for x, _ in sub)
            my = min(y for _, y in sub)
            nsub = set((x - mx, y - my) for x, y in sub)
            mx = max(x for x, _ in nsub)
            my = max(y for _, y in nsub)
            print(nsub)
            for y in range(my + 1):
                print("".join("X" if (x, y) in nsub else " " for x in range(mx + 1)))
        nperimeter = set(perimeter)
        ppp = 0
        while nperimeter:
            if DEBUG:
                print(nperimeter)
            first = min(nperimeter, key=lambda t: (t[1], t[0]))
            if DEBUG:
                print(first)
            x, y = first
            if valid_direction(0, x + 0.5, y + 0.5):
                x -= 0.5
                y += 0.5
                d = 0
            elif valid_direction(1, x - 0.5, y + 0.5):
                x -= 0.5
                y -= 0.5
                d = 1
            elif valid_direction(2, x - 0.5, y - 0.5):
                x += 0.5
                y -= 0.5
                d = 2
            elif valid_direction(3, x + 0.5, y - 0.5):
                x += 0.5
                y += 0.5
                d = 3
            else:
                assert False
            first = (x, y)
            pp = 1
            while (x, y) != first or pp < 2:
                diff = (x - 0.5, y - 0.5) if d == 0 else (x + 0.5, y - 0.5) if d == 1 else (
                    x + 0.5, y + 0.5) if d == 2 else (x - 0.5, y + 0.5)
                if DEBUG:
                    print(x, y, d, diff)
                if diff in nperimeter:
                    nperimeter.remove(diff)
                dx, dy = [(1, 0), (0, 1), (-1, 0), (0, -1)][d]
                xx, yy = x + dx * 1, y + dy * 1
                if valid_direction(d, xx, yy):
                    x, y = xx, yy
                    continue
                for dd in ((d - 1) % 4, (d + 1) % 4):
                    dx, dy = [(1, 0), (0, 1), (-1, 0), (0, -1)][dd]
                    xx, yy = x + dx * 1, y + dy * 1
                    if valid_direction(dd, xx, yy):
                        x, y, d = xx, yy, dd
                        if DEBUG:
                            print("turn", pp)
                        pp += 1
                        break
                else:
                    assert False
            diff = (x - 0.5, y - 0.5) if d == 0 else (x + 0.5, y - 0.5) if d == 1 else (
                x + 0.5, y + 0.5) if d == 2 else (x - 0.5, y + 0.5)
            if DEBUG:
                print(x, y, d, diff)
            if diff in nperimeter:
                nperimeter.remove(diff)
            ppp += pp

        area = len(sub)
        if DEBUG:
            print(c, area, ppp, area * ppp)
            print(firrrst[1], firrrst[0], ppp)
        ans += ppp * area

print(ans)
