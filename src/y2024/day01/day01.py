from collections import Counter

data = [[int(a), int(b)] for a, b in (line.strip().split() for line in open("data.in").readlines())]

a, b = zip(*data)
print(sum(abs(c - d) for c, d in zip(sorted(a), sorted(b))))
b = Counter(b)
print(sum(c * b[c] for c in a))
