data = [[int(x) for x in line.strip().split()] for line in open("data.in").readlines()][0]
n = len(data)

for _ in range(25):
    new_data = []
    for s in data:
        if s == 0:
            new_data.append(1)
        elif len(str(s)) % 2 == 0:
            m = len(str(s)) // 2
            new_data.append(int(str(s)[:m]))
            new_data.append(int(str(s)[m:]))
        else:
            new_data.append(s * 2024)
    data = new_data
print(len(data))

