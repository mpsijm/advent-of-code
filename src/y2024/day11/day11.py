from collections import Counter

data = [[int(x) for x in line.strip().split()] for line in open("data.in").readlines()][0]
n = len(data)

data = Counter(data)
for i in range(75):
    new_data = Counter()
    for s, c in data.items():
        if s == 0:
            new_data[1] += c
        elif len(str(s)) % 2 == 0:
            m = len(str(s)) // 2
            new_data[int(str(s)[:m])] += c
            new_data[int(str(s)[m:])] += c
        else:
            new_data[s * 2024] += c
    data = new_data
    if i == 24 or i == 74:
        print(sum(v for v in data.values()))
