from collections import defaultdict

data = [[x for x in block.replace("\n", " ").split()] for block in open("data.in").read().strip().split("\n\n")]

rules = defaultdict(set)
for a, b in (map(int, line.split("|")) for line in data[0]):
    rules[a].add(b)


def ordered(line):
    line_set = set(line)
    rules_for_this_line = defaultdict(set)
    for c in line:
        rules_for_this_line[c] = set(rules[c]) & line_set
    todo = set(line)
    ordered_line = []
    while len(ordered_line) != len(line):
        for a in list(todo):
            if not rules_for_this_line[a]:
                ordered_line.append(a)
                todo.remove(a)
                for s in rules_for_this_line.values():
                    if a in s:
                        s.remove(a)
    # Probably in reverse order, but we only need the middle element 🤷
    return ordered_line


ans, ans2 = 0, 0
for line in (list(map(int, line.split(","))) for line in data[1]):
    assert len(line) % 2 == 1, "Line length must be even"
    broken = any(b not in rules[a] for i, a in enumerate(line) for j, b in enumerate(line[i + 1:]))
    if broken:
        ans2 += ordered(line)[len(line) // 2]
    else:
        ans += line[len(line) // 2]
print(ans)
print(ans2)
