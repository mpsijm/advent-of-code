from functools import cmp_to_key

data = [[x for x in block.replace("\n", " ").split()] for block in open("data.in").read().strip().split("\n\n")]
rules = set(tuple(map(int, line.split("|"))) for line in data[0])

for part in (1, 0):
    print(sum(
        sorted_line[len(line) // 2]
        for line in (list(map(int, line.split(","))) for line in data[1])
        if (line == (sorted_line := sorted(line, key=cmp_to_key(lambda a, b: -1 if (a, b) in rules else 1)))) == part
    ))
