from functools import cmp_to_key

data = [[x for x in block.replace("\n", " ").split()] for block in open("data.in").read().strip().split("\n\n")]

rules, ans, ans2 = set(tuple(map(int, line.split("|"))) for line in data[0]), 0, 0
for line in (list(map(int, line.split(","))) for line in data[1]):
    sorted_line = sorted(line, key=cmp_to_key(lambda a, b: -1 if (a, b) in rules else 1))
    if line == sorted_line:
        ans += line[len(line) // 2]
    else:
        ans2 += sorted_line[len(line) // 2]
print(ans)
print(ans2)
