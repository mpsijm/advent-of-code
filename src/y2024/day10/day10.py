data = [[-9 if c == '.' else int(c) for c in line.strip()] for line in open("data.in").readlines()]
h, w = len(data), len(data[0])


def find(y, x, c):
    if c == 9:
        return {(y, x)} if part == 1 else 1
    a = set() if part == 1 else 0
    for d in range(4):
        dx, dy = [(1, 0), (0, 1), (-1, 0), (0, -1)][d]
        xx, yy = x + dx, y + dy
        if 0 <= xx < w and 0 <= yy < h and data[yy][xx] == c + 1:
            f = find(yy, xx, c + 1)
            if part == 1:
                a |= f
            else:
                a += f
    return a


for part in (1, 2):
    ans = 0
    for y in range(h):
        for x in range(w):
            if data[y][x] != 0:
                continue
            ans += len(find(y, x, 0)) if part == 1 else find(y, x, 0)
    print(ans)
