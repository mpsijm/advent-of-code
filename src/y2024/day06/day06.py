data = [line.strip() for line in open("data.in").readlines()]
h, w = len(data), len(data[0])

seen = set()
original_path = list()
start_y = next(i for i in range(h) if "^" in data[i])
start_x = next(j for j in range(w) if "^" == data[start_y][j])
x, y = start_x, start_y
d = 0
dirs = [(0, -1), (1, 0), (0, 1), (-1, 0)]
while 0 <= x < w and 0 <= y < h:
    seen.add((x, y))
    original_path.append((x, y, d))
    dx, dy = dirs[d]
    xx, yy = x + dx, y + dy
    if not (0 <= xx < w and 0 <= yy < h):
        break
    if data[yy][xx] == "#":
        d = (d + 1) % 4
        continue
    x, y = xx, yy

print(len(seen))

ans = 0
obstacles_seen = set()
for i in range(len(original_path) - 1):
    X, Y, D = original_path[i + 1]
    if (X, Y) in obstacles_seen:
        continue
    obstacles_seen.add((X, Y))
    x, y, d = original_path[i]
    if d != D:
        continue
    assert abs(x - X) == 1 or abs(y - Y) == 1
    seen = set(original_path[:i])
    d = (d + 1) % 4
    while 0 <= x < w and 0 <= y < h:
        if (x, y, d) in seen:
            ans += 1
            break
        seen.add((x, y, d))
        dx, dy = dirs[d]
        xx, yy = x + dx, y + dy
        if not (0 <= xx < w and 0 <= yy < h):
            break
        if data[yy][xx] == "#" or (xx, yy) == (X, Y):
            d = (d + 1) % 4
            continue
        x, y = xx, yy
print(ans)
