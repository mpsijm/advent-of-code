data = [line.strip() for line in open("data.in").readlines()]
h, w = len(data), len(data[0])

seen = set()
y = next(i for i in range(h) if "^" in data[i])
x = next(j for j in range(w) if "^" == data[y][j])
d = 0
dirs = [(0, -1), (1, 0), (0, 1), (-1, 0)]
while 0 <= x < w and 0 <= y < h:
    seen.add((x, y))
    dx, dy = dirs[d]
    xx, yy = x + dx * 1, y + dy * 1
    if not (0 <= xx < w and 0 <= yy < h):
        break
    if data[yy][xx] == "#":
        d = (d + 1) % 4
        continue
    x, y = xx, yy

print(len(seen))

ans = 0
for Y in range(h):
    for X in range(w):
        seen = set()
        y = next(i for i in range(h) if "^" in data[i])
        x = next(j for j in range(w) if "^" == data[y][j])
        d = 0
        while 0 <= x < w and 0 <= y < h:
            if (x, y, d) in seen:
                ans += 1
                break
            seen.add((x, y, d))
            dx, dy = dirs[d]
            xx, yy = x + dx * 1, y + dy * 1
            if not (0 <= xx < w and 0 <= yy < h):
                break
            if data[yy][xx] == "#" or (xx, yy) == (X, Y):
                d = (d + 1) % 4
                continue
            x, y = xx, yy
print(ans)
