data = [[x for x in block.split("\n")] for block in open("data.in").read().strip().split("\n\n")]

keys = []
locks = []
for block in data:
    if "." in block[0]:
        lock = []
        for line in zip(*block):
            lock.append(next((i for i, c in enumerate(list(reversed(line))[1:]) if c == "."), len(line) - 1))
        locks.append(lock)
    else:
        key = []
        for line in zip(*block):
            key.append(next((i for i, c in enumerate(line[1:]) if c == "."), len(line) - 1))
        keys.append(key)
ans = 0
for key in keys:
    for lock in locks:
        ans += all(a + b <= 5 for a, b in zip(key, lock))
print(ans)
