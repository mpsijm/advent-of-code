data = [block.split("\n") for block in open("data.in").read().strip().split("\n\n")]
blocks = [{(x, y) for y, line in enumerate(block) for x, c in enumerate(line) if c == "#"} for block in data]
print(sum(len(key | lock) == len(key) + len(lock) for i, key in enumerate(blocks) for lock in blocks[:i]))
