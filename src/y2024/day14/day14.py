import math

data = [line.strip() for line in open("data.in").readlines()]

# Sneaky trick: my ./run script automatically replaces "datb.in" with "data.in", so use that to switch
is_example = "b" in """open("datb.in")"""

w, h = (11, 7) if is_example else (101, 103)
robots = [[tuple(map(int, p[2:].split(","))) for p in line.split(" ")] for line in data]
for i in range(30000):
    if i == 99:
        mx = w // 2
        my = h // 2
        if is_example:
            print(robots)
        print(math.prod(map(sum, zip(*(
            (x < mx and y < my, x > mx and y < my, x > mx and y > my, x < mx and y > my) for (x, y), _ in robots)))))

    pic = [[" "] * w for _ in range(h)]
    for (x, y), _ in robots:
        pic[y][x] = "#"
    if sum("".join(line).count("########") for line in pic):
        print(i)
        # print(*("".join(line) for line in pic), sep="\n")
        break

    robots = [(((x + a) % w, (y + b) % h), (a, b)) for (x, y), (a, b) in robots]
